﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class ProfileModerationManager

    Dim ignoreList As String() = {"CreatedDate", "ModifiedBy", "ModifiedDate", "Active", "ID", "UserID", "Name"}
    Public Function GetNewUserProfileData(ByVal id As Integer) As ProfileModeration
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim newData As ProfileModeration = New ProfileModeration()
            newData = dbHelper.[Get](Of ProfileModeration)(command, "GetNewUserProfileData").FirstOrDefault()
            newData.ProfilePicture = CheckIfFileExists(newData.ProfilePicture)
            Return newData
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function CheckIfFileExists(ByVal fileName As String) As String
        Try
            Dim defaultUserProfileImage = ConfigManager.DefaultUserProfileImage()
            Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
            Dim filePath = folderPath + fileName
            If (File.Exists(filePath)) Then
                Return fileName
            End If

            Return defaultUserProfileImage
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetOldUserProfileData(ByVal id As Integer) As ProfileModeration
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim oldData As ProfileModeration = New ProfileModeration()
            oldData = dbHelper.[Get](Of ProfileModeration)(command, "GetOldUserProfileData").FirstOrDefault()
            oldData.ProfilePicture = CheckIfFileExists(oldData.ProfilePicture)
            Return oldData
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function GetListOfUserProfileDraftData(ByVal status As String, ByVal pagging As Pagging)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()


            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "Offset"
            dbDataParameter.Value = pagging.Offset
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameterSearchValue As IDbDataParameter = command.CreateParameter()
            dbDataParameterSearchValue.ParameterName = "SearchValue"
            dbDataParameterSearchValue.Value = pagging.SearchValue
            command.Parameters.Add(dbDataParameterSearchValue)

            Dim dbDataParameterSortBy As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortBy.ParameterName = "SortBy"
            dbDataParameterSortBy.Value = pagging.SortBy
            command.Parameters.Add(dbDataParameterSortBy)

            Dim dbDataParameterSortOrder As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortOrder.ParameterName = "SortOrder"
            dbDataParameterSortOrder.Value = pagging.SortOrder
            command.Parameters.Add(dbDataParameterSortOrder)

            Dim dbDataParameterPageSize As IDbDataParameter = command.CreateParameter()
            dbDataParameterPageSize.ParameterName = "PageSize"
            dbDataParameterPageSize.Value = pagging.PageSize
            command.Parameters.Add(dbDataParameterPageSize)

            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)


            Dim dbDataParameterStatus As IDbDataParameter = command.CreateParameter()
            dbDataParameterStatus.ParameterName = "Status"
            'Dim approved As Boolean = Convert.ToBoolean(IsApproved)
            dbDataParameterStatus.Value = status
            command.Parameters.Add(dbDataParameterStatus)

            Dim profileModeration As List(Of ProfileModeration) = New List(Of ProfileModeration)()
            profileModeration = dbHelper.[Get](Of ProfileModeration)(command, "GetUserProfileDraftData")

            Dim paggedResult As New PaggedResult(Of ProfileModeration)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = profileModeration

            Return paggedResult
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    'Public Function GetListOfUserProfileDraftData(ByVal status As String) As List(Of ProfileModeration)

    '    Dim dbHelper As DBHelper = New DBHelper()
    '    Dim command As IDbCommand = dbHelper.GetCommandObject()
    '    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '    dbDataParameter.ParameterName = "Status"
    '    'Dim approved As Boolean = Convert.ToBoolean(IsApproved)
    '    dbDataParameter.Value = status
    '    command.Parameters.Add(dbDataParameter)
    '    Dim users As List(Of ProfileModeration) = New List(Of ProfileModeration)()
    '    users = dbHelper.[Get](Of ProfileModeration)(command, "GetUserProfileDraftData")
    '    Return users

    'End Function

    Public Sub SaveProfileModeration(ByVal profileModeration As ProfileModeration)
        Try
            'Dim userName = HttpContext.Current.User.Identity.Name
            'profileModeration.UserID = GetUserId()
            'profileModeration.UserName = userName
            Dim dbHelper As DBHelper = New DBHelper()
            dbHelper.Save(profileModeration, ignoreList)
            Dim profileModerationStatusWithMessage As String = ""
            Dim userManager As UserManager = New UserManager()
            Dim userprofilemanager As UserProfileManager = New UserProfileManager()
            Dim userDetail = userManager.GetUserByUserName(profileModeration.UserName)
            Dim preference = userprofilemanager.GetUserProfileNotificationByUserID(userDetail.ID)
            'If (String.IsNullOrEmpty(profileModeration.Message) And (profileModeration.Status = "Approved")) Then
            '    profileModerationStatusWithMessage = "Status: " + profileModeration.Status + ";" + "Message: " + "Approved"
            'Else
            '    profileModerationStatusWithMessage = profileModeration.Status + " " + profileModeration.Message
            'End If

            If preference.Count <> 0 Then
                If preference.Exists(Function(x) (x.PreferenceID = NotificationPreferenceEnum.EMAIL Or x.PreferenceID = NotificationPreferenceEnum.BOTH) AndAlso x.ID = CInt(NotificationSettingEnum.UPDATEPROFILE)) Then
                    SendProfileModerationSaveEmail(userDetail.email, profileModeration.Status.ToLower(), userDetail.FirstName + " " + userDetail.LastName)
                End If
            End If
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub
    Public Function SendProfileModerationSaveEmail(ByVal email As String, ByVal message As String, ByVal name As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "ProfileModerationEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForProfileModerationSaveEmail() + message
            emailBody = GetEmailBodyForProfileModerationSave(body, message, name)
            EmailHelper.SendEmail(emailSubject, emailBody, email)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Private Function GetEmailBodyForProfileModerationSave(ByVal body As String, ByVal message As String, ByVal name As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()

            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@Message@@", message)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Private Shared Function GetUserId() As Integer
        Try
            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim userDetails = userManager.GetUserByUserName(userName)
            Return userDetails.ID
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetProfileUserByUserName(ByVal user As String) As User
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserName"
            dbDataParameter.Value = user
            command.Parameters.Add(dbDataParameter)
            Dim users As User = New User()
            users = dbHelper.[Get](Of User)(command, "GetUserByUserName").FirstOrDefault()
            Return users
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
End Class
