﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Reflection
Imports enova.cossp.Common.Lib

Public Class EventLogManager
    Dim ignoreList As String() = {"CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate", "Active", "DateLogged", "Count"}
    Public Function GetListOfEventLog(ByVal pagging As Pagging) ' As List(Of EventLog)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "Offset"
            dbDataParameter.Value = pagging.Offset
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameterSearchValue As IDbDataParameter = command.CreateParameter()
            dbDataParameterSearchValue.ParameterName = "SearchValue"
            dbDataParameterSearchValue.Value = pagging.SearchValue
            command.Parameters.Add(dbDataParameterSearchValue)

            Dim dbDataParameterSortBy As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortBy.ParameterName = "SortBy"
            dbDataParameterSortBy.Value = pagging.SortBy
            command.Parameters.Add(dbDataParameterSortBy)

            Dim dbDataParameterSortOrder As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortOrder.ParameterName = "SortOrder"
            dbDataParameterSortOrder.Value = pagging.SortOrder
            command.Parameters.Add(dbDataParameterSortOrder)

            Dim dbDataParameterPageSize As IDbDataParameter = command.CreateParameter()
            dbDataParameterPageSize.ParameterName = "PageSize"
            dbDataParameterPageSize.Value = pagging.PageSize
            command.Parameters.Add(dbDataParameterPageSize)

            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)

            Dim dbDataParameterdateFrom As IDbDataParameter = command.CreateParameter()
            dbDataParameterdateFrom.ParameterName = "DateFrom"
            dbDataParameterdateFrom.Value = pagging.dateFrom
            command.Parameters.Add(dbDataParameterdateFrom)

            Dim dbDataParameterdateTo As IDbDataParameter = command.CreateParameter()
            dbDataParameterdateTo.ParameterName = "DateTo"
            dbDataParameterdateTo.Value = pagging.dateTo
            command.Parameters.Add(dbDataParameterdateTo)


            Dim eventLogs As List(Of EventLog) = New List(Of EventLog)()
            eventLogs = dbHelper.[Get](Of EventLog)(command, "GetEventLog")

            Dim paggedResult As New PaggedResult(Of EventLog)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = eventLogs

            Return paggedResult
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function SaveEventLog(ByVal eventLog As EventLog)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            dbHelper.Save(eventLog, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function GetEventTypeCount(ByVal pagging As Pagging) As List(Of EventLog)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()

            Dim dbDataParameterdateFrom As IDbDataParameter = command.CreateParameter()
            dbDataParameterdateFrom.ParameterName = "DateFrom"
            dbDataParameterdateFrom.Value = pagging.dateFrom
            command.Parameters.Add(dbDataParameterdateFrom)

            Dim dbDataParameterdateTo As IDbDataParameter = command.CreateParameter()
            dbDataParameterdateTo.ParameterName = "DateTo"
            dbDataParameterdateTo.Value = pagging.dateTo
            command.Parameters.Add(dbDataParameterdateTo)


            Dim eventLog As List(Of EventLog) = New List(Of EventLog)()
            eventLog = dbHelper.[Get](Of EventLog)(command, "GetEventTypeCount")
            Return eventLog
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
