﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Script.Serialization
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities
Imports Newtonsoft.Json

Public Class CafmManager

    Public Function GetCafmAuthToken() As String
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim userName As String = ConfigManager.GetCafmApiUserName()
            Dim password As String = ConfigManager.GetCafmApiPassword()
            Dim url = cafmApiUrl + CafmAPIEndPoints.Auth
            Using client As New WebClient()
                Dim reqparm = New System.Collections.Specialized.NameValueCollection()
                reqparm.Add("UserName", userName)
                reqparm.Add("Password", password)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqparm)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response = (New JavaScriptSerializer()).Deserialize(Of Object)(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


#Region "Task realted API calls"

    Public Function Authenticate()
        Try
            Dim serviceUrl As String = "https://fm.enova-me.com/EnovaExternal/Services/Basic/SecurityService.svc"
            Dim input As Object = New With {
                    .loginName = "ENOVAIT",
                    .password = "P@ssw0rd123456"
        }
            'Dim inputJson As String = (New JavaScriptSerializer()).Serialize(input)

            Dim inputJson = JsonConvert.SerializeObject(input)
            Dim doc = JsonConvert.DeserializeXmlNode(inputJson, "breakdowntaskDTo")
            Dim xmlString As String = doc.ChildNodes(0).InnerXml

            Dim bytes As Byte() = Encoding.UTF8.GetBytes(xmlString)

            Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(serviceUrl & "/Authenticate")), HttpWebRequest)
            'httpRequest.Accept = "application/json"
            'httpRequest.ContentType = "application/json"
            'httpRequest.Accept = "text/xml"
            httpRequest.ContentType = "text/xml;" '"text/xml"
            httpRequest.Method = "POST"
            httpRequest.ContentLength = bytes.Length


            Using stream As Stream = httpRequest.GetRequestStream()
                stream.Write(bytes, 0, bytes.Length)
                stream.Close()
            End Using

            Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
                Using stream As Stream = httpResponse.GetResponseStream()
                    Dim response = (New StreamReader(stream)).ReadToEnd()
                End Using
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function SaveTask(ByVal task As Task, ByVal sessonId As String) As Object
        Try
            Dim cafmApiUrl As String = String.Empty 'ConfigManager.GetCafmServiceURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.CreateTask
            Using client As New WebClient()
                'Dim token = GetCafmAuthToken()
                'client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                Dim jsonSTring = JsonConvert.SerializeObject(task)
                Dim doc = JsonConvert.DeserializeXmlNode(jsonSTring, "breakdowntaskDTo")
                Dim xmlString As String = doc.ChildNodes(0).InnerXml

                reqParam.Add("sessionId", sessonId)
                reqParam.Add("breakdowntaskDTo", xmlString.ToString())
                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)

                Dim eventLog As EventLog = New EventLog()
                eventLog.Comments = "Task created"
                eventLog.ID = UserManager.GetUserId()
                eventLog.EventType = "Task created"
                eventLog.LoggedBy = UserManager.GetUserId()
                ' eventLog.LastLoggedOn = System.DateTime.Now
                Dim eventLogManager As EventLogManager = New EventLogManager()
                eventLogManager.SaveEventLog(eventLog)

                Return response

                'reqParam.Add("AssetId", task.AssetId)
                'reqParam.Add("AssetId", task.AssetId)
                'reqParam.Add("BuildingId", task.BuildingId)
                'reqParam.Add("Category", task.Category)
                'reqParam.Add("ContractId", task.ContractId)
                'reqParam.Add("CreatedDate", task.CreatedDate)
                'reqParam.Add("Discipline", task.Discipline)
                'reqParam.Add("InstructionId", task.InstructionId)
                'reqParam.Add("LocationId", task.LocationId)
                'reqParam.Add("LongDescription", task.LongDescription)
                'reqParam.Add("Priority", task.Priority)
                'reqParam.Add("RaisedDate", task.RaisedDate)
                'reqParam.Add("ShortDescription", task.ShortDescription)
                'reqParam.Add("CallerSource", task.CallerSource)
                'reqParam.Add("Phone", task.Phone)
                'reqParam.Add("ReporterName", task.ReporterName)
                'reqParam.Add("ReporterId", task.ReporterId)
                'reqParam.Add("Timezone", task.Timezone)
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function UpdateEmailForTask(ByVal taskSeq As String, ByVal email As String)
        Try

            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.EmailUpdateForTask
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskSeq", taskSeq)
                reqParam.Add("Email", email)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function



    Public Function SaveTaskPicture(ByVal taskSeq As String, ByVal base64Image As String, ByVal FileName As String, ByVal FileType As String)
        Try

            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.SaveTaskPicture
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskSeq", taskSeq)
                reqParam.Add("PictureBase64", base64Image)
                reqParam.Add("FileName", FileName)
                reqParam.Add("FileType", FileType)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetTaskPictures(ByVal taskSeq As String) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.GetTaskPictures
            Dim taskSequence = CInt(taskSeq)
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskSeq", taskSequence)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfTask(ByVal params As TaskListCafmParameters) As Object
        Try
            Dim paging = params.PagingModel
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.TaskList
            Using client As New WebClient()
                client.Encoding = Encoding.UTF8
                client.Encoding = UTF8Encoding.UTF8
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("ConSeq", paging.Id)
                reqParam.Add("PageNo", paging.PageNo)
                reqParam.Add("PageSize", paging.PageSize)
                reqParam.Add("OrderBy", paging.OrderBy)
                reqParam.Add("IsOrderAsc", paging.IsOrderAsc)

                reqParam.Add("DuDateFrom", params.DuDateFrom)
                reqParam.Add("DuDateTo", params.DuDateTo)
                reqParam.Add("TaskType", params.TaskType)
                reqParam.Add("TaskStatus", params.TaskStatus)
                reqParam.Add("Building", params.Building)
                reqParam.Add("TaskID", params.TaskID)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function GetTaskDetail(ByVal taskId As String) As Object
        Try
            Dim userManager As UserManager = New UserManager()
            Dim userId = UserManager.GetUserId()
            Dim userDetails = UserManager.GetUserDetails()
            Dim buildingSeq = ","
            Dim contractSeq = ","

            Dim userContracts = userManager.GetListOfContractByUser(userId).ToList()
            Dim contract As Contract
            For Each contract In userContracts
                contractSeq = contractSeq + contract.ConSeq.ToString() + ","
            Next

            If userDetails.IsAllBuilding Then
                buildingSeq = ""
            Else
                Dim buildingList = userManager.GetListOfBuildingByUser(userId, userDetails.IsAllBuilding)
                Dim building As Building
                For Each building In buildingList
                    buildingSeq = buildingSeq + building.BG_SEQ.ToString() + ","
                Next
            End If

            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.TaskDetail
            Using client As New WebClient()
                client.Encoding = Encoding.UTF8
                client.Encoding = UTF8Encoding.UTF8
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)

                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskSeq", taskId)
                reqParam.Add("ConSeq", contractSeq)
                reqParam.Add("BGSeq", buildingSeq)

                'reqParam.Add("TaskSeq", "8504953")
                'reqParam.Add("ConSeq", ",1190,")
                'reqParam.Add("BGSeq", ",7495,")

                'client.QueryString.Add("seq", taskId)
                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function GetHappinessIndex(ByVal userID As Integer) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.HappinessIndex
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                'client.QueryString.Add("userId", userID)
                Dim responseString As String = client.DownloadString(url)
                Dim response As Object = JsonConvert.DeserializeObject(responseString)
                Return response
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function SaveCSF(ByVal csf As CSF)
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.SaveCSF

            Using client As New WebClient()
                client.Encoding = Encoding.UTF8
                client.Encoding = UTF8Encoding.UTF8
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)

                Dim reqparm = New System.Collections.Specialized.NameValueCollection()
                reqparm.Add("TaskID", csf.TaskId)
                reqparm.Add("Rating", csf.Rating)
                reqparm.Add("Notes", csf.notes_Comments)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqparm)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response = (New JavaScriptSerializer()).Deserialize(Of Object)(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListIOfCSFTasks(ByVal phoneNumber As String, ByVal email As String) As List(Of CafmTask)
        Try
            'Dim paging = New PagingModelCafm()
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.GetListOfCSFTasks

            Using client As New WebClient()
                client.Encoding = Encoding.UTF8
                client.Encoding = UTF8Encoding.UTF8
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)

                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("PhoneNumber", phoneNumber)
                reqParam.Add("Email", email)
                'reqParam.Add("PhoneNumber", "971 4 409 4106")
                'reqParam.Add("Email", "North28@maf.co.ae")
                reqParam.Add("PageNo", 1)
                reqParam.Add("PageSize", 500)
                reqParam.Add("OrderBy", "TaskSeq")
                reqParam.Add("IsOrderAsc", 0)

                'reqParam.Add("PhoneNumber", "971 4 409 4106")
                'reqParam.Add("Email", "North28@maf.co.ae")
                'reqParam.Add("PageNo", paging.PageNo)
                'reqParam.Add("PageSize", paging.PageSize)
                'reqParam.Add("OrderBy", paging.OrderBy)
                'reqParam.Add("IsOrderAsc", paging.IsOrderAsc)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As List(Of CafmTask) = JsonConvert.DeserializeObject(Of List(Of CafmTask))(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetContractList() As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.ContractList
            Using client As New WebClient()

                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim responseString As String = client.DownloadString(url)
                Dim response As Object = JsonConvert.DeserializeObject(responseString)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetBuildingListByContractId(ByVal contractId As String) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.BuildingList
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                client.QueryString.Add("conseq", "," + contractId + ",")
                Dim responseString As String = client.DownloadString(url)
                Dim response As Object = JsonConvert.DeserializeObject(responseString)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfLocationByBuildingId(ByVal buildingId As String) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.LocationList
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                client.QueryString.Add("BGSeq", "," + buildingId + ",")
                Dim responseString As String = client.DownloadString(url)
                Dim response As Object = JsonConvert.DeserializeObject(responseString)
                Return response
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfProblem() As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.ProblemList
            Using client As New WebClient()
                client.Encoding = Encoding.UTF8
                client.Encoding = UTF8Encoding.UTF8
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim responseString As String = client.DownloadString(url)
                Dim response As Object = JsonConvert.DeserializeObject(responseString)
                Return response
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfEvents(ByVal paging As PagingModelCafm) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.EventList
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskSeq", paging.Id)
                reqParam.Add("PageNo", paging.PageNo)
                reqParam.Add("PageSize", paging.PageSize)
                reqParam.Add("OrderBy", paging.OrderBy)
                reqParam.Add("IsOrderAsc", paging.IsOrderAsc)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfStock(ByVal paging As PagingModelCafm) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.StockList
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Encoding = Encoding.UTF8
                client.Encoding = UTF8Encoding.UTF8
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskSeq", paging.Id)
                reqParam.Add("PageNo", paging.PageNo)
                reqParam.Add("PageSize", paging.PageSize)
                reqParam.Add("OrderBy", paging.OrderBy)
                reqParam.Add("IsOrderAsc", paging.IsOrderAsc)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    'Public Function GetProblemByContractId(ByVal contractId As String) As Object
    '    Try
    '        Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
    '        Dim url = cafmApiUrl + CafmAPIEndPoints.ProblemByContract
    '        Using client As New WebClient()
    '            Dim token = GetCafmAuthToken()
    '            client.Headers.Add("Authorization", "Bearer " + token)
    '            client.QueryString.Add("conseq", "," + contractId + ",")
    '            Dim responseString As String = client.DownloadString(url)
    '            Dim response As Object = JsonConvert.DeserializeObject(responseString)
    '            Return response
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Function GetLDAPLogin(ByVal Username As String, ByVal Password As String)
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.LDAPLoginURL
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("Username", Username)
                reqParam.Add("Password", Password)
                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function CheckCSFDone(ByVal TaskId As Integer) As Object
        Try
            Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
            Dim url = cafmApiUrl + CafmAPIEndPoints.CheckCSFDone
            Using client As New WebClient()
                Dim token = GetCafmAuthToken()
                client.Headers.Add("Authorization", "Bearer " + token)
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                reqParam.Add("TaskID", TaskId)
                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                Return response
            End Using
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
#End Region

End Class
