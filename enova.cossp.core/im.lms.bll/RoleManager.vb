﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Reflection
Imports System.Web
Imports enova.cossp.Common.Lib

Public Class RoleManager
    Dim ignoreList As String() = {"CreatedDate", "ModifiedBy", "ModifiedDate", "Screens", "Reports", "ScreensPermitted", "Active"}

    Public Sub SaveRole(ByVal role As Role)
        Try
            role.CreatedBy = GetUserId()
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            extraParams.Add("@ScreenList", GetDataTable(role.Screens))
            If role.Reports IsNot Nothing Then
                extraParams.Add("@ReportList", GetDataTable(role.Reports))

            End If


            dbHelper.Save(role, extraParams, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub
    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("Name", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("Name")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("Name") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function GetListOfRole() As List(Of Role)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim roles As List(Of Role) = New List(Of Role)()
            roles = dbHelper.[Get](Of Role)(command, "GetRole")
            Return roles
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetRoleByID(ByVal roleID As Integer) As Role
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = roleID
            command.Parameters.Add(dbDataParameter)
            Dim role As Role = New Role()
            role = dbHelper.[Get](Of Role)(command, "GetRoleByID").FirstOrDefault()
            role.Screens = GetListOfScreenByRole(roleID)
            role.Reports = GetListOfReportByRole(roleID)
            Return role
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function DeleteRoleByID(ByVal roleID As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = roleID
            command.Parameters.Add(dbDataParameter)
            'Dim roles As Role = New Role()
            Dim userCount = dbHelper.ExecuteScalar(command, "RoleDelete")
            Return userCount
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetListOfReportByRole(roleID As Integer) As List(Of Report)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim report As Report = New Report
            dbDataParameter.ParameterName = "RoleID"
            dbDataParameter.Value = roleID
            command.Parameters.Add(dbDataParameter)
            Dim reports As List(Of Report) = New List(Of Report)()
            reports = dbHelper.[Get](Of Report)(command, "GetReportByRole")
            For Each report In reports
                report.Name = report.Name + " - " + report.TypeName
            Next
            Return reports
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetListOfScreenByRole(roleID As Integer) As List(Of Screen)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "RoleID"
            dbDataParameter.Value = roleID
            command.Parameters.Add(dbDataParameter)
            Dim screens As List(Of Screen) = New List(Of Screen)()
            screens = dbHelper.[Get](Of Screen)(command, "GetScreenByRole")
            Return screens
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function ValidatedRoleName(ByVal name As String) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "@Name"
            dbDataParameter.Value = name
            command.Parameters.Add(dbDataParameter)
            Dim userCount = dbHelper.ExecuteScalar(command, "ValidatedRoleName")
            Return userCount
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function GetUserId() As Integer
        Try
            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim userDetails = userManager.GetUserByUserName(userName)
            Return userDetails.ID
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

End Class
