﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text

Public Class NotificationManager

    Dim ignoreList As String() = {"ID", "CreatedDate", "isRead", "ModifiedDate", "Active"}

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("Name", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("Name")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("Name") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function

    Public Function GetNotificationsByUserID(userID As Integer) As List(Of Notification)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim notifications As List(Of Notification) = New List(Of Notification)()
            notifications = dbHelper.[Get](Of Notification)(command, "GetNotificationsByUserID")
            Return notifications
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Public Function SetNotificationIsRead(ByVal id As Integer) As Notification
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim notification As Notification = New Notification()
            notification = dbHelper.[Get](Of Notification)(command, "SetNotificationIsRead").FirstOrDefault()
            Return notification
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function

    Public Function SaveNotification(ByVal notification As Notification)
        Try

            Dim dbHelper As DBHelper = New DBHelper()
            dbHelper.Save(notification, ignoreList)

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


End Class
