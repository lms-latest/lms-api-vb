﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Net
Imports System.Web.Script.Serialization
Imports enova.cossp.Common.Lib
Imports System.Text
Imports Newtonsoft.Json

Public Class ReportManager
    Dim ignoreList As String() = {"Active", "CreatedDate", "CreatedBy", "ModifiedBy", "ModifiedDate", "TypeName"}

    Public Sub SaveReport(ByVal report As Report)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            dbHelper.Save(report, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub

    Public Function GetListOfReport() As List(Of Report)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim report As Report = New Report
            Dim reports As List(Of Report) = New List(Of Report)()
            reports = dbHelper.[Get](Of Report)(command, "GetReport")

            For Each report In reports
                report.Name = report.Name + " - " + report.TypeName
            Next

            Return reports
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetTypeOfReport() As List(Of ReportType)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim reports As List(Of ReportType) = New List(Of ReportType)()
            reports = dbHelper.[Get](Of ReportType)(command, "GetReportType")
            Return reports
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetSizeOfWidget() As List(Of WidgetSize)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim reports As List(Of WidgetSize) = New List(Of WidgetSize)()
            reports = dbHelper.[Get](Of WidgetSize)(command, "GetWidgetSize")
            Return reports
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetReportByID(ByVal reportID As Integer) As Report
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = reportID
            command.Parameters.Add(dbDataParameter)
            Dim report As Report = New Report()
            report = dbHelper.[Get](Of Report)(command, "GetReportByID").FirstOrDefault()
            'If (report.ReportID IsNot Nothing And (report.TypeID = 2)) Then
            '    Dim embedUrl = PowerBIHelper.GetPowerBIEmbeddedUrl(report.ReportID).Result
            '    report.EmbeddedUrl = embedUrl
            'End If
            Return report
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetReportByUserID(ByVal userID As Integer) As List(Of Report)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserId"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter2.ParameterName = "TypeId"
            dbDataParameter2.Value = 3
            command.Parameters.Add(dbDataParameter2)

            Dim report = dbHelper.[Get](Of Report)(command, "GetReportByUserID")
            Return report
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function DeleteReportByID(ByVal reportID As Integer) As Report
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = reportID
            command.Parameters.Add(dbDataParameter)
            Dim report As Report = New Report()
            report = dbHelper.[Get](Of Report)(command, "ReportDelete").FirstOrDefault()
            Return report
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    'Functions for Report and View Report

    Public Function GetTableAndPowerBIReports(userID As Integer) As List(Of ReportSummary)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "UserID"
            parameter1.Value = userID
            command.Parameters.Add(parameter1)

            Dim reports As List(Of ReportSummary) = New List(Of ReportSummary)()
            reports = dbHelper.[Get](Of ReportSummary)(command, "GetTableAndPowerBIReportByUser")

            Return reports
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    'Public Function GetTableAndPowerBIReports(userID As Integer, ByVal pagging As Pagging)
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()

    '        Dim parameter1 As IDbDataParameter = command.CreateParameter()
    '        parameter1.ParameterName = "UserID"
    '        parameter1.Value = userID
    '        command.Parameters.Add(parameter1)

    '        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '        dbDataParameter.ParameterName = "Offset"
    '        dbDataParameter.Value = pagging.Offset
    '        command.Parameters.Add(dbDataParameter)

    '        Dim dbDataParameterSearchValue As IDbDataParameter = command.CreateParameter()
    '        dbDataParameterSearchValue.ParameterName = "SearchValue"
    '        dbDataParameterSearchValue.Value = pagging.SearchValue
    '        command.Parameters.Add(dbDataParameterSearchValue)

    '        Dim dbDataParameterSortBy As IDbDataParameter = command.CreateParameter()
    '        dbDataParameterSortBy.ParameterName = "SortBy"
    '        dbDataParameterSortBy.Value = pagging.SortBy
    '        command.Parameters.Add(dbDataParameterSortBy)

    '        Dim dbDataParameterSortOrder As IDbDataParameter = command.CreateParameter()
    '        dbDataParameterSortOrder.ParameterName = "SortOrder"
    '        dbDataParameterSortOrder.Value = pagging.SortOrder
    '        command.Parameters.Add(dbDataParameterSortOrder)

    '        Dim dbDataParameterPageSize As IDbDataParameter = command.CreateParameter()
    '        dbDataParameterPageSize.ParameterName = "PageSize"
    '        dbDataParameterPageSize.Value = pagging.PageSize
    '        command.Parameters.Add(dbDataParameterPageSize)

    '        Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
    '        dbDataParameterTotalRecords.Direction = ParameterDirection.Output
    '        dbDataParameterTotalRecords.ParameterName = "TotalRecords"
    '        dbDataParameterTotalRecords.DbType = DbType.Int32
    '        dbDataParameterTotalRecords.Value = 1
    '        command.Parameters.Add(dbDataParameterTotalRecords)

    '        Dim reports As List(Of ReportSummary) = New List(Of ReportSummary)()
    '        reports = dbHelper.[Get](Of ReportSummary)(command, "GetTableAndPowerBIReportByUser")

    '        Dim paggedResult As New PaggedResult(Of ReportSummary)
    '        paggedResult.Count = dbDataParameterTotalRecords.Value
    '        paggedResult.Result = reports

    '        Return paggedResult
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function

    Public Function ValidateReportName(ByVal name As String) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "Name"
            dbDataParameter.Value = name
            command.Parameters.Add(dbDataParameter)
            Dim userCount = dbHelper.ExecuteScalar(command, "ValidateReportName")
            Return userCount

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function ValidateReportAccessForUser(ByVal userId As Integer, ByVal reportId As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userId
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter2.ParameterName = "ReportID"
            dbDataParameter2.Value = reportId
            command.Parameters.Add(dbDataParameter2)

            Dim userCount = dbHelper.ExecuteScalar(command, "ValidateReportAccessForUser")
            Return userCount

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetReportTableData(reportDetails As Report, contractsList As List(Of Contract), paging As PagingModelCafm) As Object
        Try
            'Dim url As Stringhttps://jsonplaceholder.typicode.com/todos
            'Using client As New WebClient()
            '    Dim json As String = client.DownloadString(url)
            '    Dim response = (New JavaScriptSerializer()).Deserialize(Of Object)(json)
            '    Return response
            'End Using

            Dim url = reportDetails.ServiceUrl
            Using client As New WebClient()
                Dim reqParam = New System.Collections.Specialized.NameValueCollection()
                Dim contractSeqString As String = ","

                'If Authentication is checked
                If reportDetails.Authentication Then
                    Dim token = GetTableTypeReportToken()
                    client.Headers.Add("Authorization", "Bearer " + token)
                End If

                'If Contract Filter is checked
                If reportDetails.ContractFilter Then
                    Dim contract As Contract
                    For Each contract In contractsList
                        contractSeqString = contractSeqString + contract.ConSeq.ToString() + ","
                    Next
                    reqParam.Add("ConSeq", contractSeqString)
                End If

                'If PaggingSupport is checked
                If reportDetails.PagigngSupport Then
                    If paging IsNot Nothing Then

                        reqParam.Add("PageNo", paging.PageNo)
                        reqParam.Add("PageSize", paging.PageSize)
                        reqParam.Add("OrderBy", paging.OrderBy)
                        reqParam.Add("IsOrderAsc", paging.IsOrderAsc)
                        reqParam.Add(reportDetails.FilterParameter, paging.SearchValue)
                        'reqParam.Add("SearchParam", paging.SearchValue)
                    End If
                End If

                'POST request in case any one from ContractFilter or PaggingSupport is checked
                If (reportDetails.PagigngSupport Or reportDetails.ContractFilter) Then

                    Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqParam)
                    Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                    Dim response As Object = JsonConvert.DeserializeObject(responsebody)
                    Return response

                Else
                    Dim json As String = client.DownloadString(url)
                    Dim response = (New JavaScriptSerializer()).Deserialize(Of Object)(json)
                    Return response
                End If

            End Using


        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetTableTypeReportToken() As String
        Try
            Dim url As String = ConfigManager.ApiUrlForTableTypeReportToken()
            Dim userName As String = ConfigManager.UsernameForTableTypeReportToken()
            Dim password As String = ConfigManager.PasswordForTableTypeReportToken()

            Using client As New WebClient()
                Dim reqparm = New System.Collections.Specialized.NameValueCollection()
                reqparm.Add("UserName", userName)
                reqparm.Add("Password", password)

                Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqparm)
                Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                Dim response = (New JavaScriptSerializer()).Deserialize(Of Object)(responsebody)
                Return response
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

End Class
