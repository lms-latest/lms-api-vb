﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class AnnouncementManager
    Dim ignoreList As String() = {"CreatedDate", "ModifiedBy", "ModifiedDate", "announcementOption", "oldPircture", "visibleTo", "count",
        "first", "size", "sortBy", "sortOrderBy", "searchString"}
    Dim ignoreListPaging As String() = {"dateFrom", "dateTo"}

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("Name", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("Name")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("Name") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    'Public Function SendPollSaveEmail(ByVal emails As List(Of String), ByVal name As String)
    '    Try
    '        Dim emailBody As String = ""
    '        Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "PollCreationEmailTemplate.html")
    '        Dim emailSubject = ConfigManager.SubjectForPollSaveEmail()
    '        emailBody = GetEmailBodyForPollSave(body, name)
    '        EmailHelper.SendEmailMultiple(emailSubject, emailBody, emails)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try


    'End Function
    Public Function SendAnnouncementSaveEmail(ByVal email As String, ByVal name As String, ByVal username As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "AnnouncementCreationEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForAnnouncementSaveEmail()
            emailBody = GetEmailBodyForAnnouncementSave(body, name, username)
            EmailHelper.SendEmail(emailSubject, emailBody, email)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Private Function GetEmailBodyForAnnouncementSave(ByVal body As String, ByVal name As String, ByVal username As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()

            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", username)
            emailBodyHtml.Replace("@@AnnouncementName@@", name)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function SaveAnnouncement(ByVal announcement As Announcement)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            extraParams.Add("@RoleList", GetDataTable(announcement.visibleTo))
            'extraParams.Add("@OptionList", GetDataTable(announcement.announcementOption))
            Dim result1 = dbHelper.Save(announcement, extraParams, ignoreList)
            If Not (String.IsNullOrEmpty(announcement.Picture) Or String.IsNullOrEmpty(announcement.oldPircture)) Then
                Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/AnnoucementPic/")
                UpdateImageName(result1(0).ID, announcement.Picture.Replace(result1(0).ID.ToString() + "-", ""))
                Dim fileName = result1(0).ID.ToString() + "-" + announcement.Picture.Replace(result1(0).ID.ToString() + "-", "")
                Return AnnouncementManager.RenameFileName(announcement.oldPircture, fileName, folderPath)
            End If
            Dim roles As DataTable = New DataTable()
            roles = GetDataTable(announcement.visibleTo)
            Dim result As List(Of UserInfo) = GetUsersByRoles(roles)
            Dim userprofilemanager As UserProfileManager = New UserProfileManager()
            'Dim emails As List(Of String) = result.[Select](Function(o) o.email).ToList()
            'Dim testemails As List(Of String) = New List(Of String)(New String() {"ajita.pancholi@infobeans.com", "dharmendra.baghel@infobeans.com", "apsys10@gmail.com"})
            For Each userinfo In result
                SendAnnouncementSaveEmail(userinfo.email, announcement.name, userinfo.FirstName + " " + userinfo.LastName)
            Next

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function UpdateImageName(ByVal userId As Integer, ByVal fileName As String)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = userId
            command.Parameters.Add(dbDataParameter)

            Dim dbFileNameDataParameter As IDbDataParameter = command.CreateParameter()
            dbFileNameDataParameter.ParameterName = "FileName"
            dbFileNameDataParameter.Value = fileName
            command.Parameters.Add(dbFileNameDataParameter)
            Dim filName = dbHelper.ExecuteScalar(command, "UpdateAnnouncementImage")
            Return filName
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function RenameFileName(ByVal oldFileName As String, ByVal newFileName As String, ByVal folderPath As String)
        'Dim userId = UserManager.GetUserId()
        'Dim fileName = recordId + "-" + stringFileName + "-" + userId
        System.IO.File.Move(folderPath + oldFileName, folderPath + newFileName)
        Return newFileName
    End Function
    'Public Sub SavePollResult(ByVal pollResult As PollResult)
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        dbHelper.Save(pollResult, ignoreList)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    Public Function GetListOfAnnouncement(ByVal filters As Pagging) As PaggedResult(Of Announcement)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dictionary As New Dictionary(Of String, Object)

            Dim info() As PropertyInfo = filters.GetType().GetProperties()
            For Each prop In info
                If Not ignoreListPaging.Contains(prop.Name) Then
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = prop.Name
                    dbDataParameter.Value = prop.GetValue(filters, Nothing)
                    command.Parameters.Add(dbDataParameter)
                End If
            Next
            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)


            Dim poll As List(Of Announcement) = New List(Of Announcement)()
            poll = dbHelper.[Get](Of Announcement)(command, "GetAnnouncement")
            Dim paggedResult As New PaggedResult(Of Announcement)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = poll
            Return paggedResult

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetAnnouncementByID(ByVal pollID As Integer) As Announcement
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim announcement As Announcement = New Announcement()
            announcement = dbHelper.[Get](Of Announcement)(command, "GetAnnouncementByID").FirstOrDefault()
            'poll.announcementOption = GetListOfOptionsByAnnouncement(pollID)
            announcement.visibleTo = GetListOfAllowedRolesByAnnouncement(pollID)
            announcement.Picture = CheckIfFileExists(announcement.Picture)


            Return announcement
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Function CheckIfFileExists(ByVal fileName As String) As String
        Try
            Dim defaultUserProfileImage = ConfigManager.DefaultUserProfileImage()
            Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/AnnoucementPic/")
            Dim filePath = folderPath + fileName
            If (File.Exists(filePath)) Then
                Return fileName
            End If

            Return ""
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    'Public Function GetListOfOptionsByAnnouncement(pollID As Integer) As List(Of AnnouncementOption)
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()
    '        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '        dbDataParameter.ParameterName = "AnnouncementID"
    '        dbDataParameter.Value = pollID
    '        command.Parameters.Add(dbDataParameter)
    '        Dim options As List(Of AnnouncementOption) = New List(Of AnnouncementOption)()
    '        options = dbHelper.[Get](Of AnnouncementOption)(command, "GetOptionsByAnnouncement")
    '        Return options
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function

    Public Function GetListOfAllowedRolesByAnnouncement(pollID As Integer) As List(Of Role)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "AnnouncementID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim roles As List(Of Role) = New List(Of Role)()
            roles = dbHelper.[Get](Of Role)(command, "GetAllowedRolesByAnnouncement")
            Return roles
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function DeleteAnnouncementByID(ByVal pollID As Integer) As Announcement
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim poll As Announcement = New Announcement()
            poll = dbHelper.[Get](Of Announcement)(command, "AnnouncementDelete").FirstOrDefault()
            Return poll
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    'Public Function GetPollResultReportByPollID(pollID As Integer) As List(Of PollResultReport)
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()
    '        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '        dbDataParameter.ParameterName = "ID"
    '        dbDataParameter.Value = pollID
    '        command.Parameters.Add(dbDataParameter)
    '        Dim result As List(Of PollResultReport) = New List(Of PollResultReport)()
    '        result = dbHelper.[Get](Of PollResultReport)(command, "GetPollResultReportByPollID")
    '        Return result
    '    Catch ex As Exception
    '        Throw ex

    '    End Try


    'End Function
    'Public Function CheckUserResponseOnPoll(userID As Integer?, pollID As Integer) As Integer
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()
    '        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '        Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
    '        dbDataParameter.ParameterName = "PollID"
    '        dbDataParameter.Value = pollID
    '        dbDataParameter2.ParameterName = "UserID"
    '        dbDataParameter2.Value = userID
    '        command.Parameters.Add(dbDataParameter)
    '        command.Parameters.Add(dbDataParameter2)
    '        Dim result As Integer = New Integer()
    '        'result = dbHelper.[Get](Of Integer)(command, "CheckUserResponseOnPoll").FirstOrDefault()
    '        result = dbHelper.ExecuteScalar(command, "CheckUserResponseOnPoll")
    '        Return result
    '    Catch ex As Exception
    '        Throw ex
    '    End Try


    'End Function

    Public Shared Function GetUsersByRoles(ByVal roleList As DataTable) As List(Of UserInfo)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "RoleList"
            dbDataParameter.Value = roleList
            command.Parameters.Add(dbDataParameter)
            Dim result As List(Of UserInfo) = New List(Of UserInfo)()
            result = dbHelper.[Get](Of UserInfo)(command, "GetUsersByRoles")

            Return result

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function



End Class
