﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class UserProfileManager

    Dim userProfileignoreList As String() = {"contract", "building", "categoryID", "Message", "Preference", "IsAllBuilding", "Name"}
    'Dim userProfileignoreList As String() = {"contract", "building", "jobTitle", "company", "categoryID", "Message", "IsNotificationClosed", "oldProfilePircture", "Preference"}
    'Dim userProfileignoreList As String() = {"contract", "building", "jobTitle", "company", "categoryID", "Message", "IsNotificationClosed", "Preference"}
    Dim notificationignoreList As String() = {"UserID"}

    Public Function GetListOfContractBuildingByUser(userID As Integer) As List(Of Contract)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim contracts As List(Of Contract) = New List(Of Contract)()
            contracts = dbHelper.[Get](Of Contract)(command, "GetContractBuildingByUser")
            Return contracts
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function GetListOfContractByUser(userID As Integer) As List(Of Contract)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim contracts As List(Of Contract) = New List(Of Contract)()
            contracts = dbHelper.[Get](Of Contract)(command, "GetContractByUser")
            Return contracts
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    'Public Function GetListOfBuildingByUser(userID As Integer) As List(Of Building)
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()
    '        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '        dbDataParameter.ParameterName = "UserID"
    '        dbDataParameter.Value = userID
    '        command.Parameters.Add(dbDataParameter)
    '        Dim buildings As List(Of Building) = New List(Of Building)()
    '        buildings = dbHelper.[Get](Of Building)(command, "GetBuildingByUser")
    '        Return buildings
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function SaveUserProfileData(ByVal userInfo As UserInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            dbHelper.Save(userInfo, userProfileignoreList)
            Dim userManager As UserManager = New UserManager()
            Dim userInfos As UserInfo = New UserInfo()
            'If Not String.IsNullOrEmpty(userInfo.profilePicture) And Not String.IsNullOrEmpty(userInfo.oldProfilePircture) Then
            If Not (String.IsNullOrEmpty(userInfo.profilePicture) Or String.IsNullOrEmpty(userInfo.oldProfilePircture)) Then
                Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
                'UserManager.UpdateImageName(userInfo.ID, userInfo.profilePicture)
                Dim fileName = userInfo.ID.ToString() + "-" + userInfo.profilePicture
                Return UserManager.RenameFileName(userInfo.oldProfilePircture, fileName, folderPath)
            End If
            Dim results As List(Of UserInfo) = GetAdminUsers()
            Dim test As List(Of String) = New List(Of String)
            For Each userInfos In results
                test.Add(userInfos.email)
            Next
            SendUpdateProfileSaveEmail(test, userInfo.FirstName + " " + userInfo.LastName, "Moderator")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function GetAdminUsers() As List(Of UserInfo)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim result As List(Of UserInfo) = New List(Of UserInfo)()
            result = dbHelper.[Get](Of UserInfo)(command, "GetAdminUsers")
            Return result

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function SendUpdateProfileSaveEmail(ByVal ToMail As List(Of String), ByVal profileusername As String, ByVal name As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "UpdateProfileEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForUpdateProfileSaveEmail()
            emailBody = GetEmailBodyForUpdateProfileSave(body, profileusername, name)
            EmailHelper.SendEmailMultiple(emailSubject, emailBody, ToMail)
        Catch ex As Exception
            Throw ex
        End Try


    End Function
    Private Function GetEmailBodyForUpdateProfileSave(ByVal body As String, ByVal profileusername As String, ByVal name As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()

            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()
            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@ProfileUserName@@", profileusername)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetUserProfileDetails(ByVal user As String) As UserInfo
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserName"
            dbDataParameter.Value = user
            command.Parameters.Add(dbDataParameter)
            Dim userInfo As UserInfo = New UserInfo()
            Dim userManager As UserManager = New UserManager()
            userInfo = dbHelper.[Get](Of UserInfo)(command, "GetUserByUserName").FirstOrDefault()
            userInfo.contract = GetListOfContractByUser(userInfo.ID)
            userInfo.building = userManager.GetListOfBuildingByUser(userInfo.ID, userInfo.IsAllBuilding)
            For Each contract As Contract In userInfo.contract
                contract.building = userInfo.building.Where(Function(o) o.ContractID = contract.ID).ToList
            Next
            userInfo.jobTitle = userInfo.jobTitle
            userInfo.profilePicture = CheckIfFileExists(userInfo.profilePicture)
            Return userInfo
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function CheckIfFileExists(ByVal fileName As String) As String
        Try
            Dim defaultUserProfileImage = ConfigManager.DefaultUserProfileImage()
            Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
            Dim filePath = folderPath + fileName
            If (File.Exists(filePath)) Then
                Return fileName
            End If

            Return defaultUserProfileImage
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    'Public Function GetJobTitleByUser(userID As Integer) As List(Of JobTitle)
    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()
    '        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '        dbDataParameter.ParameterName = "UserID"
    '        dbDataParameter.Value = userID
    '        command.Parameters.Add(dbDataParameter)
    '        Dim jobtitle As List(Of JobTitle) = New List(Of JobTitle)()
    '        jobtitle = dbHelper.[Get](Of JobTitle)(command, "GetJobTitleByUser")
    '        Return jobtitle
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function GetListOfNotificationSetting() As List(Of NotificationSetting)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim notificationSetting As List(Of NotificationSetting) = New List(Of NotificationSetting)()
            notificationSetting = dbHelper.[Get](Of NotificationSetting)(command, "GetNotificationSetting")
            Return notificationSetting
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfNotificationPreferences() As List(Of NotificationPreference)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim notificationPreference As List(Of NotificationPreference) = New List(Of NotificationPreference)()
            notificationPreference = dbHelper.[Get](Of NotificationPreference)(command, "GetNotificationPreference")
            Return notificationPreference
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Sub SaveNotificationData(ByVal notificationSetting As List(Of NotificationSetting), ByVal UserID As Integer)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            'Dim extraParams As Hashtable = New Hashtable()
            'extraParams.Add("UserID", UserID)
            'extraParams.Add("@NotificationList", GetDataTable(notificationSetting))
            Dim userIDParam As IDbDataParameter = command.CreateParameter()
            userIDParam.ParameterName = "UserID"
            userIDParam.Value = UserID
            command.Parameters.Add(userIDParam)
            Dim notificationListParam As IDbDataParameter = command.CreateParameter()
            notificationListParam.ParameterName = "NotificationList"
            notificationListParam.Value = GetDataTable(notificationSetting)
            command.Parameters.Add(notificationListParam)
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "NotificationSettingSave"
            dbHelper.ExecuteQuery(command)
            'dbHelper.Save(New List(Of NotificationSetting)(), extraParams, notificationignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("PreferenceID", GetType(Integer))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyPreferenceID As PropertyInfo = tentity.GetProperty("PreferenceID")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("PreferenceID") = propertyPreferenceID.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function GetUserProfileNotificationByUserID(ByVal UserID As Integer) As List(Of NotificationSetting)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim userIDParam As IDbDataParameter = command.CreateParameter()
            userIDParam.ParameterName = "UserID"
            userIDParam.Value = UserID
            command.Parameters.Add(userIDParam)
            Dim notificationSetting As List(Of NotificationSetting) = New List(Of NotificationSetting)()
            notificationSetting = dbHelper.[Get](Of NotificationSetting)(command, "GetUserProfileNotificationByUserID")
            Return notificationSetting
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function UpdateIsNotificationClosed(ByVal userName As String)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim userIDParam As IDbDataParameter = command.CreateParameter()
            userIDParam.ParameterName = "UserName"
            userIDParam.Value = userName
            command.Parameters.Add(userIDParam)
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "UpdateIsNotificationClosed"
            dbHelper.ExecuteQuery(command)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
