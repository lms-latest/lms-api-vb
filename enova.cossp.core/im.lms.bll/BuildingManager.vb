﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Public Class BuildingManager

    Public Function GetListOfBuildingByContract(ByVal contract As List(Of Contract)) As List(Of Building)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ContractList"
            dbDataParameter.Value = GetDataTable(contract)
            command.Parameters.Add(dbDataParameter)
            Dim building As List(Of Building) = New List(Of Building)()
            building = dbHelper.[Get](Of Building)(command, "GetBuildingByContract")
            Return building
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("Name", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("Name")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("Name") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfBuilding() As List(Of Building)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            'Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim building As List(Of Building) = New List(Of Building)()
            building = dbHelper.[Get](Of Building)(command, "GetBuilding")
            Return building
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfBuildingByContractId(ByVal contractId As String) As Object
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ContractId"
            dbDataParameter.Value = contractId
            command.Parameters.Add(dbDataParameter)
            Dim building As List(Of Building) = New List(Of Building)()
            building = dbHelper.[Get](Of Building)(command, "GetBuilding")
            Return building
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
