﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web
Imports enova.cossp.Common.Lib

Public Class DirectoryManger
    Public Function GetDirectory(ByVal pagging As Pagging) 'As List(Of Directory)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "Offset"
            dbDataParameter.Value = pagging.Offset
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameterSearchValue As IDbDataParameter = command.CreateParameter()
            dbDataParameterSearchValue.ParameterName = "SearchValue"
            dbDataParameterSearchValue.Value = pagging.SearchValue
            command.Parameters.Add(dbDataParameterSearchValue)

            Dim dbDataParameterSortBy As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortBy.ParameterName = "SortBy"
            dbDataParameterSortBy.Value = pagging.SortBy
            command.Parameters.Add(dbDataParameterSortBy)

            Dim dbDataParameterSortOrder As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortOrder.ParameterName = "SortOrder"
            dbDataParameterSortOrder.Value = pagging.SortOrder
            command.Parameters.Add(dbDataParameterSortOrder)

            Dim dbDataParameterPageSize As IDbDataParameter = command.CreateParameter()
            dbDataParameterPageSize.ParameterName = "PageSize"
            dbDataParameterPageSize.Value = pagging.PageSize
            command.Parameters.Add(dbDataParameterPageSize)

            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)

            Dim dbDataParameterUserId As IDbDataParameter = command.CreateParameter()
            dbDataParameterUserId.ParameterName = "UserId"
            dbDataParameterUserId.Value = GetUserId()
            command.Parameters.Add(dbDataParameterUserId)

            Dim directory As List(Of Directory) = New List(Of Directory)()
            directory = dbHelper.[Get](Of Directory)(command, "GetDirectory")

            Dim paggedResult As New PaggedResult(Of Directory)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = directory

            Return paggedResult
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Private Shared Function GetUserId() As Integer
        Try
            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim userDetails = userManager.GetUserByUserName(userName)
            Return userDetails.ID
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
