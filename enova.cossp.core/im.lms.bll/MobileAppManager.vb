﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text

Public Class MobileAppManager

    Dim ignoreList As String() = {}

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("Title", GetType(String))
            dt.Columns.Add("DocumentUrl", GetType(String))
            Dim tentity As Type = GetType(T)
            Dim propertyTitle As PropertyInfo = tentity.GetProperty("Title")
            Dim propertyDocumentUrl As PropertyInfo = tentity.GetProperty("DocumentUrl")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("Title") = propertyTitle.GetValue(entity, Nothing)
                dr("DocumentUrl") = propertyDocumentUrl.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Sub SaveMobileApp(ByVal mobileApp As MobileApp)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            Dim mobileAppDocument As MobileAppDocument = New MobileAppDocument()
            If mobileApp.MobileAppDocuments Is Nothing Then
                mobileApp.MobileAppDocuments = New List(Of MobileAppDocument)()
                mobileAppDocument.ID = 0
                mobileAppDocument.Title = ""
                mobileAppDocument.DocumentUrl = ""
                mobileAppDocument.Active = True
                mobileApp.MobileAppDocuments.Add(mobileAppDocument)
            End If
            ignoreList = {"MobileAppDocuments"}
            extraParams.Add("@MobileAppDocuments", GetDataTable(mobileApp.MobileAppDocuments))
            dbHelper.Save(mobileApp, extraParams, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub


    Public Function GetMobileApp() As MobileApp
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim mobileApp As MobileApp = New MobileApp()
            mobileApp = dbHelper.[Get](Of MobileApp)(command, "GetMobileApp").FirstOrDefault()
            Return mobileApp
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetMobileAppDocument() As List(Of MobileAppDocument)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim mobileAppDocuments As List(Of MobileAppDocument) = New List(Of MobileAppDocument)()
            mobileAppDocuments = dbHelper.[Get](Of MobileAppDocument)(command, "GetMobileAppDocuments")
            Return mobileAppDocuments
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

End Class
