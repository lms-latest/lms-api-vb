﻿Imports System.Collections.Generic
Imports enova.cossp.Common.Lib
Imports enova.cossp.dal
Imports enova.cossp.entities

Public Class ManageDashboardManager
    Dim ignoreList As String() = {"Name"}

    Public Function GetManageDashboardWidget(ByVal widgetSizeId As String) As List(Of WidgetInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "SizeID"
            parameter1.Value = widgetSizeId
            command.Parameters.Add(parameter1)

            Dim parameter2 As IDbDataParameter = command.CreateParameter()
            parameter2.ParameterName = "UserID"
            parameter2.Value = CInt(ConfigManager.AllUsers())
            command.Parameters.Add(parameter2)

            Dim parameter3 As IDbDataParameter = command.CreateParameter()
            parameter3.ParameterName = "TypeID"
            parameter3.Value = CInt(ConfigManager.DashboardWidgetTypeID())
            command.Parameters.Add(parameter3)

            Return dbHelper.[Get](Of WidgetInfo)(command, "GetDashboardWidget")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


    Public Function GetScreenSection(ByVal screenID As String) As List(Of ScreenSection)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "ScreenID"
            parameter1.Value = CInt(screenID)
            command.Parameters.Add(parameter1)

            Dim parameter2 As IDbDataParameter = command.CreateParameter()
            parameter2.ParameterName = "UserID"
            parameter2.Value = CInt(ConfigManager.AllUsers)
            command.Parameters.Add(parameter2)

            Return dbHelper.[Get](Of ScreenSection)(command, "GetScreenSection")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Sub SaveDasboardSection(ByVal sectionDetails As ScreenSection, ByVal userId As Integer)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            extraParams.Add("@ModifiedBy", userId)

            dbHelper.Save(sectionDetails, extraParams, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Sub

End Class
