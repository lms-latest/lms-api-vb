﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class LocationManager


    Public Function GetListOfLocation() As List(Of Location)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim location As List(Of Location) = New List(Of Location)()
            location = dbHelper.[Get](Of Location)(command, "GetLocation")
            Return location
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfProblem() As List(Of Location)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim location As List(Of Location) = New List(Of Location)()
            location = dbHelper.[Get](Of Location)(command, "GetLocation")
            Return location
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function GetLocationByContract(ByVal contactId As Integer) As List(Of Location)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()

            dbDataParameter.ParameterName = "ContactId"
            dbDataParameter.Value = contactId
            command.Parameters.Add(dbDataParameter)

            Dim location As List(Of Location) = New List(Of Location)()
            location = dbHelper.[Get](Of Location)(command, "GetLocationByContractId")
            Return location
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetLocationByBuilding(ByVal buildingId As String) As List(Of Location)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "BuildingId"
            dbDataParameter.Value = buildingId
            command.Parameters.Add(dbDataParameter)
            Dim location As List(Of Location) = New List(Of Location)()
            '    location = dbHelper.[Get](Of Location)(command, "GetLocationByContractId")
            location = dbHelper.[Get](Of Location)(command, "GetLocationByBuildingId")
            Return location
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

End Class
