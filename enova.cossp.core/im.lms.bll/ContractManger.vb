﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class ContractManger

    Public Function GetListOfContract() As List(Of Contract)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim contract As List(Of Contract) = New List(Of Contract)()
            contract = dbHelper.[Get](Of Contract)(command, "GetContract")
            Return contract
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    ' To get CAFM data from envoa DB
    'Public Function GetListOfContract() As Object
    '    Dim dbHelper As DBHelper = New DBHelper()
    '    Dim command As IDbCommand = dbHelper.GetCommandObject()
    '    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '    Dim contract As List(Of Contract) = New List(Of Contract)()
    '    contract = dbHelper.[Get](Of Contract)(command, "GetContract")
    '    Return contract
    'End Function

End Class
