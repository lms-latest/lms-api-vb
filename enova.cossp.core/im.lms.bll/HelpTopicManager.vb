﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class HelpTopicManager


    'Dim ignoreList As String() = {"CreatedDate", "CreatedBy", "ModifiedBy", "ModifiedDate"}
    Dim ignoreList As String() = {"HelpTopicDocuments"}

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("HelpTopicId", GetType(Integer))
            dt.Columns.Add("DocumentPath", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("HelpTopicId")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("ActualDocumentPath")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("HelpTopicId") = propertyInfoID.GetValue(entity, Nothing)
                dr("DocumentPath") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Sub SaveHelpTopic(ByVal helpTopic As HelpTopic)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            If (helpTopic.HelpTopicDocuments IsNot Nothing) Then
                extraParams.Add("@HelpTopicDocuments", GetDataTable(helpTopic.HelpTopicDocuments))
            End If
            Dim result = dbHelper.Save(helpTopic, extraParams, ignoreList)
            If (helpTopic.HelpTopicDocuments IsNot Nothing) Then
                Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/HelpTopic/")
                For Each helpTopicDoc In helpTopic.HelpTopicDocuments
                    Dim newFile = result(0).ID.ToString() + "-" + helpTopicDoc.ActualDocumentPath
                    Dim oldFileName = helpTopicDoc.DocumentPath
                    UserManager.RenameFileName(oldFileName, newFile, folderPath)
                Next
            End If

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub

    Public Function GetListOfHelpTopicAll() As List(Of HelpTopic)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim helpTopics As List(Of HelpTopic) = New List(Of HelpTopic)()
            helpTopics = dbHelper.[Get](Of HelpTopic)(command, "GetHelpTopicAll")
            Return helpTopics

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfHelpTopic(ByVal pagging As Pagging) 'As List(Of T)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "Offset"
            dbDataParameter.Value = pagging.Offset
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameterSearchValue As IDbDataParameter = command.CreateParameter()
            dbDataParameterSearchValue.ParameterName = "SearchValue"
            dbDataParameterSearchValue.Value = pagging.SearchValue
            command.Parameters.Add(dbDataParameterSearchValue)

            Dim dbDataParameterSortBy As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortBy.ParameterName = "SortBy"
            dbDataParameterSortBy.Value = pagging.SortBy
            command.Parameters.Add(dbDataParameterSortBy)

            Dim dbDataParameterSortOrder As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortOrder.ParameterName = "SortOrder"
            dbDataParameterSortOrder.Value = pagging.SortOrder
            command.Parameters.Add(dbDataParameterSortOrder)

            Dim dbDataParameterPageSize As IDbDataParameter = command.CreateParameter()
            dbDataParameterPageSize.ParameterName = "PageSize"
            dbDataParameterPageSize.Value = pagging.PageSize
            command.Parameters.Add(dbDataParameterPageSize)

            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)

            Dim helpTopics As List(Of HelpTopic) = New List(Of HelpTopic)()
            helpTopics = dbHelper.[Get](Of HelpTopic)(command, "GetHelpTopic")

            Dim paggedResult As New PaggedResult(Of HelpTopic)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = helpTopics

            Return paggedResult
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetHelpTopicByID(ByVal ID As Integer) As HelpTopic
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = ID
            command.Parameters.Add(dbDataParameter)
            Dim helpTopic As HelpTopic = New HelpTopic()
            helpTopic = dbHelper.[Get](Of HelpTopic)(command, "GetHelpTopicByID").FirstOrDefault()
            If (helpTopic IsNot Nothing) Then
                helpTopic.HelpTopicDocuments = GetHelpTopicDocumentsByHelpTopiccID(helpTopic.ID)
            End If
            'user.contract = GetListOfContractByUser(userID)
            Return helpTopic
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetHelpTopicDocumentsByHelpTopiccID(ByVal ID As Integer) As List(Of HelpTopicDocument)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = ID
            command.Parameters.Add(dbDataParameter)
            Dim helpTopicDocuments As List(Of HelpTopicDocument) = New List(Of HelpTopicDocument)()
            helpTopicDocuments = dbHelper.[Get](Of HelpTopicDocument)(command, "GetHelpTopicDocumentsByHelpTopicID").ToList()
            Return helpTopicDocuments
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function DeleteHelpTopicByID(ByVal id As Integer) As HelpTopic
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim helpTopic As HelpTopic = New HelpTopic()
            helpTopic = dbHelper.[Get](Of HelpTopic)(command, "HelpTopicDelete").FirstOrDefault()
            Return helpTopic
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function ValidatedHelpTopicTitle(ByVal title As String, ByVal id As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameterTitle As IDbDataParameter = command.CreateParameter()
            dbDataParameterTitle.ParameterName = "Title"
            dbDataParameterTitle.Value = title
            command.Parameters.Add(dbDataParameterTitle)

            Dim dbDataParameterId As IDbDataParameter = command.CreateParameter()
            dbDataParameterId.ParameterName = "Id"
            dbDataParameterId.Value = id
            command.Parameters.Add(dbDataParameterId)

            Dim helpTopicCount = dbHelper.ExecuteScalar(command, "ValidatedHelpTopicTitle")
            Return helpTopicCount
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
