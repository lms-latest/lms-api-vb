﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class UserManager


    Dim ignoreList As String() = {"building", "CreatedDate", "ModifiedBy", "ModifiedDate", "contract", "oldProfilePircture", "Active", "Name"}

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("Name", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("Name")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("Name") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function SaveUser(ByVal user As User)
        Try
            user.CreatedBy = GetUserId()
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()

            extraParams.Add("@ContractList", GetDataTable(user.contract))
            extraParams.Add("@BuildingList", GetDataTable(user.building))
            'extraParams.Add("@EXCMsgList", GetDataTable(user.exchangeMessageWith))
            Dim result = dbHelper.Save(user, extraParams, ignoreList)
            'If Not String.IsNullOrEmpty(user.profilePicture) And Not String.IsNullOrEmpty(user.oldProfilePircture) Then
            If Not (String.IsNullOrEmpty(user.profilePicture) Or String.IsNullOrEmpty(user.oldProfilePircture)) Then
                Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
                UpdateImageName(result(0).ID, user.profilePicture.Replace(result(0).ID.ToString() + "-", ""))
                Dim fileName = result(0).ID.ToString() + "-" + user.profilePicture.Replace(result(0).ID.ToString() + "-", "")
                Return UserManager.RenameFileName(user.oldProfilePircture, fileName, folderPath)
            End If
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfUser(ByVal pagging As Pagging) 'As List(Of User)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "Offset"
            dbDataParameter.Value = pagging.Offset
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameterSearchValue As IDbDataParameter = command.CreateParameter()
            dbDataParameterSearchValue.ParameterName = "SearchValue"
            dbDataParameterSearchValue.Value = pagging.SearchValue
            command.Parameters.Add(dbDataParameterSearchValue)

            Dim dbDataParameterSortBy As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortBy.ParameterName = "SortBy"
            dbDataParameterSortBy.Value = pagging.SortBy
            command.Parameters.Add(dbDataParameterSortBy)

            Dim dbDataParameterSortOrder As IDbDataParameter = command.CreateParameter()
            dbDataParameterSortOrder.ParameterName = "SortOrder"
            dbDataParameterSortOrder.Value = pagging.SortOrder
            command.Parameters.Add(dbDataParameterSortOrder)

            Dim dbDataParameterPageSize As IDbDataParameter = command.CreateParameter()
            dbDataParameterPageSize.ParameterName = "PageSize"
            dbDataParameterPageSize.Value = pagging.PageSize
            command.Parameters.Add(dbDataParameterPageSize)

            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)

            Dim users As List(Of User) = New List(Of User)()
            users = dbHelper.[Get](Of User)(command, "GetUser")

            Dim paggedResult As New PaggedResult(Of User)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = users

            Return paggedResult

            'Dim dbHelper As DBHelper = New DBHelper()
            'Dim command As IDbCommand = dbHelper.GetCommandObject()
            'Dim users As List(Of User) = New List(Of User)()
            'users = dbHelper.[Get](Of User)(command, "GetUser")
            'Return users
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Shared Function GeneratePassword(ByVal length As Integer, ByVal numberOfNonAlphanumericCharacters As Integer) As String
        Try
            Dim password As String = Membership.GeneratePassword(length, numberOfNonAlphanumericCharacters)
            Return password
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetUserByUserName(ByVal user As String) As User
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserName"
            dbDataParameter.Value = user
            command.Parameters.Add(dbDataParameter)
            Dim users As User = New User()
            users = dbHelper.[Get](Of User)(command, "GetUserByUserName").FirstOrDefault()
            Return users
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetUserByID(ByVal userID As Integer) As User
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim user As User = New User()
            user = dbHelper.[Get](Of User)(command, "GetUserByID").FirstOrDefault()
            user.contract = GetListOfContractByUser(userID)
            user.building = GetListOfBuildingByUser(userID, user.IsAllBuilding)
            user.profilePicture = CheckIfFileExists(user.profilePicture)
            Return user
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfContractByUser(userID As Integer) As List(Of Contract)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim contracts As List(Of Contract) = New List(Of Contract)()
            contracts = dbHelper.[Get](Of Contract)(command, "GetContractByUser")
            Return contracts
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfBuildingByUser(userID As Integer, IsAllBuilding As Boolean) As List(Of Building)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim dbIsAllBuildingDataParameter As IDbDataParameter = command.CreateParameter()
            dbIsAllBuildingDataParameter.ParameterName = "IsAllBuilding"
            dbIsAllBuildingDataParameter.Value = IsAllBuilding
            command.Parameters.Add(dbIsAllBuildingDataParameter)
            Dim buildings As List(Of Building) = New List(Of Building)()
            buildings = dbHelper.[Get](Of Building)(command, "GetBuildingByUser")
            Return buildings
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetUserMappingByUser(userID As Integer) As List(Of User)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim users As List(Of User) = New List(Of User)()
            users = dbHelper.[Get](Of User)(command, "GetUserMappingByUser")
            Return users
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function DeleteUserByID(ByVal userID As Integer) As User
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim users As User = New User()
            users = dbHelper.[Get](Of User)(command, "UserDelete").FirstOrDefault()
            Return users
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetUserSidebarScreenList(ByVal userName As String) As List(Of NavigationScreen)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameterUN As IDbDataParameter = command.CreateParameter()
            dbDataParameterUN.ParameterName = "UserName"
            dbDataParameterUN.Value = userName
            command.Parameters.Add(dbDataParameterUN)
            Dim screenList As List(Of NavigationScreen) = New List(Of NavigationScreen)()
            screenList = dbHelper.[Get](Of NavigationScreen)(command, "GetScreenByUser")
            Return screenList

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function SendEmailForUserCreation(ByVal details As DetailsForPasswordUpdationEmail)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "UserCreationEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForUserCreationEmail()
            emailBody = GetEmailBodyForUserCreation(body, details)
            EmailHelper.SendEmail(emailSubject, emailBody, details.Email)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function SendEmailForLDAPUserCreation(ByVal details As DetailsForPasswordUpdationEmail)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "LDAPUserCreationEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForUserCreationEmail()
            emailBody = GetEmailBodyForLDAPUserCreation(body, details)
            EmailHelper.SendEmail(emailSubject, emailBody, details.Email)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Private Function GetEmailBodyForUserCreation(ByVal body As String, ByVal details As DetailsForPasswordUpdationEmail) As String
        Try
            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            Dim url = ConfigManager.WebUrlForEnova()

            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", details.Name)
            emailBodyHtml.Replace("@@UserName@@", details.UserName)
            emailBodyHtml.Replace("@@Password@@", details.Password)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Private Function GetEmailBodyForLDAPUserCreation(ByVal body As String, ByVal details As DetailsForPasswordUpdationEmail) As String
        Try
            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            Dim url = ConfigManager.WebUrlForEnova()

            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", details.Name)
            emailBodyHtml.Replace("@@UserName@@", details.UserName)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function ValidatedUserEmail(ByVal email As String) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "EmailID"
            dbDataParameter.Value = email
            command.Parameters.Add(dbDataParameter)
            Dim userCount = dbHelper.ExecuteScalar(command, "ValidatedUserEmail")
            Return userCount
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Shared Function UpdateImageName(ByVal userId As Integer, ByVal fileName As String)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = userId
            command.Parameters.Add(dbDataParameter)

            Dim dbFileNameDataParameter As IDbDataParameter = command.CreateParameter()
            dbFileNameDataParameter.ParameterName = "FileName"
            dbFileNameDataParameter.Value = fileName
            command.Parameters.Add(dbFileNameDataParameter)
            Dim filName = dbHelper.ExecuteScalar(command, "UpdateUserImage")
            Return filName
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetJobTitleByUser(ByVal userID As Integer) As JobTitle
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserId"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim jobTitle As JobTitle = New JobTitle()
            jobTitle = dbHelper.[Get](Of JobTitle)(command, "GetJobTitleByUser").FirstOrDefault()
            Return jobTitle
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function GetUserId() As Integer
        Try
            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim userDetails = userManager.GetUserByUserName(userName)
            Return userDetails.ID
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Shared Function GetUserDetails() As User

        Try
            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim userDetails = userManager.GetUserByUserName(userName)
            Return userDetails
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Shared Function GenerateFileName()
        Try
            Return GenerateGUID()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function GenerateFileName(ByVal uploadedFileName As String)
        Try
            Return GenerateGUID() + "@" + uploadedFileName
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function RenameFileName(ByVal oldFileName As String, ByVal newFileName As String, ByVal folderPath As String)
        'Dim userId = UserManager.GetUserId()
        'Dim fileName = recordId + "-" + stringFileName + "-" + userId
        Try
            System.IO.File.Move(folderPath + oldFileName, folderPath + newFileName)
            Return newFileName
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function GenerateGUID()
        Dim guid As String
        guid = System.Guid.NewGuid.ToString()
        Return guid
    End Function
    Public Function ValidatedUserName(ByVal name As String) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "@Name"
            dbDataParameter.Value = name
            command.Parameters.Add(dbDataParameter)
            Dim userCount = dbHelper.ExecuteScalar(command, "ValidatedUserName")
            Return userCount
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function CheckIfFileExists(ByVal fileName As String) As String
        Try
            Dim defaultUserProfileImage = ConfigManager.DefaultUserProfileImage()
            Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
            Dim filePath = folderPath + fileName
            If (File.Exists(filePath)) Then
                Return fileName
            End If

            Return ""
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
