﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class CSFManager

    Dim ignoreList As String() = {"ID", "Active", "CreatedDate", "CreatedBy", "ModifiedBy", "ModifiedDate"}

    Public Sub SaveCSF(ByVal csf As CSF)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            dbHelper.Save(csf, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub
End Class
