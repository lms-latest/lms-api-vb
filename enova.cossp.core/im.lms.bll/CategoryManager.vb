﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class CategoryManager

    Public Function GetListOfCategory() As List(Of Category)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim category As List(Of Category) = New List(Of Category)()
            category = dbHelper.[Get](Of Category)(command, "GetCategory")
            Return category
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
