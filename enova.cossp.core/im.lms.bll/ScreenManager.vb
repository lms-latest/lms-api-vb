﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class ScreenManager

    Public Function GetListOfAllowedControllers(ByVal userName As String, ByVal ControllerName As String) As List(Of AllowedControllers)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameterUN As IDbDataParameter = command.CreateParameter()
            dbDataParameterUN.ParameterName = "UserName"
            dbDataParameterUN.Value = userName
            command.Parameters.Add(dbDataParameterUN)
            Dim dbDataParameterCN As IDbDataParameter = command.CreateParameter()
            dbDataParameterCN.ParameterName = "ControllerName"
            dbDataParameterCN.Value = ControllerName
            command.Parameters.Add(dbDataParameterCN)
            Dim allowedControllers As List(Of AllowedControllers) = New List(Of AllowedControllers)()
            allowedControllers = dbHelper.[Get](Of AllowedControllers)(command, "GetAllowedControllers")
            Return allowedControllers
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfScreen() As List(Of Screen)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim screen As List(Of Screen) = New List(Of Screen)()
            screen = dbHelper.[Get](Of Screen)(command, "GetScreen")
            Return screen
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function ValidateScreenAccessForUser(ByVal userId As Integer, ByVal screenId As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userId
            command.Parameters.Add(dbDataParameter)

            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter2.ParameterName = "ScreenID"
            dbDataParameter2.Value = screenId
            command.Parameters.Add(dbDataParameter2)

            Dim userCount = dbHelper.ExecuteScalar(command, "ValidateScreenAccessForUser")
            Return userCount

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

End Class
