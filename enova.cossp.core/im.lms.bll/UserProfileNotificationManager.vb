﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO


Public Class UserProfileNotificationManager
    Public Function GetListOfAllUserProfileNotification() As List(Of UserProfileNotification)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim notificationSetting As List(Of UserProfileNotification) = New List(Of UserProfileNotification)()
            notificationSetting = dbHelper.[Get](Of UserProfileNotification)(Command, "GetListOfAllUserProfileNotification")
            Return notificationSetting
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
