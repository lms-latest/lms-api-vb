﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports enova.cossp.Common.Lib.CryptoHelper

Imports System.Text
Imports System.Configuration

Public Class SettingManager
    Dim ignoreList As String() = {"modifiedDate", "key"}
    Public Function GetListOfSetting() As List(Of Setting)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim setting As List(Of Setting) = New List(Of Setting)()
            setting = dbHelper.[Get](Of Setting)(command, "GetListOfSetting")
            Return setting

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


    Public Sub UpdateSetting(ByVal userResponse As Setting)
        Try
            Dim Encryptionkey = ConfigManager.GetEncryptionKey()
            Dim SettingEmail = ConfigManager.GetSettingEmail()
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim mykey As String() = ConfigurationManager.AppSettings("SensitiveKeys").Split(","c)
            For Each key In mykey
                If (key.Equals(userResponse.key)) Then
                    userResponse.value = Encrypt(userResponse.value, Encryptionkey)
                    userResponse.IsEncrypted = True
                End If
            Next
            dbHelper.Save(userResponse, ignoreList)
            SendSettingSaveEmail(SettingEmail, userResponse.key, "Moderator")
            DBConfigurationManager.CacheRefersh()


        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Sub
    Public Function SendSettingSaveEmail(ByVal SettingEmail As String, ByVal key As String, ByVal name As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "SettingEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForSettingSaveEmail()
            emailBody = GetEmailBodyForSettingSave(body, key, name)
            EmailHelper.SendEmail(emailSubject, emailBody, SettingEmail)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Private Function GetEmailBodyForSettingSave(ByVal body As String, ByVal key As String, ByVal name As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()

            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@Message@@", key)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetSettingByID(ByVal ID As Integer) As Setting
        Try
            Dim Encryptionkey = ConfigManager.GetEncryptionKey()
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim setting As Setting = New Setting()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = ID
            command.Parameters.Add(dbDataParameter)
            setting = dbHelper.[Get](Of Setting)(command, "GetSettingByID").FirstOrDefault()
            If (setting.IsEncrypted.Equals(True)) Then
                setting.value = Decrypt(setting.value, Encryptionkey)
            End If

            Return setting
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
End Class
