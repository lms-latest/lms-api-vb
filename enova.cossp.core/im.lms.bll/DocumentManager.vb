﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class DocumentManager

    Dim ignoreList As String() = {"CreatedDate", "CreatedBy", "ModifiedBy", "ModifiedDate", "contract", "sentTo", "dateSent", "documentCreationDate", "Active", "ID", "ContractID", "guid", "dateFrom", "dateTo"}
    Dim ignoreListPaging As String() = {}



    Public Function SendDocumentRecievedEmail(ByVal email As String, ByVal name As String, ByVal documentName As String, ByVal from As String, ByVal path As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "DocumentRecievedTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForDocumentRecievedEmail()
            emailBody = GetEmailBodyForDocumentRecieved(body, name, documentName, from)
            EmailHelper.SendEmail(emailSubject, emailBody, email, path)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Private Function GetEmailBodyForDocumentRecieved(ByVal body As String, ByVal name As String, ByVal documentName As String, ByVal from As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()
            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@DocumentName@@", documentName)
            emailBodyHtml.Replace("@@from@@", from)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Sub SaveDocument(ByVal document As Document)
        Try
            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim user As User = New User()
            user = userManager.GetUserByUserName(userName)
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            extraParams.Add("@ContractID", document.contract.ID)
            extraParams.Add("@RecipientID", document.sentTo.ID)

            Dim result = dbHelper.Save(document, extraParams, ignoreList)
            Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/Documents/")
            Dim newName = UserManager.RenameFileName(document.guid, result(0).name, folderPath)
            SendDocumentRecievedEmail(document.sentTo.Email, document.sentTo.Name, document.name, user.userName, folderPath + newName)

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub
    Public Function GetListOfDocumentRecipients() As List(Of DocumentRecipient)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim documentRecipients As List(Of DocumentRecipient) = New List(Of DocumentRecipient)()
            documentRecipients = dbHelper.[Get](Of DocumentRecipient)(command, "GetDocumentRecipients")
            Return documentRecipients
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetUserSentDocuments(id As Integer?, filters As ContractFilter, isAllDocument As Integer
                                         ) As PaggedResult(Of DocumentInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim dbDataParameter3 As IDbDataParameter = command.CreateParameter()
            dbDataParameter3.ParameterName = "IsAllDocument"
            dbDataParameter3.Value = isAllDocument
            dbDataParameter3.DbType = DbType.Int32
            command.Parameters.Add(dbDataParameter3)


            Dim info() As PropertyInfo = filters.GetType().GetProperties()
            For Each prop In info
                If Not ignoreList.Contains(prop.Name) Then
                    Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
                    dbDataParameter2.ParameterName = prop.Name
                    dbDataParameter2.Value = prop.GetValue(filters, Nothing)
                    command.Parameters.Add(dbDataParameter2)
                End If

            Next
            If (Not filters.ContractID Is Nothing) Then
                Dim dtContract As DataTable = New DataTable()

                dtContract.Columns.Add("ID")
                For Each item In filters.ContractID
                    dtContract.Rows.Add(item
                                        )

                Next
                Dim ContractIDs As IDbDataParameter = command.CreateParameter()
                ContractIDs.ParameterName = "ContractIDs"
                ContractIDs.Value = dtContract

                command.Parameters.Add(ContractIDs)

            End If



            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)

            Dim documentInfo As List(Of DocumentInfo) = New List(Of DocumentInfo)()
            documentInfo = dbHelper.[Get](Of DocumentInfo)(command, "GetUserSentDocuments")
            Dim paggedResult As New PaggedResult(Of DocumentInfo)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = documentInfo
            Return paggedResult
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetUserRecievedDocuments(id As Integer?, filters As ContractFilter, isAllDocument As Integer) As PaggedResult(Of DocumentInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim dbDataParameter3 As IDbDataParameter = command.CreateParameter()
            dbDataParameter3.ParameterName = "IsAllDocument"
            dbDataParameter3.Value = isAllDocument
            command.Parameters.Add(dbDataParameter3)
            'Dim info() As PropertyInfo = filters.GetType().GetProperties()
            'For Each prop In info
            '    Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            '    dbDataParameter2.ParameterName = prop.Name
            '    dbDataParameter2.Value = prop.GetValue(filters, Nothing)
            '    command.Parameters.Add(dbDataParameter2)
            'Next
            Dim info() As PropertyInfo = filters.GetType().GetProperties()
            For Each prop In info
                If Not ignoreList.Contains(prop.Name) Then
                    Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
                    dbDataParameter2.ParameterName = prop.Name
                    dbDataParameter2.Value = prop.GetValue(filters, Nothing)
                    command.Parameters.Add(dbDataParameter2)
                End If

            Next
            If (Not filters.ContractID Is Nothing) Then
                Dim dtContract As DataTable = New DataTable()

                dtContract.Columns.Add("ID")
                For Each item In filters.ContractID
                    dtContract.Rows.Add(item
                                        )

                Next
                Dim ContractIDs As IDbDataParameter = command.CreateParameter()
                ContractIDs.ParameterName = "ContractIDs"
                ContractIDs.Value = dtContract

                command.Parameters.Add(ContractIDs)

            End If
            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)

            Dim documentInfo As List(Of DocumentInfo) = New List(Of DocumentInfo)()
            documentInfo = dbHelper.[Get](Of DocumentInfo)(command, "GetUserRecievedDocuments")

            Dim paggedResult As New PaggedResult(Of DocumentInfo)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = documentInfo
            Return paggedResult
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetUserSentDocumentsByContract(userID As Integer, contractID As Integer) As List(Of DocumentInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter1 As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter1.ParameterName = "ID"
            dbDataParameter1.Value = userID

            command.Parameters.Add(dbDataParameter1)
            dbDataParameter2.ParameterName = "ContractID"
            dbDataParameter2.Value = contractID

            command.Parameters.Add(dbDataParameter2)

            Dim documentInfo As List(Of DocumentInfo) = New List(Of DocumentInfo)()
            documentInfo = dbHelper.[Get](Of DocumentInfo)(command, "GetUserSentDocumentsByContract")
            Return documentInfo
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetUserRecievedDocumentsByContract(userID As Integer, contractID As Integer) As List(Of DocumentInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter1 As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter1.ParameterName = "ID"
            dbDataParameter1.Value = userID
            command.Parameters.Add(dbDataParameter1)
            dbDataParameter2.ParameterName = "ContractID"
            dbDataParameter2.Value = contractID
            command.Parameters.Add(dbDataParameter2)
            Dim documentInfo As List(Of DocumentInfo) = New List(Of DocumentInfo)()
            documentInfo = dbHelper.[Get](Of DocumentInfo)(command, "GetUserRecievedDocumentsByContract")
            Return documentInfo
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function CheckUserAccessSentDocument(userID As Integer, DocID As Integer, DocName As String, IsAllDocument As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter3 As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter4 As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            dbDataParameter2.ParameterName = "DocID"
            dbDataParameter2.Value = DocID
            dbDataParameter3.ParameterName = "DocName"
            dbDataParameter3.Value = DocName
            dbDataParameter4.ParameterName = "IsAllDocument"
            dbDataParameter4.Value = IsAllDocument
            command.Parameters.Add(dbDataParameter)
            command.Parameters.Add(dbDataParameter2)
            command.Parameters.Add(dbDataParameter3)
            command.Parameters.Add(dbDataParameter4)
            Dim result As Integer = New Integer()

            result = dbHelper.ExecuteScalar(command, "UserAccessSentDocuments")
            Return result
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function CheckUserAccessRecievedDocument(userID As Integer, DocID As Integer, DocName As String, IsAllDocument As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter3 As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter4 As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            dbDataParameter2.ParameterName = "DocID"
            dbDataParameter2.Value = DocID
            dbDataParameter3.ParameterName = "DocName"
            dbDataParameter3.Value = DocName
            dbDataParameter4.ParameterName = "IsAllDocument"
            dbDataParameter4.Value = userID
            command.Parameters.Add(dbDataParameter)
            command.Parameters.Add(dbDataParameter2)
            command.Parameters.Add(dbDataParameter3)
            command.Parameters.Add(dbDataParameter4)
            Dim result As Integer = New Integer()

            result = dbHelper.ExecuteScalar(command, "UserAccessRecievedDocuments")
            Return result
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function PostInvoice(invoice As Invoice)

        Try
            Dim result = GetNewFileName(invoice)

            Dim bytes = Convert.FromBase64String(invoice.FileContent)
            Dim Path = ConfigManager.DocumentFolderPath + result
            File.WriteAllBytes(Path, bytes)
            'get users that have contract assigned and access to all document screen

            Dim contract = GetContractByConSeq(invoice.ConSeq)
            Dim UserResults = GetUsersForInvoiceEmail(invoice.ConSeq)
            Dim userprofilemanager As UserProfileManager = New UserProfileManager()
            For Each userinfo In UserResults
                Dim preference = userprofilemanager.GetUserProfileNotificationByUserID(userinfo.ID)
                If preference.Count <> 0 Then
                    userinfo.Preference = preference
                    If userinfo.Preference.Exists(Function(x) (x.PreferenceID = NotificationPreferenceEnum.EMAIL Or x.PreferenceID = NotificationPreferenceEnum.BOTH) AndAlso x.ID = CInt(NotificationSettingEnum.INVOICEDOCUMENTS)) Then
                        SendInvoiceEmail(userinfo.email, userinfo.Name, invoice.FileName, Path, contract.Name
                                             )
                    End If
                End If
            Next
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetNewFileName(record As Invoice) As String

        Try
            Dim dbHelperObj As DBHelper = New DBHelper()

            Dim info() As PropertyInfo = record.GetType().GetProperties()
            Dim command As IDbCommand = dbHelperObj.GetCommandObject()
            For Each prop In info
                If prop.Name <> "FileContent" Then

                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = prop.Name
                    dbDataParameter.Value = prop.GetValue(record, Nothing)
                    command.Parameters.Add(dbDataParameter)
                End If
            Next

            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "DocumentCosspApiUpdate"
            Dim result = dbHelperObj.GetDataTable(command)
            Dim field As String = result.Rows(0).Field(Of String)(0)
            Dim InsertCount As Integer = result.Rows(0).Field(Of Integer)(1)


            Return field

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function SendInvoiceEmail(ByVal email As String, ByVal name As String, ByVal invoiceName As String, ByVal Path As String, ByVal contractName As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "InvoiceRecievedEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForRecievedInvoiveEmail() + " for " + contractName
            emailBody = GetEmailBodyForInvoice(body, name, invoiceName)
            EmailHelper.SendEmail(emailSubject, emailBody, email, Path)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function GetEmailBodyForInvoice(ByVal body As String, ByVal name As String, ByVal invoiceName As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()
            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@InvoiceName@@", invoiceName)
            emailBodyHtml.Replace("@@URL@@", url)


            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


    Public Function GetUsersForInvoiceEmail(conseq As Integer) As List(Of UserInfo)

        Try
            Dim allDocScreenId = ConfigManager.AllDocScreenID()
            Dim dbHelperObj As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelperObj.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ConSeq"
            dbDataParameter.Value = conseq
            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter2.ParameterName = "ScreenID"
            dbDataParameter2.Value = allDocScreenId
            command.Parameters.Add(dbDataParameter)
            command.Parameters.Add(dbDataParameter2)
            Return dbHelperObj.[Get](Of UserInfo)(command, "GetUsersForInvoiceEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function GetContractByConSeq(conseq As Integer) As Contract

        Try
            Dim dbHelperObj As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelperObj.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ConSeq"
            dbDataParameter.Value = conseq
            command.Parameters.Add(dbDataParameter)
            Return dbHelperObj.[Get](Of Contract)(command, "GetContractByConSeq").FirstOrDefault()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
