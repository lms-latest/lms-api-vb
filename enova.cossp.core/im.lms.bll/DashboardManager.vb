﻿Imports enova.cossp.Common.Lib
Imports enova.cossp.dal
Imports enova.cossp.entities

Public Class DashboardManager
    Dim ignoreList As String() = {""}

    Public Function GetDashboardWidgets(ByVal widgetSizeId As String, userId As Integer) As List(Of WidgetInfo)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "SizeID"
            parameter1.Value = widgetSizeId
            command.Parameters.Add(parameter1)

            Dim parameter2 As IDbDataParameter = command.CreateParameter()
            parameter2.ParameterName = "UserID"
            parameter2.Value = userId
            command.Parameters.Add(parameter2)

            Dim parameter3 As IDbDataParameter = command.CreateParameter()
            parameter3.ParameterName = "TypeID"
            parameter3.Value = CInt(ConfigManager.DashboardWidgetTypeID())
            command.Parameters.Add(parameter3)

            Return dbHelper.[Get](Of WidgetInfo)(command, "GetDashboardWidget")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Function GetUserScreenSection(ByVal screenID As String, ByVal userID As Integer) As List(Of ScreenSection)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "ScreenID"
            parameter1.Value = CInt(screenID)
            command.Parameters.Add(parameter1)

            Dim parameter2 As IDbDataParameter = command.CreateParameter()
            parameter2.ParameterName = "UserID"
            parameter2.Value = userID
            command.Parameters.Add(parameter2)

            Return dbHelper.[Get](Of ScreenSection)(command, "GetScreenSection")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Sub SaveUserDasboardSection(ByVal sectionDetails As UserScreenSection)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()

            dbHelper.Save(sectionDetails, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Sub

End Class
