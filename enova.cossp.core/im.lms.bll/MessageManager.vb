﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text
Imports System.Web
Imports System.IO

Public Class MessageManager

    Dim ignoreList As String() = {"Active", "CreatedDate", "CreatedBy", "ModifiedBy", "ModifiedDate", "DateSent", "DateViewed", "SendOrReceived", "OtherUserName", "profilePicture", "FormattedDateSent", "FormattedDateViewed"}

    Public Sub SaveMessage(ByVal message As Message)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            dbHelper.Save(message, ignoreList)
            Dim userManager As UserManager = New UserManager()
            Dim userprofilemanager As UserProfileManager = New UserProfileManager()
            Dim userDetail = userManager.GetUserByID(message.ToUserID)
            Dim fromUserDetail = userManager.GetUserByID(message.FromUserID)
            Dim preference = userprofilemanager.GetUserProfileNotificationByUserID(message.ToUserID)
            If message.IsViewed = False Then
                If preference.Count <> 0 Then
                    If preference.Exists(Function(x) (x.PreferenceID = NotificationPreferenceEnum.EMAIL Or x.PreferenceID = NotificationPreferenceEnum.BOTH) AndAlso x.ID = CInt(NotificationSettingEnum.MESSAGES)) Then
                        SendMessageSaveEmail(userDetail.email, message.Message, userDetail.Name, fromUserDetail.Name)
                    End If
                End If
            End If


        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub

    Public Function GetUserByUserName(ByVal user As String) As MessageUserInfo
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserName"
            dbDataParameter.Value = user
            command.Parameters.Add(dbDataParameter)
            Dim messageUserInfo As MessageUserInfo = New MessageUserInfo()
            messageUserInfo = dbHelper.[Get](Of MessageUserInfo)(command, "GetUserByUserName").FirstOrDefault()
            messageUserInfo.ExchangeMessageWith = GetUserMappingByUser(messageUserInfo.ID)
            messageUserInfo.profilePicture = CheckIfFileExists(messageUserInfo.profilePicture)
            Return messageUserInfo
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function CheckIfFileExists(ByVal fileName As String) As String
        Try
            Dim defaultUserProfileImage = ConfigManager.DefaultUserProfileImage()
            Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
            Dim filePath = folderPath + fileName
            If (File.Exists(filePath)) Then
                Return fileName
            End If

            Return defaultUserProfileImage
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetUserMappingByUser(userID As Integer) As List(Of MessageUserInfo)
        Try
            Dim userManager As UserManager = New UserManager()
            Dim message As MessageUserInfo = New MessageUserInfo()
            Dim user = userManager.GetUserByID(userID)
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim dbDataParameter1 As IDbDataParameter = command.CreateParameter()
            dbDataParameter1.ParameterName = "MessageAllowed"
            dbDataParameter1.Value = user.MessageAllowed
            command.Parameters.Add(dbDataParameter1)
            Dim messageUserInfo As List(Of MessageUserInfo) = New List(Of MessageUserInfo)()
            messageUserInfo = dbHelper.[Get](Of MessageUserInfo)(command, "GetUserMappingByUser")
            For Each message In messageUserInfo
                message.profilePicture = CheckIfFileExists(message.profilePicture)
            Next
            Return messageUserInfo
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfMessagesByID(id As Integer) As List(Of Message)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = id
            command.Parameters.Add(dbDataParameter)
            Dim message As Message = New Message()
            Dim messages As List(Of Message) = New List(Of Message)()
            messages = dbHelper.[Get](Of Message)(command, "GetMessageByUserID")
            For Each message In messages
                message.profilePicture = CheckIfFileExists(message.profilePicture)
            Next

            Return messages
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function SendMessageSaveEmail(ByVal email As String, ByVal message As String, ByVal name As String, ByVal fromUsername As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "MessageCreationEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForMessageSaveEmail()
            emailBody = GetEmailBodyForMessageSave(body, message, name, fromUsername)
            EmailHelper.SendEmail(emailSubject, emailBody, email)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Private Function GetEmailBodyForMessageSave(ByVal body As String, ByVal message As String, ByVal name As String, ByVal fromUsername As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()

            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()
            emailBodyHtml.Replace("@@FromUserName@@", fromUsername)
            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@Message@@", message)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


End Class
