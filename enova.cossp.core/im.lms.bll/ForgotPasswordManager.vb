﻿Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib
Imports System.Text

Public Class ForgotPasswordManager

    Public Function SendResetPasswordLink(ByVal userEmail As String, ByVal resetUrl As String, ByVal name As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "ResetPasswordEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForResetPasswordEmail()
            emailBody = GetEmailBodyForResetPasswordLink(body, resetUrl, name)
            EmailHelper.SendEmail(emailSubject, emailBody, userEmail)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function SendEmailForUpdatedPassordConfirmation(ByVal userEmail As String, ByVal details As DetailsForPasswordUpdationEmail)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "UpdatedPasswordEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForUpdatePasswordConfirmationEmail()
            emailBody = GetEmailBodyForUpdatedPassword(body, details)
            EmailHelper.SendEmail(emailSubject, emailBody, userEmail)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Private Function GetEmailBodyForResetPasswordLink(ByVal body As String, ByVal resetUrl As String, ByVal name As String) As String
        Try
            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", name)
            emailBodyHtml.Replace("@@Link@@", resetUrl)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Private Function GetEmailBodyForUpdatedPassword(ByVal body As String, ByVal details As DetailsForPasswordUpdationEmail) As String
        Try
            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", details.Name)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function ValidateEmailAndSaveToken(ByVal userTokenDetails As UserTokenDetails)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "Email"
            parameter1.Value = userTokenDetails.Email
            command.Parameters.Add(parameter1)

            Dim parameter2 As IDbDataParameter = command.CreateParameter()
            parameter2.ParameterName = "Token"
            parameter2.Value = userTokenDetails.Token
            command.Parameters.Add(parameter2)

            Dim parameter3 As IDbDataParameter = command.CreateParameter()
            parameter3.ParameterName = "TokenExpireOn"
            parameter3.Value = userTokenDetails.TokenExpiresOn
            command.Parameters.Add(parameter3)

            'Return dbHelper.ExecuteScalar(command, "ValidateEmailandSaveToken")
            Return dbHelper.[Get](Of User)(command, "ValidateEmailandSaveToken").FirstOrDefault()

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function ValidateForgotPasswordToken(ByVal token As String) As Integer

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "Token"
            parameter1.Value = token
            command.Parameters.Add(parameter1)

            Return dbHelper.ExecuteScalar(command, "ValidatePasswordToken")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function ChangeUserPassword(ByVal newPasswordDetails As NewPasswordDetails)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim parameter1 As IDbDataParameter = command.CreateParameter()
            parameter1.ParameterName = "Token"
            parameter1.Value = newPasswordDetails.Token
            command.Parameters.Add(parameter1)

            Dim parameter2 As IDbDataParameter = command.CreateParameter()
            parameter2.ParameterName = "NewPassword"
            parameter2.Value = newPasswordDetails.NewPassword
            command.Parameters.Add(parameter2)

            'Return dbHelper.ExecuteScalar(command, "UpdateUserPassword")
            Return dbHelper.[Get](Of User)(command, "UpdateUserPassword").FirstOrDefault()

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

End Class
