﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Security
Imports System.Reflection
Imports enova.cossp.Common.Lib
Imports System.Text

Public Class PollManager


    Dim ignoreList As String() = {"CreatedDate", "ModifiedBy", "ModifiedDate", "pollOption", "visibleTo", "count",
        "first", "size", "sortBy", "sortOrderBy", "searchString"}
    Dim ignoreListPaging As String() = {"dateFrom", "dateTo"}

    Public Function GetDataTable(Of T As New)(ByVal entityList As List(Of T)) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            dt.Columns.Add("ID", GetType(Integer))
            dt.Columns.Add("Name", GetType(String))

            Dim tentity As Type = GetType(T)
            Dim propertyInfoID As PropertyInfo = tentity.GetProperty("ID")
            Dim propertyInfoName As PropertyInfo = tentity.GetProperty("Name")
            For Each entity As T In entityList
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = propertyInfoID.GetValue(entity, Nothing)
                dr("Name") = propertyInfoName.GetValue(entity, Nothing)
                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()
            Return dt
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    'Public Function SendPollSaveEmail(ByVal emails As List(Of String), ByVal name As String)
    '    Try
    '        Dim emailBody As String = ""
    '        Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "PollCreationEmailTemplate.html")
    '        Dim emailSubject = ConfigManager.SubjectForPollSaveEmail()
    '        emailBody = GetEmailBodyForPollSave(body, name)
    '        EmailHelper.SendEmailMultiple(emailSubject, emailBody, emails)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try


    'End Function
    Public Function SendPollSaveEmail(ByVal email As String, ByVal name As String, ByVal username As String)
        Try
            Dim emailBody As String = ""
            Dim body = EmailHelper.ReadEmailTemplate(ConfigManager.EmailTemplatesPath() + "PollCreationEmailTemplate.html")
            Dim emailSubject = ConfigManager.SubjectForPollSaveEmail()
            emailBody = GetEmailBodyForPollSave(body, name, username)
            EmailHelper.SendEmail(emailSubject, emailBody, email)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Private Function GetEmailBodyForPollSave(ByVal body As String, ByVal name As String, ByVal username As String) As String
        Try
            Dim url = ConfigManager.WebUrlForEnova()

            Dim emailBodyHtml As StringBuilder = New StringBuilder()
            emailBodyHtml.Append(body).ToString()

            emailBodyHtml.Replace("@@Name@@", username)
            emailBodyHtml.Replace("@@PollName@@", name)
            emailBodyHtml.Replace("@@URL@@", url)

            Return emailBodyHtml.ToString()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Sub SavePoll(ByVal poll As Poll)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim extraParams As Hashtable = New Hashtable()
            extraParams.Add("@RoleList", GetDataTable(poll.visibleTo))
            extraParams.Add("@OptionList", GetDataTable(poll.pollOption))
            dbHelper.Save(poll, extraParams, ignoreList)
            'Dim roles As DataTable = New DataTable()
            'roles = GetDataTable(poll.visibleTo)
            'Dim result As List(Of UserInfo) = GetUsersByRoles(roles)
            'Dim userprofilemanager As UserProfileManager = New UserProfileManager()
            ''Dim emails As List(Of String) = result.[Select](Function(o) o.email).ToList()
            ''Dim testemails As List(Of String) = New List(Of String)(New String() {"ajita.pancholi@infobeans.com", "dharmendra.baghel@infobeans.com", "apsys10@gmail.com"})
            'For Each userinfo In result
            '    Dim preference = userprofilemanager.GetUserProfileNotificationByUserID(userinfo.ID)
            '    If preference.Count <> 0 Then
            '        userinfo.Preference = preference
            '        If userinfo.Preference.Exists(Function(x) (x.PreferenceID = NotificationPreferenceEnum.EMAIL Or NotificationPreferenceEnum.BOTH) AndAlso x.ID = CInt(NotificationSettingEnum.POLLS)) Then
            '            SendPollSaveEmail(userinfo.email, poll.name, userinfo.FirstName + " " + userinfo.LastName)
            '        End If
            '    End If


            'Next

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub
    Public Sub SavePollResult(ByVal pollResult As PollResult)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            dbHelper.Save(pollResult, ignoreList)

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Sub

    Public Function GetListOfPoll(ByVal filters As Pagging) As PaggedResult(Of Poll)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dictionary As New Dictionary(Of String, Object)

            Dim info() As PropertyInfo = filters.GetType().GetProperties()
            For Each prop In info
                If Not ignoreListPaging.Contains(prop.Name) Then
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = prop.Name
                    dbDataParameter.Value = prop.GetValue(filters, Nothing)
                    command.Parameters.Add(dbDataParameter)
                End If
            Next
            Dim dbDataParameterTotalRecords As IDbDataParameter = command.CreateParameter()
            dbDataParameterTotalRecords.Direction = ParameterDirection.Output
            dbDataParameterTotalRecords.ParameterName = "TotalRecords"
            dbDataParameterTotalRecords.DbType = DbType.Int32
            dbDataParameterTotalRecords.Value = 1
            command.Parameters.Add(dbDataParameterTotalRecords)


            Dim poll As List(Of Poll) = New List(Of Poll)()
            poll = dbHelper.[Get](Of Poll)(command, "GetPoll")
            Dim paggedResult As New PaggedResult(Of Poll)
            paggedResult.Count = dbDataParameterTotalRecords.Value
            paggedResult.Result = poll
            Return paggedResult

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetPollByID(ByVal pollID As Integer) As Poll
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim poll As Poll = New Poll()
            poll = dbHelper.[Get](Of Poll)(command, "GetPollByID").FirstOrDefault()
            poll.pollOption = GetListOfOptionsByPoll(pollID)
            poll.visibleTo = GetListOfAllowedRolesByPoll(pollID)
            Return poll
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfOptionsByPoll(pollID As Integer) As List(Of PollOption)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "PollID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim options As List(Of PollOption) = New List(Of PollOption)()
            options = dbHelper.[Get](Of PollOption)(command, "GetOptionsByPoll")
            Return options
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Function GetListOfAllowedRolesByPoll(pollID As Integer) As List(Of Role)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "PollID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim roles As List(Of Role) = New List(Of Role)()
            roles = dbHelper.[Get](Of Role)(command, "GetAllowedRolesByPoll")
            Return roles
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Function DeletePollByID(ByVal pollID As Integer) As Poll
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim poll As Poll = New Poll()
            poll = dbHelper.[Get](Of Poll)(command, "PollDelete").FirstOrDefault()
            Return poll
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Public Function GetPollResultReportByPollID(pollID As Integer) As List(Of PollResultReport)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ID"
            dbDataParameter.Value = pollID
            command.Parameters.Add(dbDataParameter)
            Dim result As List(Of PollResultReport) = New List(Of PollResultReport)()
            result = dbHelper.[Get](Of PollResultReport)(command, "GetPollResultReportByPollID")
            Return result
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function
    Public Function CheckUserResponseOnPoll(userID As Integer?, pollID As Integer) As Integer
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim dbDataParameter2 As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "PollID"
            dbDataParameter.Value = pollID
            dbDataParameter2.ParameterName = "UserID"
            dbDataParameter2.Value = userID
            command.Parameters.Add(dbDataParameter)
            command.Parameters.Add(dbDataParameter2)
            Dim result As Integer = New Integer()
            'result = dbHelper.[Get](Of Integer)(command, "CheckUserResponseOnPoll").FirstOrDefault()
            result = dbHelper.ExecuteScalar(command, "CheckUserResponseOnPoll")
            Return result
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try


    End Function

    Public Shared Function GetUsersByRoles(ByVal roleList As DataTable) As List(Of UserInfo)

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "RoleList"
            dbDataParameter.Value = roleList
            command.Parameters.Add(dbDataParameter)
            Dim result As List(Of UserInfo) = New List(Of UserInfo)()
            result = dbHelper.[Get](Of UserInfo)(command, "GetUsersByRoles")

            Return result

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


End Class
