﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class JobTitleManager

    Public Function GetListOfJobTitle() As List(Of JobTitle)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim jobTitle As List(Of JobTitle) = New List(Of JobTitle)()
            jobTitle = dbHelper.[Get](Of JobTitle)(command, "GetJobTitle")
            Return jobTitle
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
