﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class TaskManager
    Dim ignoreList As String() = {"ID", "Active", "CreatedDate", "CreatedBy", "ModifiedBy", "ModifiedDate", "TaskId", "Type", "Priority", "DateCreated", "DateClosed", "Status"}
    Public Function GetTask() As List(Of Task)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim task As List(Of Task) = New List(Of Task)()
            task = dbHelper.[Get](Of Task)(command, "GetTask")
            Return task
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function SaveTask(ByVal task As Task)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            dbHelper.Save(task, ignoreList)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfTaskType() As List(Of TaskType)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim taskTypeList As List(Of TaskType) = New List(Of TaskType)()
            taskTypeList = dbHelper.[Get](Of TaskType)(command, "GetTaskType")
            Return taskTypeList
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Function GetListOfTaskStatus() As List(Of TaskStatus)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()

            Dim taskStatusList As List(Of TaskStatus) = New List(Of TaskStatus)()
            taskStatusList = dbHelper.[Get](Of TaskStatus)(command, "GetTaskStatus")
            Return taskStatusList
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Function GetListOfBuildingByUser(userID As Integer, IsAllBuilding As Boolean, contractID As String) As List(Of Building)
        Try

            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "UserID"
            dbDataParameter.Value = userID
            command.Parameters.Add(dbDataParameter)
            Dim dbIsAllBuildingDataParameter As IDbDataParameter = command.CreateParameter()
            dbIsAllBuildingDataParameter.ParameterName = "IsAllBuilding"
            dbIsAllBuildingDataParameter.Value = IsAllBuilding
            command.Parameters.Add(dbIsAllBuildingDataParameter)
            Dim contractIDdbDataParameter As IDbDataParameter = command.CreateParameter()
            contractIDdbDataParameter.ParameterName = "FilterByContractID"
            contractIDdbDataParameter.Value = contractID
            command.Parameters.Add(contractIDdbDataParameter)
            Dim buildings As List(Of Building) = New List(Of Building)()
            buildings = dbHelper.[Get](Of Building)(command, "GetBuildingByUserContract")
            Return buildings
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

End Class
