﻿Imports System.Data
Imports System.Collections.Generic
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Public Class ProblemManager
    Public Function GetListOfProblem() As List(Of Problem)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            Dim problem As List(Of Problem) = New List(Of Problem)()
            problem = dbHelper.[Get](Of Problem)(command, "GetProblem")
            Return problem
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    'Public Function GetProblemByContract(ByVal contactId As Integer) As Problem
    '    Dim dbHelper As DBHelper = New DBHelper()
    '    Dim command As IDbCommand = dbHelper.GetCommandObject()
    '    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
    '    dbDataParameter.ParameterName = "ContactId"
    '    dbDataParameter.Value = contactId
    '    command.Parameters.Add(dbDataParameter)
    '    Dim problem As Problem = New Problem()
    '    problem = dbHelper.[Get](Of Problem)(command, "GetProblemByContactId").FirstOrDefault()
    '    Return problem

    'End Function


    Public Function GetProblemByContract(ByVal contactId As String) As List(Of Problem)
        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
            dbDataParameter.ParameterName = "ContactId"
            dbDataParameter.Value = contactId
            command.Parameters.Add(dbDataParameter)
            Dim problem As List(Of Problem) = New List(Of Problem)()
            problem = dbHelper.[Get](Of Problem)(command, "GetProblemByContactId")
            Return problem
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

End Class
