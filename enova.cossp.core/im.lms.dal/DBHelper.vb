﻿Imports System
Imports System.Data
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration
Imports log4net
Imports System.Collections.Generic
Imports System.Reflection
Imports System.Linq

Public Class DBHelper
    Private _connectionString As String = ConfigurationManager.ConnectionStrings("Db").ToString()
    Private _sqlConn As SqlConnection

    Public Sub New()
    End Sub

    Public Function OpenConnection() As SqlConnection
        SqlConnection.ClearAllPools()

        If _connectionString <> String.Empty Then
            _sqlConn = New SqlConnection(_connectionString)

            If _sqlConn IsNot Nothing Then
                If _sqlConn.State = System.Data.ConnectionState.Closed Then _sqlConn.Open()
            End If
        End If

        Return _sqlConn
    End Function

    Public Sub CloseConnection()
        If _sqlConn IsNot Nothing Then
            If _sqlConn.State <> System.Data.ConnectionState.Closed Then _sqlConn.Close()
        End If
    End Sub

    Public Sub DisposeConnection()
        If _sqlConn IsNot Nothing Then
            If _sqlConn.State <> System.Data.ConnectionState.Closed Then _sqlConn.Dispose()
        End If
    End Sub

    Public Function GetDataTable(ByVal _sqlCmd As IDbCommand) As DataTable
        Dim oDt As DataTable = New DataTable()

        Try

            Using sqlCon As SqlConnection = New SqlConnection(_connectionString)

                If _sqlCmd IsNot Nothing Then
                    _sqlCmd.Connection = sqlCon
                End If

                Dim oDa As SqlDataAdapter = New SqlDataAdapter(CType(_sqlCmd, SqlCommand))
                oDa.Fill(oDt)
                _sqlCmd = Nothing
            End Using

        Catch ex As Exception
            ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Finally
        End Try

        Return oDt
    End Function

    Public Function ExecuteQuery(ByVal _sqlCmd As IDbCommand) As Boolean
        Dim oResult As Boolean = False

        Try

            If _sqlCmd IsNot Nothing Then
                _sqlCmd.Connection = OpenConnection()
            End If

            _sqlCmd.ExecuteNonQuery()
            oResult = True
        Catch ex As Exception
            ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
            Throw ex
        Finally
            CloseConnection()
        End Try

        Return oResult
    End Function

    Public Function ExecuteScalar(ByVal _sqlCmd As IDbCommand, ByVal Optional tableName As String = Nothing) As Integer
        Dim sResult As Integer

        _sqlCmd.CommandType = CommandType.StoredProcedure
        If String.IsNullOrEmpty(_sqlCmd.CommandText) Then _sqlCmd.CommandText = String.Format(tableName)

        Try

            If _sqlCmd IsNot Nothing Then
                _sqlCmd.Connection = OpenConnection()
            End If

            sResult = _sqlCmd.ExecuteScalar()
        Catch ex As Exception
            ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Finally
            CloseConnection()
        End Try

        Return sResult
    End Function

    Public Function GetDataset(ByVal _sqlCmd As IDbCommand) As DataSet
        Dim objDataSet As DataSet = New DataSet()

        Try

            Using sqlCon As SqlConnection = New SqlConnection(_connectionString)

                If _sqlCmd IsNot Nothing Then
                    _sqlCmd.Connection = sqlCon
                End If

                Dim oDa As IDbDataAdapter = New SqlDataAdapter(CType(_sqlCmd, SqlCommand))
                oDa.Fill(objDataSet)
                _sqlCmd = Nothing
            End Using

        Catch ex As Exception
            ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Finally
        End Try

        Return objDataSet
    End Function

    Public Function ExecuteReader(ByVal cmd As IDbCommand) As IDataReader
        Dim result As IDataReader = Nothing

        Try

            If cmd IsNot Nothing Then
                cmd.Connection = OpenConnection()
            End If

            result = cmd.ExecuteReader()
        Catch ex As Exception
            ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        End Try

        Return result
    End Function

    Public Function [Get](Of T As New)(ByVal Optional tableName As String = Nothing) As List(Of T)
        Dim tget As Type = GetType(T)
        Dim returnObject As T = New T()
        Dim command As IDbCommand = GetCommandObject()
        command.CommandType = CommandType.StoredProcedure

        If String.IsNullOrEmpty(tableName) Then
            command.CommandText = String.Format("{0}Get", tget.Name)
        Else
            command.CommandText = String.Format("{0}Get", tableName)
        End If

        Return Me.[Get](Of T)(command)
    End Function

    Public Function [Get](Of T As New)(ByVal command As IDbCommand, ByVal Optional tableName As String = Nothing) As List(Of T)
        Dim tget1 As Type = GetType(T)
        Dim returnObject As T = New T()
        command.CommandType = CommandType.StoredProcedure
        If String.IsNullOrEmpty(command.CommandText) Then command.CommandText = String.Format(tableName)
        Return Me.[Get](Of T)(command)
    End Function

    Public Function Save(Of T As New)(ByVal entity As T, ParamArray ignoreList As String()) As List(Of T)
        Dim tsave As Type = GetType(T)
        Dim returnObject As T = New T()
        Dim command As IDbCommand = GetCommandObject()
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = String.Format("{0}Save", tsave.Name)
        Dim propertyInfoList As PropertyInfo() = tsave.GetProperties(BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

        If propertyInfoList IsNot Nothing Then

            For Each propertyInfo As PropertyInfo In propertyInfoList

                If Not ignoreList.Contains(propertyInfo.Name) Then
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = propertyInfo.Name

                    If propertyInfo.GetValue(entity, Nothing) IsNot Nothing Then
                        dbDataParameter.Value = propertyInfo.GetValue(entity, Nothing)
                    Else
                        dbDataParameter.Value = DBNull.Value
                    End If

                    command.Parameters.Add(dbDataParameter)
                End If
            Next
        End If

        Return Me.[Get](Of T)(command)
    End Function

    Public Function Save(Of T As New)(ByVal entity As T, extraParams As Hashtable, ParamArray ignoreList As String()) As List(Of T)
        Dim tsave As Type = GetType(T)
        Dim returnObject As T = New T()
        Dim command As IDbCommand = GetCommandObject()
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = String.Format("{0}Save", tsave.Name)
        Dim propertyInfoList As PropertyInfo() = tsave.GetProperties(BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

        If propertyInfoList IsNot Nothing Then

            For Each propertyInfo As PropertyInfo In propertyInfoList

                If Not ignoreList.Contains(propertyInfo.Name) Then
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = propertyInfo.Name

                    If propertyInfo.GetValue(entity, Nothing) IsNot Nothing Then
                        dbDataParameter.Value = propertyInfo.GetValue(entity, Nothing)
                    Else
                        dbDataParameter.Value = DBNull.Value
                    End If

                    command.Parameters.Add(dbDataParameter)
                End If
            Next

            If extraParams IsNot Nothing Then

                For Each key As String In extraParams.Keys
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = key

                    If extraParams(key) IsNot Nothing Then
                        dbDataParameter.Value = extraParams(key)
                    Else
                        dbDataParameter.Value = DBNull.Value
                    End If

                    command.Parameters.Add(dbDataParameter)
                Next
            End If

        End If

        Return Me.[Get](Of T)(command)
    End Function

    Public Function Delete(Of T As New)(ByVal entity As T, ParamArray ignoreList As String()) As List(Of T)
        Dim tdelete As Type = GetType(T)
        Dim returnObject As T = New T()
        Dim command As IDbCommand = GetCommandObject()
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = String.Format("{0}Delete", tdelete.Name)
        Dim propertyInfoList As PropertyInfo() = tdelete.GetProperties(BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

        If propertyInfoList IsNot Nothing Then

            For Each propertyInfo As PropertyInfo In propertyInfoList

                If Not ignoreList.Contains(propertyInfo.Name) Then
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = propertyInfo.Name

                    If propertyInfo.GetValue(entity, Nothing) IsNot Nothing Then
                        dbDataParameter.Value = propertyInfo.GetValue(entity, Nothing)
                    Else
                        dbDataParameter.Value = DBNull.Value
                    End If

                    command.Parameters.Add(dbDataParameter)
                End If
            Next
        End If

        Return Me.[Get](Of T)(command)
    End Function

    Public Function UpdateFeatured(Of T As New)(ByVal entity As T, ParamArray ignoreList As String()) As List(Of T)
        Dim tupdate As Type = GetType(T)
        Dim returnObject As T = New T()
        Dim command As IDbCommand = GetCommandObject()
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = String.Format("{0}Update", "Featured")
        Dim propertyInfoList As PropertyInfo() = tupdate.GetProperties(BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

        If propertyInfoList IsNot Nothing Then

            For Each propertyInfo As PropertyInfo In propertyInfoList

                If Not ignoreList.Contains(propertyInfo.Name) Then
                    Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
                    dbDataParameter.ParameterName = propertyInfo.Name

                    If propertyInfo.GetValue(entity, Nothing) IsNot Nothing Then
                        dbDataParameter.Value = propertyInfo.GetValue(entity, Nothing)
                    Else
                        dbDataParameter.Value = DBNull.Value
                    End If

                    command.Parameters.Add(dbDataParameter)
                End If
            Next
        End If

        Return Me.[Get](Of T)(command)
    End Function

    Public Function GetCommandObject() As IDbCommand
        Return New SqlCommand()
    End Function

    Public Function GetParameterObject(ByVal parameterName As String, ByVal objectValue As Object) As IDbDataParameter
        Return New SqlParameter(parameterName, objectValue)
    End Function

    Private Function [Get](Of T As New)(ByVal command As IDbCommand) As List(Of T)
        Dim dbDataParameter As IDbDataParameter = command.CreateParameter()
        Return ExecuteReader(command).ConvertToList(Of T)()
    End Function
End Class
