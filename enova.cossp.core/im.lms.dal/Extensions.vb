﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Globalization
Imports System.Data
Imports System.Reflection
Imports System.Linq.Expressions
Imports System.IO
Imports System.Xml.Serialization
Imports System.Xml
Imports System.ComponentModel
Imports System.Text.RegularExpressions

Imports System.Runtime.CompilerServices

Module MyExtensionMethods
    <Extension()>
    Function ToStringSafe(ByVal s As Object) As String
        If s Is Nothing Then
            Return String.Empty
        Else
            Dim result As String = s.ToString()

            If result Is Nothing Then
                Return String.Empty
            Else
                Return result
            End If
        End If
    End Function

    <Extension()>
    Function IsNotEmpty(ByVal s As String) As Boolean
        Return Not String.IsNullOrEmpty(s)
    End Function

    <Extension()>
    Function ToProperCase(ByVal TextToFormat As String) As String
        Dim properCase As String = String.Empty

        If TextToFormat IsNot Nothing Then
            properCase = New CultureInfo("en").TextInfo.ToTitleCase(TextToFormat.ToLower())
        End If

        Return properCase
    End Function

    <Extension()>
    Function ToIntSafe(ByVal s As Object) As Integer
        Dim outParameter As Integer = -1

        If s IsNot Nothing Then
            Integer.TryParse(s.ToString().Trim(), outParameter)
        End If

        Return outParameter
    End Function

    <Extension()>
    Function ToInt64Safe(ByVal s As Object) As Int64
        Dim outParameter As Int64 = -1

        If s IsNot Nothing Then
            Int64.TryParse(s.ToString().Trim(), outParameter)
        End If

        Return outParameter
    End Function

    <Extension()>
    Function ToDecimalSafe(ByVal val As Object) As Decimal
        Dim outParameter As Decimal = 0D

        If val IsNot Nothing Then
            Decimal.TryParse(val.ToStringSafe(), outParameter)
        End If

        Return outParameter
    End Function

    <Extension()>
    Function ToBooleanSafe(ByVal val As Object) As Boolean
        Dim result As Boolean
        Dim integralResult As Integer

        If val IsNot Nothing Then

            If Int32.TryParse(val.ToStringSafe(), integralResult) Then

                If integralResult = 0 Then
                    result = False
                Else
                    result = True
                End If
            Else
                Boolean.TryParse(val.ToStringSafe(), result)
            End If
        Else
            result = False
        End If

        Return result
    End Function

    <Extension()>
    Function ToDateTimeSafe(ByVal val As Object) As DateTime
        Dim result As DateTime

        If val IsNot Nothing Then
            DateTime.TryParse(val.ToStringSafe().Replace("EDT", ""), result)
        Else
            result = DateTime.MinValue
        End If

        Return result
    End Function

    <Extension()>
    Sub Log(ByVal ex As Exception, ByVal oType As Type)
        log4net.LogManager.GetLogger(oType).[Error](ex.Message, ex)
    End Sub

    <Extension()>
    Function TruncateAtWord(ByVal input As String, ByVal length As Integer) As String
        If input Is Nothing OrElse input.Length < length Then Return input
        Dim iNextSpace As Integer = input.LastIndexOf(" ", length)
        Return String.Format("{0}...", input.Substring(0, If((iNextSpace > 0), iNextSpace, length)).Trim())
    End Function

    <Extension()>
    Function ConvertToList(Of T As New)(ByVal table As DataTable) As List(Of T)
        Dim t1 As Type = GetType(T)
        Dim returnObject As List(Of T) = New List(Of T)()

        For Each dr As DataRow In table.Rows
            Dim newRow As T = dr.ConvertToEntity(Of T)()
            returnObject.Add(newRow)
        Next

        Return returnObject
    End Function

    <Extension()>
    Function ConvertToEntity(Of T As New)(ByVal tableRow As DataRow) As T
        Dim t3 As Type = GetType(T)
        Dim returnObject As T = New T()

        For Each col As DataColumn In tableRow.Table.Columns
            Dim colName As String = col.ColumnName
            Dim pInfo As PropertyInfo = t3.GetProperty(colName.ToLower(), BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

            If pInfo IsNot Nothing Then
                Dim val As Object = tableRow(colName)
                Dim IsNullable As Boolean = (Nullable.GetUnderlyingType(pInfo.PropertyType) IsNot Nothing)

                If IsNullable Then

                    If TypeOf val Is System.DBNull Then
                        val = Nothing
                    Else
                        val = Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType))
                    End If
                Else
                    val = Convert.ChangeType(val, pInfo.PropertyType)
                End If

                pInfo.SetValue(returnObject, val, Nothing)
            End If
        Next

        Return returnObject
    End Function

    Function ConvertToDataTable(Of T)(ByVal items As List(Of T)) As DataTable
        Dim dataTable As DataTable = New DataTable(GetType(T).Name)
        Dim Props As PropertyInfo() = GetType(T).GetProperties(BindingFlags.[Public] Or BindingFlags.Instance)

        For Each prop As PropertyInfo In Props
            Dim type = (If(prop.PropertyType.IsGenericType AndAlso prop.PropertyType.GetGenericTypeDefinition() = GetType(Nullable(Of)), Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
            dataTable.Columns.Add(prop.Name, type)
        Next

        For Each item As T In items
            Dim values = New Object(Props.Length - 1) {}

            For i As Integer = 0 To Props.Length - 1
                values(i) = Props(i).GetValue(item, Nothing)
            Next

            dataTable.Rows.Add(values)
        Next

        Return dataTable
    End Function

    <Extension()>
    Function ConvertToListUsingDataReader(Of T As New)(ByVal dataReader As IDataReader) As List(Of T)
        Try

            If dataReader IsNot Nothing Then
                Dim t2 As Type = GetType(T)
                Dim entityList As List(Of T) = New List(Of T)()

                While dataReader.Read()
                    Dim entityObject As T = New T()

                    For fieldIndex As Integer = 0 To dataReader.FieldCount - 1
                        Dim colName As String = dataReader.GetName(fieldIndex)
                        Dim pInfo As PropertyInfo = t2.GetProperty(colName.ToLower(), BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

                        If pInfo IsNot Nothing Then
                            Dim val As Object = dataReader(colName)
                            Dim IsNullable As Boolean = (Nullable.GetUnderlyingType(pInfo.PropertyType) IsNot Nothing)

                            If IsNullable Then

                                If TypeOf val Is System.DBNull Then
                                    val = Nothing
                                Else
                                    val = Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType))
                                End If
                            Else
                                val = Convert.ChangeType(val, pInfo.PropertyType)
                            End If

                            pInfo.SetValue(entityObject, val, Nothing)
                        End If
                    Next

                    entityList.Add(entityObject)
                End While

                Return entityList
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        Finally

            If Not dataReader.IsClosed Then
                dataReader.Close()
            End If
        End Try
    End Function

    <Extension()>
    Function ConvertToEntityUsingDataReader(Of T As New)(ByVal dataReader As IDataReader) As T
        Dim returnObject As T = New T()

        If dataReader IsNot Nothing Then
            Dim t6 As Type = GetType(T)

            While dataReader.Read()

                For fieldIndex As Integer = 0 To dataReader.FieldCount - 1
                    Dim colName As String = dataReader.GetName(fieldIndex)
                    Dim pInfo As PropertyInfo = t6.GetProperty(colName.ToLower(), BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

                    If pInfo IsNot Nothing Then
                        Dim val As Object = dataReader(colName)
                        Dim IsNullable As Boolean = (Nullable.GetUnderlyingType(pInfo.PropertyType) IsNot Nothing)

                        If IsNullable Then

                            If TypeOf val Is System.DBNull Then
                                val = Nothing
                            Else
                                val = Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType))
                            End If
                        Else
                            val = Convert.ChangeType(val, pInfo.PropertyType)
                        End If

                        pInfo.SetValue(returnObject, val, Nothing)
                    End If
                Next
            End While

            Return returnObject
        Else
            Return returnObject
        End If
    End Function

    <Extension()>
    Function ConvertToList(Of T As New)(ByVal dataReader As IDataReader) As List(Of T)
        If dataReader IsNot Nothing Then
            Dim t4 As Type = GetType(T)
            Dim entityList As List(Of T) = New List(Of T)()
            'exception here
            While dataReader.Read()
                Dim entityObject As T = New T()

                For fieldIndex As Integer = 0 To dataReader.FieldCount - 1

                    Try
                        Dim colName As String = dataReader.GetName(fieldIndex)
                        Dim pInfo As PropertyInfo = t4.GetProperty(colName.ToLower(), BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

                        If pInfo IsNot Nothing Then
                            Dim val As Object = dataReader(colName)
                            Dim IsNullable As Boolean = (Nullable.GetUnderlyingType(pInfo.PropertyType) IsNot Nothing)

                            If IsNullable Then

                                If TypeOf val Is System.DBNull Then
                                    val = Nothing
                                Else
                                    val = Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType))
                                End If
                            Else
                                If TypeOf val Is System.DBNull Then
                                    val = Nothing
                                Else
                                    val = Convert.ChangeType(val, pInfo.PropertyType)
                                End If
                                'val = Convert.ChangeType(val, pInfo.PropertyType)
                            End If

                            pInfo.SetValue(entityObject, val, Nothing)
                        End If

                    Catch ex As Exception
                        ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
                    End Try
                Next

                entityList.Add(entityObject)
            End While
            dataReader.Close() 'Added By Dharam as on 04Nov2019
            Return entityList
        Else
            Return Nothing
        End If
    End Function

    <Extension()>
    Function ConvertToEntity(Of T As New)(ByVal dataReader As IDataReader) As T
        Dim returnObject As T = New T()

        If dataReader IsNot Nothing Then
            Dim t5 As Type = GetType(T)

            While dataReader.Read()

                For fieldIndex As Integer = 0 To dataReader.FieldCount - 1
                    Dim colName As String = dataReader.GetName(fieldIndex)
                    Dim pInfo As PropertyInfo = t5.GetProperty(colName.ToLower(), BindingFlags.IgnoreCase Or BindingFlags.[Public] Or BindingFlags.Instance)

                    If pInfo IsNot Nothing Then
                        Dim val As Object = dataReader(colName)
                        Dim IsNullable As Boolean = (Nullable.GetUnderlyingType(pInfo.PropertyType) IsNot Nothing)

                        If IsNullable Then

                            If TypeOf val Is System.DBNull Then
                                val = Nothing
                            Else
                                val = Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType))
                            End If
                        Else
                            val = Convert.ChangeType(val, pInfo.PropertyType)
                        End If

                        pInfo.SetValue(returnObject, val, Nothing)
                    End If
                Next
            End While

            Return returnObject
        Else
            Return returnObject
        End If
    End Function

    <Extension()>
    Function IsNumeric(ByVal s As String) As Boolean
        Dim output As Single
        Return Single.TryParse(s, output)
    End Function

    <Extension()>
    Function GetMemberName(Of T)(ByVal instance As T, ByVal expression As Expression(Of Func(Of T, Object))) As String
        Return expression.ToString().Split("."c)(1).Split(")"c)(0)
    End Function

    <Extension()>
    Function GetMemberName(Of T)(ByVal instance As T, ByVal expression As Expression(Of Action(Of T))) As String
        Return GetMemberName(expression)
    End Function

    Function GetMemberName(Of T)(ByVal expression As Expression(Of Action(Of T))) As String
        If expression Is Nothing Then
            Throw New ArgumentException("The expression cannot be null.")
        End If

        Return GetMemberName(expression.Body)
    End Function

    Private Function GetMemberName(ByVal expression As Expression) As String
        If expression Is Nothing Then
            Throw New ArgumentException("The expression cannot be null.")
        End If

        If TypeOf expression Is MemberExpression Then
            Dim memberExpression = CType(expression, MemberExpression)
            Return memberExpression.Member.Name
        End If

        If TypeOf expression Is MethodCallExpression Then
            Dim methodCallExpression = CType(expression, MethodCallExpression)
            Return methodCallExpression.Method.Name
        End If

        If TypeOf expression Is UnaryExpression Then
            Dim unaryExpression = CType(expression, UnaryExpression)
            Return GetMemberName(unaryExpression)
        End If

        Throw New ArgumentException("Invalid expression")
    End Function

    Private Function GetMemberName(ByVal unaryExpression As UnaryExpression) As String
        If TypeOf unaryExpression.Operand Is MethodCallExpression Then
            Dim methodExpression = CType(unaryExpression.Operand, MethodCallExpression)
            Return methodExpression.Method.Name
        End If

        Return (CType(unaryExpression.Operand, MemberExpression)).Member.Name
    End Function

    <Extension()>
    Function ConvertXMLStringToList(Of T As New)(ByVal XmlString As String) As T
        Dim returnedXmlClass As T = Nothing

        Using reader As System.IO.TextReader = New StringReader(XmlString)

            Try
                returnedXmlClass = CType(New XmlSerializer(GetType(T)).Deserialize(reader), T)
            Catch __unusedInvalidOperationException1__ As InvalidOperationException
                Throw New InvalidOperationException("Getting Error While Performing Deserialize")
            End Try
        End Using

        Return returnedXmlClass
    End Function

    <Extension()>
    Function ConvertObjectToXML(Of T As New)(ByVal ObjectName As T) As String
        Dim xmlSerializer As XmlSerializer = New XmlSerializer(ObjectName.[GetType]())
        Dim stringBuilder As StringBuilder = New StringBuilder()

        Using writer = XmlWriter.Create(stringBuilder)
            xmlSerializer.Serialize(writer, ObjectName)
        End Using

        Return stringBuilder.ToString()
    End Function

    <Extension()>
    Function ConvertToXML(Of T)(ByVal objEntity As T) As String
        Dim ser As XmlSerializer = New XmlSerializer(objEntity.[GetType]())
        Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder()

        Try
            Dim settings As XmlWriterSettings = New XmlWriterSettings() With {
                .Indent = False,
                .OmitXmlDeclaration = True
            }
            Dim stream = New MemoryStream()

            Using xw As XmlWriter = XmlWriter.Create(sb, settings)
                Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces({XmlQualifiedName.Empty})
                Dim x As XmlSerializer = New XmlSerializer(objEntity.[GetType](), "")
                x.Serialize(xw, objEntity, ns)
            End Using

        Catch ex As Exception
            Throw ex
        End Try

        Return sb.ToString()
    End Function

    <Extension()>
    Function ConvertToEntity(Of T As New)(ByVal xmlValue As String) As List(Of T)
        Dim entity As T = Nothing
        Dim returnObject As List(Of T) = New List(Of T)()
        Dim xmlDocument As XmlDocument = New XmlDocument()
        xmlDocument.LoadXml(xmlValue)

        If xmlDocument IsNot Nothing AndAlso xmlDocument.ChildNodes.Count > 0 Then
            Dim rootElement As XmlElement = xmlDocument.DocumentElement

            For Each node As XmlNode In rootElement.ChildNodes

                Using reader As StringReader = New StringReader(node.OuterXml)
                End Using
            Next
        End If

        Return returnObject
    End Function

    <Extension()>
    Function GetDescription(ByVal value As [Enum]) As String
        Dim field As FieldInfo = value.[GetType]().GetField(value.ToString())
        Dim attribute As DescriptionAttribute = TryCast(attribute.GetCustomAttribute(field, GetType(DescriptionAttribute)), DescriptionAttribute)
        Return If(attribute Is Nothing, value.ToString(), attribute.Description)
    End Function

    <Extension()>
    Function ToSafeFileName(ByVal s As String, ByVal Optional rc As String = "") As String
        Dim s_name = s.Replace("<", rc).Replace(">", rc).Replace(":", rc).Replace("""", rc).Replace("\", rc).Replace("/", rc).Replace("|", rc).Replace("?", rc).Replace("*", rc).Replace("'", rc).Replace(" ", rc).Replace("&", rc).Replace(".", rc)
        Dim my_String As String = Regex.Replace(s_name, "[^0-9a-zA-Z]+", "")
        Return my_String
    End Function
End Module

