﻿Public Class UserStatistics
    Inherits BaseEntity
    Public Property DateLogged As DateTime
    Public Property Events As String
    Public Property LoggedBy As String
    Public Property Comments As String
    Public Property SuccessfulLogins As Integer
    Public Property LoginsFailed As Integer
    Public Property ActiveSessions As Integer
    Public Property InactiveUsers As Integer
    Public Property TasksCreated As Integer
    Public Property PageAccessed As Integer

End Class
