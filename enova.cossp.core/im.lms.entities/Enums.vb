﻿Public Enum NotificationSettingEnum
    CSF = 0
    POLLS = 2
    MESSAGES = 3
    UPDATEPROFILE = 4
    INVOICEDOCUMENTS = 5

End Enum

Public Enum NotificationPreferenceEnum
    SMS = 0
    EMAIL = 2
    BOTH = 3
    NONE = 4

End Enum


