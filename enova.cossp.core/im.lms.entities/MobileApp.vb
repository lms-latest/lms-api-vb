﻿Public Class MobileApp
    Inherits BaseEntity

    Public Property VideoUrl As String
    Public Property AndroidAppUrl As String
    Public Property IosAppUrl As String
    Public Property Description As String
    Public Property MobileAppDocuments As List(Of MobileAppDocument)
End Class


Public Class MobileAppDocument
    Inherits BaseEntity

    Public Property Title As String
    Public Property DocumentUrl As String
End Class