﻿Public Class Report
    Inherits BaseEntity
    Public Property Name As String
    Public Property TypeID As Integer
    Public Property ServiceUrl As String
    Public Property PagigngSupport As Boolean
    Public Property ContractFilter As Boolean
    Public Property Authentication As Boolean
    Public Property ReportID As String
    Public Property PageNumber As String
    Public Property SizeID As Integer
    Public Property EmbeddedUrl As String
    Public Property ShowHeader As Boolean
    Public Property TypeName As String
    Public Property FilterParameter As String
End Class
