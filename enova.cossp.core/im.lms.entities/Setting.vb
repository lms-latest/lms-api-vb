﻿Public Class Setting
    Public Property ID As Integer
    Public Property key As String
    Public Property value As String
    Public Property modifiedBy As Integer
    Public Property modifiedDate As DateTime
    Public Property IsEncrypted As Boolean
End Class
