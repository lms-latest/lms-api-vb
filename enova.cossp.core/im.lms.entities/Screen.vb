﻿Public Class Screen
    Inherits BaseEntity
    Public Property ModuleID As Integer
    Public Property Name As String
    Public Property Description As String
    Public Property DisplayName As String
    Public Property URL As String
    Public Property Assigned As Boolean
    Public Property Userid As Integer
    Public Property RoleID As Integer
    Public Property RoleName As String
    Public Property ModuleName As String
    Public Property ModuleIconClass As String
    Public Property PermissionID As Integer
    Public Property RolePagePermissionID As Integer

End Class
