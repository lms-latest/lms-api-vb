﻿Public Class BaseEntity
    Public Property ID As Integer
    Public Property CreatedBy As Integer
    Public Property CreatedDate As DateTime?
    Public Property ModifiedBy As Integer
    Public Property ModifiedDate As DateTime?
    Public Property Active As Boolean
End Class
