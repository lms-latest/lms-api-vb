﻿Public Class ProfileModeration
    Inherits BaseEntity
    Public Property UserID As Integer 'UserID
    Public Property Name As String
    Public Property FirstName As String
    Public Property LastName As String
    Public Property JobTitle As String
    Public Property UserName As String
    Public Property Email As String
    '  Public Property JobTitle As List(Of JobTitle)
    Public Property ProfilePicture As String
    Public Property MobileNo As String
    Public Property PhoneNo As String
    ' Public Property JobTitleID As Integer
    'Public Property IsApproved As Boolean
    Public Property Message As String
    Public Property Status As String
    Public Property Company As String
End Class

Public Class ProfileModerationPaging
    Public Property Paging As Pagging
    Public Property Status As String
End Class
