﻿Public Class Pagging
    Public Property Offset As Integer
    Public Property PageSize As Integer
    Public Property SortBy As String
    Public Property SortOrder As String
    Public Property SearchValue As String
    Public Property dateFrom As DateTime?
    Public Property dateTo As DateTime?
End Class

Public Class PagingModelCafm
    Public Property Id As String
    Public Property PageNo As Integer
    Public Property PageSize As Integer
    Public Property OrderBy As String
    Public Property IsOrderAsc As Boolean
    Public Property SearchValue As String

End Class

Public Class PagingModelTabelTypeReport
    Public Property PageNo As Integer
    Public Property PageSize As Integer
    Public Property OrderBy As String
    Public Property IsOrderAsc As Boolean
End Class


