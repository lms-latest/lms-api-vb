﻿Public Class User
    Inherits BaseEntity

    Public Property userName As String
    Public Property password As String
    Public Property email As String
    Public Property Name As String
    Public Property oldProfilePircture As String
    Public Property profilePicture As String
    Public Property mobileNo As String
    Public Property phoneNo As String
    Public Property jobTitle As String
    'Public Property jobTitle As JobTitle
    Public Property contract As List(Of Contract)
    Public Property categoryID As Integer
    'Public Property category As Category
    Public Property building As List(Of Building)
    Public Property roleID As Integer
    Public Property company As String
    Public Property cafmUserID As String
    'Public Property exchangeMessageWith As List(Of User)
    Public Property MessageAllowed As Boolean
    Public Property IsAllBuilding As Boolean
    Public Property IsActive As Boolean
    Public Property FirstName As String
    Public Property LastName As String
    Public Property IsAldapUser As Boolean

End Class


