﻿Public Class HelpTopic
    Inherits BaseEntity

    Public Property Title As String
    Public Property Description As String
    Public Property HelpTopicDocuments As List(Of HelpTopicDocument)
    Public Property VideoUrl As String
End Class


Public Class HelpTopicDocument
    Inherits BaseEntity
    Public Property HelpTopicId As Integer
    Public Property DocumentPath As String
    Public Property ActualDocumentPath As String
    'Public Property OldDocumentPath As String
End Class





