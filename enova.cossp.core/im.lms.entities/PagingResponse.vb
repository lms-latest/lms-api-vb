﻿'Public Class PagingResponse(Of T As Class)
'    Public Property TotalRecors As Integer
'    Public Property Result As List(Of T)
'End Class


Public Class PagingResponse(Of T)
        Inherits List(Of T)

        Public Property TotalNumberOfItems As Integer

        Public Sub New(ByVal totalNumberOfItems As Integer, ByVal collection As IEnumerable(Of T))
            MyBase.New(collection)
            totalNumberOfItems = totalNumberOfItems
        End Sub


    End Class

