﻿Public Class Role
    Inherits BaseEntity
    Public Property ID As Integer
    Public Property Name As String
    Public Property Description As String
    Public Property Screens As List(Of Screen)
    Public Property Notes As String
    Public Property Reports As List(Of Report)
    Public Property ScreensPermitted As String
    Public Property IsActive As Boolean
End Class
