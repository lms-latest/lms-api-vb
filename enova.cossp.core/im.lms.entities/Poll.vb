﻿Public Class Poll
    Inherits BaseEntity
    Public Property name As String
    Public Property description As String
    Public Property isDisplayResultToUser As Boolean
    Public Property shouldExpireBy As Date
    Public Property visibleTo As List(Of Role)
    Public Property pollOption As List(Of PollOption)
    Public Property count As Integer



End Class
