﻿Public Class UserInfo
    Public Property ID As Integer 'UserID
    Public Property userName As String
    Public Property email As String
    Public Property Name As String
    Public Property FirstName As String
    Public Property LastName As String
    Public Property jobTitle As String
    '  Public Property jobTitle As List(Of JobTitle)
    Public Property oldProfilePircture As String
    Public Property profilePicture As String
    Public Property mobileNo As String
    Public Property phoneNo As String
    ' Public Property jobTitleID As Integer
    Public Property contract As List(Of Contract)
    Public Property building As List(Of Building)
    Public Property company As String
    Public Property categoryID As Integer
    Public Property Status As String
    Public Property Message As String
    Public Property IsNotificationClosed As Boolean
    Public Property IsAllBuilding As Boolean
    'Public Property role As String
    'Public Property screens As NavigationScreen
    'Public Property IsPendingForModeration As Boolean
    Public Property Preference As List(Of NotificationSetting)
End Class

