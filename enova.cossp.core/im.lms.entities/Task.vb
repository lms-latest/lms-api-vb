﻿Public Class Task
    'Inherits BaseEntity
    'Public Property TaskId As Integer
    'Public Property Type As String
    'Public Property Contract As String
    'Public Property Building As String
    'Public Property Location As String
    'Public Property Problem As String
    'Public Property Priority As String
    'Public Property Category As String
    'Public Property Discipline As String
    'Public Property Description As String
    'Public Property Picture As String
    'Public Property DateCreated As Date
    'Public Property DateClosed As Date
    'Public Property Status As String


    Public Property AssetId As Integer
    Public Property BuildingId As Integer
    Public Property Category As String
    Public Property ContractId As Integer
    Public Property TimeZoneId As Integer
    Public Property CreatedDate As DateTime
    Public Property Discipline As String
    Public Property InstructionId As Integer
    Public Property LocationId As Integer
    Public Property LongDescription As String
    Public Property Priority As String
    Public Property RaisedDate As DateTime
    Public Property ShortDescription As String
    Public Property CallerSource As String
    Public Property Phone As String
    Public Property ReporterName As String
    Public Property ReporterId As Integer
    Public Property Timezone As String
    Public Property TaskImageName As String
    Public Property TaskImageBase64 As String
    Public Property TaskImageMimeType As String
    Public Property FileName As String
    Public Property FileType As String
End Class
