﻿Public Class Message
    Inherits BaseEntity

    Public Property FromUserID As Integer
    Public Property ToUserID As Integer
    Public Property Message As String
    Public Property DateSent As DateTime
    Public Property DateViewed As DateTime?
    Public Property IsViewed As Boolean
    Public Property SendOrReceived As String
    Public Property OtherUserName As String
    Public Property profilePicture As String
    Public Property FormattedDateSent As String
    Public Property FormattedDateViewed As String



End Class
