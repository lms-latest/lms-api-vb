﻿Imports System.Web
Imports Microsoft.Identity.Core
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports System.Web.Caching
Imports System.Runtime.Caching


'Imports enova.cossp.bll.SettingManager


Public Class DBConfigurationManager
    'Public Shared Sub InitializeAppSetting()

    '    Try
    '        Dim dbHelper As DBHelper = New DBHelper()
    '        Dim command As IDbCommand = dbHelper.GetCommandObject()
    '        Dim setting As List(Of Setting) = New List(Of Setting)()
    '        setting = dbHelper.[Get](Of Setting)(command, "GetListOfSetting")

    '        HttpContext.Current.Cache.Insert("AppSettings", setting, Nothing, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    'Public Shared Function GetAppSetting(ByVal key As String)
    '    Try
    '        If HttpContext.Current.Cache("AppSettings") Is Nothing Then
    '            InitializeAppSetting()
    '        End If
    '        Dim settings = CType(HttpContext.Current.Cache("AppSettings"), List(Of Setting))
    '        Dim firstMatch = settings.FirstOrDefault(Function(i) i.key = key)
    '        Return firstMatch.value
    '    Catch ex As Exception

    '        Throw ex
    '    End Try

    'End Function
    'Public Shared Sub CacheRefersh()
    '    Try
    '        HttpContext.Current.Cache.Remove("AppSettings")

    '        InitializeAppSetting()

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub
    Public Shared Property cache As MemoryCache = MemoryCache.[Default]
    Public Shared Sub InitializeAppSetting()

        Try
            Dim dbHelper As DBHelper = New DBHelper()
            Dim command As IDbCommand = dbHelper.GetCommandObject()
            Dim setting As List(Of Setting) = New List(Of Setting)()
            setting = dbHelper.[Get](Of Setting)(command, "GetListOfSetting")

            Dim policy As CacheItemPolicy = New CacheItemPolicy()
            Dim settingsCache = New CacheItem("AppSettings", setting)
            cache.Add(settingsCache, policy)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Sub

    Public Shared Function GetAppSetting(ByVal key As String)
        Try
            If cache("AppSettings") Is Nothing Then
                InitializeAppSetting()
            End If
            Dim settings = CType(cache("AppSettings"), List(Of Setting))
            Dim firstMatch = settings.FirstOrDefault(Function(i) i.key = key)
            Return firstMatch.value
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Shared Sub CacheRefersh()
        Try

            cache.Remove("AppSettings")
            InitializeAppSetting()

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Sub



End Class
