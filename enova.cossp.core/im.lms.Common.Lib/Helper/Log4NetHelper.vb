﻿Public Class Log4NetHelper
    Public Shared ErrorMessage As String = "An exception occured while executing function "
    Public Shared ExceptionMessage As String = ". The exception is: "

    Public Shared Sub LogError(ByVal Optional ex As Exception = Nothing, ByVal Optional message As String = "")
        Dim logErrorMessage As String = ErrorMessage + System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() + "." + System.Reflection.MethodInfo.GetCurrentMethod().Name + ExceptionMessage + message.ToString()
        If message = String.Empty Then
            logErrorMessage = "Error in " & Convert.ToString(ex.Source) & "."
        End If

        If ex IsNot Nothing Then
            logErrorMessage += Environment.NewLine & " Exception - " + ex.Message
        End If

        Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        log4net.Config.XmlConfigurator.Configure()
        log.[Error](logErrorMessage)
    End Sub

    Public Shared Sub LogInfo(ByVal Optional methodName As String = "", ByVal Optional message As String = "")
        log4net.Config.XmlConfigurator.Configure()
        If message <> String.Empty Then
            Dim logInfoMessage As String = ErrorMessage + System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() + "." + System.Reflection.MethodInfo.GetCurrentMethod().Name + ExceptionMessage + message.ToString()
            Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
            log4net.Config.XmlConfigurator.Configure()
            log.Info(logInfoMessage)
        End If
    End Sub

    'Public Shared Sub LogError(ByVal Optional ex As Exception = Nothing, ByVal Optional sendMail As Boolean = True, ByVal Optional message As String = "")
    '    If message = String.Empty Then
    '        message = "Error in " & Convert.ToString(ex.Source) & "."
    '    End If

    '    If ex IsNot Nothing Then
    '        message += Environment.NewLine & " Exception - " + ex.Message
    '    End If

    '    Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    '    log4net.Config.XmlConfigurator.Configure()
    '    log.[Error](message)
    'End Sub

    'Public Shared Sub LogInfo(ByVal message As String)
    '    log4net.Config.XmlConfigurator.Configure()

    '    If message <> String.Empty Then
    '        Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    '        log4net.Config.XmlConfigurator.Configure()
    '        log.Info(message)
    '    End If
    'End Sub
End Class
