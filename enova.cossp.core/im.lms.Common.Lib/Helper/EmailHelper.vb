﻿Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.Net.Mime

Public Class EmailHelper

    Public Shared Function ReadEmailTemplate(ByVal templatePath As String) As String
        Try
            Dim readContents As String = String.Empty
            Dim streamReader As StreamReader = New StreamReader(templatePath, Encoding.UTF8)
            readContents = streamReader.ReadToEnd()
            Return readContents
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Shared Function SendEmail(ByVal Subject As String, ByVal emailBody As String, ByVal ToMail As String, ByVal Optional filepath As String = Nothing) As Boolean

        Dim fromEmail = ConfigManager.FromEmail()
        Dim password = ConfigManager.EmailPassword()
        Dim host = ConfigManager.SmtpHost()
        Dim port = CInt(ConfigManager.SmtpPort())

        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(fromEmail, password)
            Smtp_Server.Port = port
            Smtp_Server.EnableSsl = True
            Smtp_Server.Host = host

            e_mail = New MailMessage()
            e_mail.From = New MailAddress(fromEmail)
            e_mail.To.Add(ToMail)
            e_mail.Subject = Subject
            e_mail.IsBodyHtml = True
            e_mail.Body = emailBody
            If filepath IsNot Nothing Then
                Dim data As Attachment = New Attachment(filepath, MediaTypeNames.Application.Octet)
                Dim OriginalName As String = filepath.Substring(filepath.IndexOf("-") + 1)
                data.Name = OriginalName
                e_mail.Attachments.Add(data)
            End If

            Smtp_Server.Send(e_mail)
            e_mail.Dispose()
            Smtp_Server.Dispose()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Shared Function SendEmailMultiple(ByVal Subject As String, ByVal emailBody As String, ByVal ToMail As List(Of String)) As Boolean

        Dim fromEmail = ConfigManager.FromEmail()
        Dim password = ConfigManager.EmailPassword()
        Dim host = ConfigManager.SmtpHost()
        Dim port = CInt(ConfigManager.SmtpPort())

        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(fromEmail, password)
            Smtp_Server.Port = port
            Smtp_Server.EnableSsl = True
            Smtp_Server.Host = host

            e_mail = New MailMessage()
            e_mail.From = New MailAddress(fromEmail)
            For Each value As String In ToMail
                e_mail.To.Add(value)


            Next

            e_mail.Subject = Subject
            e_mail.IsBodyHtml = True
            e_mail.Body = emailBody
            Smtp_Server.Send(e_mail)
            e_mail.Dispose()
            Smtp_Server.Dispose()
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

End Class
