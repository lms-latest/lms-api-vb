﻿Imports System.Configuration
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib
Imports enova.cossp.Common.Lib.CryptoHelper

Public Class ConfigManager
    Public Shared Function [Get](Of T)(ByVal key As String) As T
        Dim value
        Dim IsSensitiveKey As Boolean
        Dim result
        Try
            'Log4NetHelper.LogInfo("ConfigManager.Get", key)
            value = ConfigurationManager.AppSettings(key)
            IsSensitiveKey = IsSensitivekeys(key)

            If value IsNot Nothing Then
                If (IsSensitiveKey.Equals(True)) Then
                    value = Decrypt(value, GetEncryptionKey)
                End If
                Return CType(Convert.ChangeType(value, GetType(T)), T)
            Else
                result = DBConfigurationManager.GetAppSetting(key)
                If (IsSensitiveKey.Equals(True)) Then
                    result = Decrypt(result, GetEncryptionKey)
                End If
                Return CType(Convert.ChangeType(result, GetType(T)), T)
            End If

        Catch ex As Exception
            Log4NetHelper.LogInfo("ConfigManager.Get", "Error in ConfigManager.Get:- " + ex.ToString() + "key:- " + key + "value:- " + value + "result:- " + result)
        End Try
    End Function

    Public Shared Function IsSensitivekeys(key As String) As Boolean
        Try
            Dim mykey As String() = ConfigurationManager.AppSettings("SensitiveKeys").Split(","c)
            For Each keys In mykey
                If (keys.Equals(key)) Then
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function WebUrlForEnova() As String
        Try
            Return [Get](Of String)("WebUrlForEnova")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("WebUrlForEnova")

    End Function

    Public Shared Function AllDocScreenID() As Integer
        Try
            Return [Get](Of Integer)("AllDocScreenID")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Shared Function GetTaskTypeUrl() As String
        Try
            Return [Get](Of String)("GetTaskTypeUrl")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function
    Public Shared Function GetTaskStatusUrl() As String
        Try
            Return [Get](Of String)("GetTaskStatusUrl")

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function


    Public Shared Function AllowedAPIEndPoints() As String
        Try
            Return [Get](Of String)("AllowedAPIEndPoints")
            'Return ConfigurationManager.AppSettings("AllowedAPIEndPoints")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function TokenExpireTimeInDays() As Integer
        Try
            Return [Get](Of Integer)("TokenExpireTimeInDays")
            'Dim days = CInt(ConfigurationManager.AppSettings("TokenExpireTimeInDays"))
            'Return days
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function EmailTemplatesPath() As String
        Try
            Return [Get](Of String)("EmailTemplatesPath")
            'Return ConfigurationManager.AppSettings("EmailTemplatesPath")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function getSaltForHash() As String
        Try
            Return [Get](Of String)("getSaltForHash")
            'Return ConfigurationManager.AppSettings("EmailTemplatesPath")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function FromEmail() As String
        Try
            Return [Get](Of String)("FromEmail")
            'Return ConfigurationManager.AppSettings("FromEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function EmailPassword() As String
        Try
            Return [Get](Of String)("EmailPassword")
            'Return ConfigurationManager.AppSettings("EmailPassword")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function SmtpHost() As String
        Try
            Return [Get](Of String)("SmtpHost")
            'Return ConfigurationManager.AppSettings("SmtpHost")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function SmtpPort() As String
        Try
            Return [Get](Of String)("Port")
            'Return ConfigurationManager.AppSettings("Port")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function SubjectForResetPasswordEmail() As String
        Try
            Return [Get](Of String)("SubjectForResetPasswordEmail")
            'Return ConfigurationManager.AppSettings("SubjectForResetPasswordEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForDocumentRecievedEmail() As String
        Try
            Return [Get](Of String)("SubjectForDocumentRecievedEmail")
            'Return ConfigurationManager.AppSettings("SubjectForDocumentRecievedEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForRecievedInvoiveEmail() As String
        Try
            Return [Get](Of String)("SubjectForRecievedInvoiveEmail")
            'Return ConfigurationManager.AppSettings("SubjectForRecievedInvoiveEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForMessageSaveEmail() As String
        Try
            Return [Get](Of String)("SubjectForMessageSaveEmail")
            'Return ConfigurationManager.AppSettings("SubjectForPollSaveEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForUpdateProfileSaveEmail() As String
        Return [Get](Of String)("SubjectForUpdateProfileSaveEmail")
        'Return ConfigurationManager.AppSettings("SubjectForPollSaveEmail")

    End Function
    Public Shared Function SubjectForProfileModerationSaveEmail() As String
        Try
            Return [Get](Of String)("SubjectForProfileModerationSaveEmail")
            'Return ConfigurationManager.AppSettings("SubjectForPollSaveEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForPollSaveEmail() As String
        Try
            Return [Get](Of String)("SubjectForPollSaveEmail")
            'Return ConfigurationManager.AppSettings("SubjectForPollSaveEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForAnnouncementSaveEmail() As String
        Try
            Return [Get](Of String)("SubjectForAnnouncementSaveEmail")
            'Return ConfigurationManager.AppSettings("SubjectForPollSaveEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function DocumentFolderPath() As String
        Try
            Return [Get](Of String)("DocumentFolderPath")
            'Return ConfigurationManager.AppSettings("DocumentFolderPath")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try

    End Function

    Public Shared Function SubjectForUpdatePasswordConfirmationEmail() As String
        Try
            Return [Get](Of String)("SubjectForUpdatePasswordConfirmationEmail")
            'Return ConfigurationManager.AppSettings("SubjectForUpdatePasswordConfirmationEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function SubjectForUserCreationEmail() As String
        Try
            Return [Get](Of String)("SubjectForUserCreationEmail")
            'Return ConfigurationManager.AppSettings("SubjectForUserCreationEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function OneByTwoWidgetSizeId() As String
        Try
            Return [Get](Of String)("OneByTwoWidgetSizeId")
            'Return ConfigurationManager.AppSettings("OneByTwoWidgetSizeId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function TwoByOneWidgetSizeId() As String
        Try
            Return [Get](Of String)("TwoByOneWidgetSizeId")
            'Return ConfigurationManager.AppSettings("TwoByOneWidgetSizeId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function TwoByTwoWidgetSizeId() As String
        Try
            Return [Get](Of String)("TwoByTwoWidgetSizeId")
            'Return ConfigurationManager.AppSettings("TwoByTwoWidgetSizeId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function AllWidgetsSizeID() As String
        Try
            Return [Get](Of String)("AllWidgetsSizeID")
            'Return ConfigurationManager.AppSettings("AllWidgetsSizeID")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function DashboardWidgetTypeID() As String
        Try
            Return [Get](Of String)("DashboardWidgetTypeID")
            'Return ConfigurationManager.AppSettings("DashboardWidgetTypeID")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function AllUsers() As String
        Try
            Return [Get](Of String)("AllUsers")
            'Return ConfigurationManager.AppSettings("AllUsers")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared ReadOnly Property UserPhotoFolder As String
        Get
            Return [Get](Of String)("UserPhotoFolder")
            'Return ConfigurationManager.AppSettings("UserPhotoFolder")
        End Get
    End Property

    Public Shared Function ApplicationID() As String
        Try
            Return [Get](Of String)("ApplicationID")
            'Return ConfigurationManager.AppSettings("ApplicationID")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function PowerBiAPIResource() As String
        Try
            Return [Get](Of String)("PowerBiAPIResource")
            'Return ConfigurationManager.AppSettings("PowerBiAPIResource")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function ApplicationSecret() As String
        Try
            Return [Get](Of String)("ApplicationSecret")
            'Return ConfigurationManager.AppSettings("ApplicationSecret")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function AADAuthorityUri() As String
        Try
            Return [Get](Of String)("AADAuthorityUri")
            'Return ConfigurationManager.AppSettings("AADAuthorityUri")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function PowerBiDataset() As String
        Try
            Return [Get](Of String)("PowerBiDataset")
            'Return ConfigurationManager.AppSettings("PowerBiDataset")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Shared Function PowerBiApiUrl() As String
        Try
            Return [Get](Of String)("PowerBiApiUrl")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("PowerBiApiUrl")

    End Function

    Public Shared Function PowerBIUserName() As String
        Try
            Return [Get](Of String)("PowerBIUserName")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("PowerBIUserName")

    End Function

    Public Shared Function PowerBIUserPassword() As String
        Try
            Return [Get](Of String)("PowerBIUserPassword")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("PowerBIUserPassword")

    End Function

    Public Shared Function GetContractUrl() As String
        Try
            Return [Get](Of String)("GetContractUrl")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("GetContractUrl")

    End Function
    Public Shared Function GetBuildingUrl() As String
        Try
            Return [Get](Of String)("GetBuildingUrl")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("GetBuildingUrl")

    End Function
    Public Shared Function GetLocationsUrl() As String
        Try
            Return [Get](Of String)("GetLocationsUrl")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("GetLocationsUrl")

    End Function
    Public Shared Function GetEscalationsUrl() As String
        Try
            Return [Get](Of String)("GetEscalationsUrl")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("GetEscalationsUrl")

    End Function
    Public Shared Function GetUploadsFolder() As String
        Try
            Return [Get](Of String)("GetUploadsFolder")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("GetUploadsFolder")

    End Function
    Public Shared Function GetProblemListUrl() As String
        Try
            Return [Get](Of String)("GetProblemListUrl")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("GetProblemListUrl")

    End Function
    Public Shared Function GetCafmApiURL() As String
        Try
            Return [Get](Of String)("CafmApiURL")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("CafmApiURL")

    End Function

    Public Shared Function GetCafmSeviceLoginName() As String
        Try
            Return [Get](Of String)("CafmSeviceLoginName")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("CafmSeviceLoginName")
    End Function

    Public Shared Function GetCafmSevicePassword() As String
        Try
            Return [Get](Of String)("CafmSevicePassword")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("CafmSevicePassword")
    End Function

    Public Shared Function GetCafmApiUserName() As String
        Try
            Return [Get](Of String)("CafmApiUserName")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("CafmApiUserName")
    End Function

    Public Shared Function GetCafmApiPassword() As String
        Try
            Return [Get](Of String)("CafmApiPassword")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("CafmApiPassword")
    End Function


    Public Shared Function NotificationCSFType() As String
        Try
            Return [Get](Of String)("NotificationCSFType")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("NotificationCSFType")
    End Function

    Public Shared Function MessageForCSFTypeNotification() As String
        Try
            Return [Get](Of String)("MessageForCSFTypeNotification")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("MessageForCSFTypeNotification")
    End Function

    Public Shared Function ApiUrlForTableTypeReportToken() As String
        Try
            Return [Get](Of String)("ApiUrlForTableTypeReportToken")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("ApiUrlForTableTypeReportToken")
    End Function

    Public Shared Function UsernameForTableTypeReportToken() As String
        Try
            Return [Get](Of String)("UsernameForTableTypeReportToken")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("UsernameForTableTypeReportToken")
    End Function

    Public Shared Function PasswordForTableTypeReportToken() As String
        Try
            Return [Get](Of String)("PasswordForTableTypeReportToken")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("PasswordForTableTypeReportToken")
    End Function

    Public Shared Function DefaultUserProfileImage() As String
        Try
            Return [Get](Of String)("DefaultUserProfileImage")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("DefaultUserProfileImage")
    End Function


    Public Shared Function GetCSFScreenId() As Integer
        Try
            Return [Get](Of Integer)("CSFScreenId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Shared Function GetReportScreenId() As Integer
        Try
            Return [Get](Of Integer)("ReportScreenId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function


    Public Shared Function GetMessageScreenId() As Integer
        Try
            Return [Get](Of Integer)("MessageScreenId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function GetAccountId() As Integer
        Return [Get](Of Integer)("AccountId")
    End Function

    Public Shared Function GetInstructionId() As Integer
        Try
            Return [Get](Of Integer)("InstructionId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function GetBDETCallerSourceId() As Integer
        Try
            Return [Get](Of Integer)("BDETCALLERSOURCEID")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function GetTimeZoneId() As Integer
        Try
            Return [Get](Of Integer)("TimeZoneId")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    'Public Shared Function GetIssuer() As String
    '    Return [Get](Of String)("Issuer")
    'End Function
    'Public Shared Function GetAudience() As String
    '    Return [Get](Of String)("Audience")
    'End Function
    'Public Shared Function GetSec() As String
    '    Return [Get](Of String)("Sec")
    'End Function
    Public Shared Function GetTokenExpiryTime() As Integer
        Try
            Return [Get](Of Integer)("TokenExpiryTime")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function GetEncryptionKey() As String
        Try
            Return [Get](Of String)("EncryptionKey")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function GetRememberMeTime() As Integer
        Try
            Return [Get](Of Integer)("RememberMeTime")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function GetSettingEmail() As String
        Try
            Return [Get](Of String)("SettingEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function SubjectForSettingSaveEmail() As String
        Try
            Return [Get](Of String)("SubjectForSettingSaveEmail")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("SubjectForPollSaveEmail")
    End Function

    Public Shared Function cryptoPrivateKey() As String
        Try
            Return [Get](Of String)("cryptoPrivateKey")
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
        'Return ConfigurationManager.AppSettings("DefaultUserProfileImage")
    End Function
End Class



