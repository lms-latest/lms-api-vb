﻿Imports System.IdentityModel.Tokens
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext
Imports Microsoft.PowerBI.Api.V2
Imports Microsoft.PowerBI.Api.V2.Models
Imports Microsoft.Rest

Public Class PowerBIHelper
    Public Shared Async Function GetPowerBIToken() As Task(Of String)

        Dim powerBiApiUrl = ConfigManager.PowerBiApiUrl()
        Dim resourceUri = ConfigManager.PowerBiAPIResource()
        Dim authorityUri = ConfigManager.AADAuthorityUri()
        Dim clientID = ConfigManager.ApplicationID()
        Dim powerBIUserName = ConfigManager.PowerBIUserName()
        Dim powerBIUserPassword = ConfigManager.PowerBIUserPassword()

        Dim authContext As Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext = New Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext(authorityUri)
        Dim cred = New UserPasswordCredential(powerBIUserName, powerBIUserPassword)

        Try
            Dim response = authContext.AcquireTokenAsync(resourceUri, clientID, cred)
            Dim authenticationResult = response.Result
            Return authenticationResult.AccessToken

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
            Return Nothing
        End Try

    End Function

    Public Shared Async Function GetPowerBIEmbeddedUrl(ByVal reportId As String) As Task(Of String)
        Try
            Dim embedUrl
            Dim powerBiApiUrl = ConfigManager.PowerBiApiUrl()
            Dim token As String = GetPowerBIToken().Result

            Using client = New PowerBIClient(New Uri(powerBiApiUrl), New TokenCredentials(token, "Bearer"))
                Dim report = New Microsoft.PowerBI.Api.V2.Models.Report
                report = client.Reports.GetReports().Value.FirstOrDefault(Function(r) r.Id = reportId)

                If (report IsNot Nothing) Then
                    embedUrl = report.EmbedUrl
                End If

                Return embedUrl
            End Using

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
            Return Nothing
        End Try

    End Function

    'Dim reportId = ConfigManager.ReportId()
    'Using Client = New PowerBIClient(New Uri(powerBiApiUrl), New TokenCredentials(authenticationResult.AccessToken, "Bearer"))
    '    Dim report = New Microsoft.PowerBI.Api.V2.Models.Report
    '    report = Client.Reports.GetReports().Value.FirstOrDefault(Function(r) r.Id = reportId)
    '    Dim embedUrl = report.EmbedUrl
    'End Using

End Class



