﻿Imports System.Security.Cryptography
Imports System.Text

Public Class CryptoHelper

    Public Shared Function EncodePasswordToBase64(ByVal password As String) As String
        Try
            Dim encData_byte As Byte() = New Byte(password.Length - 1) {}
            encData_byte = System.Text.Encoding.UTF8.GetBytes(password)
            Dim encodedData As String = Convert.ToBase64String(encData_byte)
            Return encodedData
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function HashPassword(ByVal password As String) As String
        Try
            Dim salt As String = ConfigManager.getSaltForHash()
            Dim bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(password + salt)
            Dim sha256hashstring As System.Security.Cryptography.SHA256Managed = New System.Security.Cryptography.SHA256Managed()
            Dim hash As Byte() = sha256hashstring.ComputeHash(bytes)
            Return Convert.ToBase64String(hash)

        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function Encrypt(ByVal input As String, ByVal key As String) As String
        Try
            Dim inputArray As Byte() = UTF8Encoding.UTF8.GetBytes(input)
            Dim tripleDES As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key)
            tripleDES.Mode = CipherMode.ECB
            tripleDES.Padding = PaddingMode.PKCS7
            Dim cTransform As ICryptoTransform = tripleDES.CreateEncryptor()
            Dim resultArray As Byte() = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length)
            tripleDES.Clear()
            Return Convert.ToBase64String(resultArray, 0, resultArray.Length)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
    Public Shared Function Decrypt(ByVal input As String, ByVal key As String) As String
        Try
            Dim inputArray As Byte() = Convert.FromBase64String(input)
            Dim tripleDES As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key)
            tripleDES.Mode = CipherMode.ECB
            tripleDES.Padding = PaddingMode.PKCS7
            Dim cTransform As ICryptoTransform = tripleDES.CreateDecryptor()
            Dim resultArray As Byte() = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length)
            tripleDES.Clear()
            Return UTF8Encoding.UTF8.GetString(resultArray)
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

End Class
