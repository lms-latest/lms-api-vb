﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Xml
Imports System.IO
Imports System.Web
Imports System.Web.Caching
Imports System.Data


Public Class ExceptionLogger


    Private Shared FileName As String = "Exception.xml"

    Private Shared FileNameWithDate As String = ("ExceptionLog" _
                    + (DateTime.Now.ToString("MM-dd-yyyy") + ".xml"))

    Private Shared FilePathException As String = (IOHelper.FilesPath + ("\" + FileName))

    Private Shared FilePathExceptionWithDate As String = (IOHelper.LogPathXML + ("\" + FileNameWithDate))

    Private Shared XmlDoc As XmlDocument

    Private Shared EXCEPTION_DOC_CACHE_KEY As String = "EXCEPTION_DOC_CACHE_KEY"

    Public Shared UnHandledException As Exception = Nothing

    Public Overloads Shared Sub Log(ByVal exception As Exception)
        ExceptionLogger.Log(exception, "")
    End Sub

    Public Overloads Shared Sub Log()
        If (Not (UnHandledException) Is Nothing) Then
            ExceptionLogger.Log(UnHandledException)
            UnHandledException = Nothing
        End If

    End Sub

    Public Overloads Shared Sub Log(ByVal exception As Exception, ByVal LoginUserUser As String)
        ' throw exception;
        Try
            ' XmlDoc = GetDocument();
            XmlDoc = ExceptionLogger.GetDocumentWithDate
            Dim nodeException As XmlNode = XmlDoc.CreateElement("Exception")
            nodeException.InnerXml = ("<LoginUserID>" _
                            + (LoginUserUser + "</LoginUserID>"))
            nodeException.InnerXml = (nodeException.InnerXml + ("<TimeLogged>" _
                            + (DateTime.Now.ToString + "</TimeLogged>")))
            If (Not (exception.InnerException) Is Nothing) Then
                nodeException.InnerXml = (nodeException.InnerXml + ("<Message>" _
                                + (exception.InnerException.Message + "</Message>")))
            Else
                nodeException.InnerXml = (nodeException.InnerXml + ("<Message>" _
                                + (exception.Message + "</Message>")))
            End If

            nodeException.InnerXml = (nodeException.InnerXml + ("<StackTrace><![CDATA[" _
                            + (exception.StackTrace + "]]></StackTrace>")))
            XmlDoc.DocumentElement.AppendChild(nodeException)
            Dim Appil As String = AppDomain.CurrentDomain.BaseDirectory
            'IOHelper.SaveFile(XmlDoc, FilePathException);
            IOHelper.SaveFile(XmlDoc, FilePathExceptionWithDate)
        Catch ex As Exception
            'new Exception("Access to the Exception Log File is denied");
            '''TODo:Mail to site admin about the critical issue 
        End Try

    End Sub

    Private Shared Function GetDocumentWithDate() As XmlDocument
        Try
            If (HttpContext.Current.Cache("EXCEPTION_DOC_CACHE_KEY") Is Nothing) Then
                XmlDoc = New XmlDocument
                'Write Root Element if Missing
                If (File.Exists(FilePathExceptionWithDate) = False) Then
                    ExceptionLogger.WriteRootElement()

                    Try
                        IOHelper.SaveFile(XmlDoc, FilePathExceptionWithDate)
                    Catch exceptions As IOException
                        '''TODo:Mail to site admin about the critical issue 
                    End Try

                End If

                Try
                    XmlDoc.Load(FilePathExceptionWithDate)
                Catch exception As System.Xml.XmlException
                    ExceptionLogger.WriteRootElement()
                End Try

                Dim cachedependency As CacheDependency = New CacheDependency(FilePathExceptionWithDate)
                HttpContext.Current.Cache.Insert(EXCEPTION_DOC_CACHE_KEY, XmlDoc, cachedependency)
                Return XmlDoc
            End If

            XmlDoc = CType(HttpContext.Current.Cache(EXCEPTION_DOC_CACHE_KEY), XmlDocument)
            Return XmlDoc
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Private Shared Function GetDocument() As XmlDocument
        Try
            If (HttpContext.Current.Cache("EXCEPTION_DOC_CACHE_KEY") Is Nothing) Then
                XmlDoc = New XmlDocument
                'Write Root Element if Missing
                If (File.Exists(FilePathException) = False) Then
                    ExceptionLogger.WriteRootElement()

                    Try
                        IOHelper.SaveFile(XmlDoc, FilePathException)
                    Catch exceptions As IOException
                        '''TODo:Mail to site admin about the critical issue 
                    End Try

                End If

                Try
                    XmlDoc.Load(FilePathException)
                Catch exception As System.Xml.XmlException
                    ExceptionLogger.WriteRootElement()
                End Try

                Dim cachedependency As CacheDependency = New CacheDependency(FilePathException)
                HttpContext.Current.Cache.Insert(EXCEPTION_DOC_CACHE_KEY, XmlDoc, cachedependency)
                Return XmlDoc
            End If

            XmlDoc = CType(HttpContext.Current.Cache(EXCEPTION_DOC_CACHE_KEY), XmlDocument)
            Return XmlDoc
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Private Shared Sub WriteRootElement()
        Try
            If ((Not (XmlDoc) Is Nothing) _
                        AndAlso (XmlDoc.InnerXml.Length = 0)) Then
                XmlDoc.AppendChild(XmlDoc.CreateElement("Root"))
            End If
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Sub

    Public Shared Function GetExceptions() As DataSet
        Try
            Dim dsExceptions As DataSet = New DataSet
            dsExceptions.ReadXml(FilePathException)
            Return dsExceptions
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class

