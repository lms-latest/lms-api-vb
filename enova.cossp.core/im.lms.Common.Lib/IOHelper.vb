﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.Xml
Imports System.Web.UI.HtmlControls

Public Class IOHelper

    Public Shared AppilcationPath As String = AppDomain.CurrentDomain.BaseDirectory

    Public Shared FilesPath As String

    Public Shared AppilcationPathWithDate As String = AppDomain.CurrentDomain.BaseDirectory

    Public Shared PerformanceLog As String = (AppilcationPathWithDate + "PerformanceLog")

    Public Shared LogPathXML As String

    Public Overloads Shared Sub SaveFile(ByVal file As FileInfo, ByVal path As String)
        'Exception handling has Deliberately left   
        file.Create
    End Sub

    Public Shared Function GetLastUpdatedFileInDirectory() As String
        Try
            Dim di As DirectoryInfo = New DirectoryInfo(LogPathXML)
            Dim xmlFiles() As FileInfo = di.GetFiles
            Dim lastUpdatedFile As FileInfo = Nothing
            Dim lastUpdate As DateTime = New DateTime(1999, 1, 1)
            For Each file As FileInfo In xmlFiles
                If (file.LastAccessTime > lastUpdate) Then
                    lastUpdatedFile = file
                    lastUpdate = file.LastAccessTime
                End If

            Next
            Return lastUpdatedFile.Name.ToString
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Overloads Shared Sub SaveFile(ByVal file As XmlDocument, ByVal path As String)
        'Exception handling has Deliberately left   
        file.Save(path)
    End Sub

    Public Shared Function UploadFile(ByVal hfile As HtmlInputFile, ByVal fName As String, ByVal FolderName As String) As String
        Try
            Dim fileName As String = ""
            Dim Path As String = (IOHelper.BaseDirectory + FolderName)
            If Not System.IO.Directory.Exists(Path) Then
                System.IO.Directory.CreateDirectory(Path)
            End If

            fileName = System.IO.Path.GetFileName(hfile.PostedFile.FileName.ToString)
            Dim file As String = (Path + fileName)
            ' hfile.PostedFile.FileName.Substring(hfile.PostedFile.FileName.LastIndexOf(c)); // +fName;
            Try
                hfile.PostedFile.SaveAs(file)
                'fileName = System.IO.Path.GetFileName(file);
            Catch ex As System.UnauthorizedAccessException
                Throw New Exception("Access to the designated upload folder is denied.Please contact Site Admin.")
            Catch ex As Exception
                fileName = ("Error : " + ex.Message)
                Throw New Exception("Problem while uploading the file.Please try again or contact Site Admin.")
            End Try

            Return fileName
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

    Public Shared Function PDFFolder() As String
        Return System.Configuration.ConfigurationSettings.AppSettings("PDFFolder").ToString
    End Function

    Public Shared Function AtexFolder() As String
        Return System.Configuration.ConfigurationSettings.AppSettings("AtexFolder").ToString
    End Function

    Public Shared Function BaseDirectory() As String
        Return AppDomain.CurrentDomain.BaseDirectory.ToString
    End Function

    Public Shared Sub WriteInFile(ByVal FileName As String, ByVal Text As String)
        ' create a writer and open the file
        Dim tw As TextWriter = New StreamWriter((PerformanceLog + ("\" + FileName)), True)
        tw.WriteLine(Text)
        tw.Close
        'TextWriter tw = new StreamWriter(PerformanceLog + "\\" + FileName, true);
        ' write a line of text to the file
        'tw.WriteLine(Text, true);
        ' close the stream
        'tw.Close();
    End Sub

    Public Shared Function ReadFromFile(ByVal FileName As String) As String
        Try
            Dim fileStream As StreamReader = New StreamReader(FileName)
            fileStream = File.OpenText(FileName)
            Dim fileContents As String
            fileContents = fileStream.ReadToEnd
            fileStream.Close()
            Return fileContents
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function
End Class
