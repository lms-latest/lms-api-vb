﻿Imports System.IdentityModel.Tokens.Jwt
Imports System.Net
Imports System.Net.Http
Imports System.Threading
Imports System.Threading.Tasks
Imports Microsoft.IdentityModel.Tokens
Imports enova.cossp.Common.Lib
Imports System.Security.Claims

Class TokenValidationHandler
    Inherits DelegatingHandler


    Private Shared Function TryRetrieveToken(ByVal request As HttpRequestMessage, ByRef token As String) As Boolean
        token = Nothing
        Dim authzHeaders As IEnumerable(Of String)

        If (Not request.Headers.TryGetValues("Authorization", authzHeaders) _
                    OrElse (authzHeaders.Count > 1)) Then
            Return False
        End If
        Dim bearerToken = authzHeaders.ElementAt(0)
        token = bearerToken.Substring(7)
        Return True

    End Function

    Private Shared Function SenUnAuthorizedResponse() As Task(Of HttpResponseMessage)
        Dim response As New HttpResponseMessage
        Dim lblnIsAuthenticated As Boolean = False
        Dim tsc As New TaskCompletionSource(Of HttpResponseMessage)

        response.StatusCode = HttpStatusCode.Unauthorized
        response.Headers.Add("Status-Message", "Authentication failed.")
        tsc.SetResult(response)
        Return tsc.Task

    End Function


    Protected Overrides Function SendAsync(ByVal request As HttpRequestMessage, ByVal cancellationToken As CancellationToken) As Task(Of HttpResponseMessage)
        Dim statusCode As HttpStatusCode
        Dim requestMessage As HttpRequestMessage = New HttpRequestMessage
        'Dim response As New HttpResponseMessage
        Dim tcs As New TaskCompletionSource(Of HttpResponseMessage)
        Dim token As String
        Try
            'Dim issuer = ConfigManager.GetIssuer()
            'Dim audience = ConfigManager.GetAudience()
            ' And Not request.Headers.Contains("Access-Control-Allow-Origin: *")
            If Not IsRequestEndsWithAllowedAPI(request.RequestUri.ToString()) And Not request.Method.Method.Contains("OPTIONS") Then

                If Not TryRetrieveToken(request, token) Then
                    statusCode = HttpStatusCode.Unauthorized
                    Return SenUnAuthorizedResponse()
                    'Return MyBase.SendAsync(request, cancellationToken)
                End If

                Const sec As String = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1"
                Dim now = DateTime.UtcNow
                Dim securityKey = New Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.[Default].GetBytes(sec))
                Dim securityToken As SecurityToken
                Dim handler As JwtSecurityTokenHandler = New JwtSecurityTokenHandler()
                Dim validationParameters As TokenValidationParameters = New TokenValidationParameters() With {
                .ValidAudience = "http://localhost:64403",
                .ValidIssuer = "http://localhost:64403",
                .ValidateLifetime = True,
                .ValidateIssuerSigningKey = True,
                .IssuerSigningKey = securityKey
            }
                Dim tokenExpiryTime = ConfigManager.GetTokenExpiryTime()
                Dim handlerr = New JwtSecurityTokenHandler()
                Dim tokenn = TryCast(handlerr.ReadToken(token), JwtSecurityToken)
                Dim tokenExpiryDate = tokenn.ValidTo
                Dim tokenExpiryInMinutes = tokenExpiryTime / 2
                Dim minutesdiff = (tokenExpiryDate - DateTime.UtcNow).TotalMinutes

                Dim identity = CType(Thread.CurrentPrincipal, ClaimsPrincipal)
                Dim name = tokenn.Claims.Where(Function(c) c.Type = "unique_name").[Select](Function(c) c.Value).SingleOrDefault()
                Thread.CurrentPrincipal = handler.ValidateToken(token, validationParameters, securityToken)
                HttpContext.Current.User = handler.ValidateToken(token, validationParameters, securityToken)

                'Return MyBase.SendAsync(request, cancellationToken)

                Return MyBase.SendAsync(request, cancellationToken).ContinueWith(
                    Function(task)
                        Dim response = task.Result

                        If (minutesdiff < tokenExpiryInMinutes) Then
                            token = SetRefreshToken(name)
                            response.Headers.Add("Access-Control-Expose-Headers", "AuthorizationToken")
                            response.Headers.Add("AuthorizationToken", token)
                            'response.Headers.Add("Access-Control-Allow-Origin", "*")
                        End If

                        Return response
                    End Function, cancellationToken)

            End If
        Catch e As SecurityTokenValidationException
            statusCode = HttpStatusCode.Unauthorized
            Log4NetHelper.LogError(Nothing, e.ToString())
            Return SenUnAuthorizedResponse()
        Catch ex As Exception
            statusCode = HttpStatusCode.InternalServerError
            Log4NetHelper.LogError(Nothing, ex.ToString())
            Return SenUnAuthorizedResponse()
        End Try
        Return MyBase.SendAsync(request, cancellationToken)
        'Return Task < HttpResponseMessage > .Factory.StartNew(() >= New HttpResponseMessage(statusCode) {})
    End Function

    Public Function LifetimeValidator(ByVal notBefore As DateTime?, ByVal expires As DateTime?, ByVal securityToken As SecurityToken, ByVal validationParameters As TokenValidationParameters) As Boolean
        If (Not (expires) Is Nothing) Then
            If (DateTime.UtcNow < expires) Then
                Return True
            End If

        End If

        Return False
    End Function

    Private Shared Function IsRequestEndsWithAllowedAPI(ByVal requestUrl As String) As Boolean

        Dim allowedAPIs = ConfigManager.AllowedAPIEndPoints

        ' Split string based on comma.
        Dim endPoints As String() = allowedAPIs.Split(New Char() {","c})
        Dim endPoint As String
        For Each endPoint In endPoints
            If requestUrl.Contains("/" + endPoint) Then
                Return True
            End If
        Next

        Return False
    End Function
    Private Shared Function SetRefreshToken(ByVal username As String) As String
        Try
            Dim tokenExpiryTime = ConfigManager.GetTokenExpiryTime()
            'Dim issuer = ConfigManager.GetIssuer()
            'Dim audience = ConfigManager.GetAudience()
            Dim issuedAt As DateTime = DateTime.UtcNow
            Dim expires As DateTime = DateTime.UtcNow.AddMinutes(tokenExpiryTime)
            Dim tokenHandler = New JwtSecurityTokenHandler()
            Dim claimsIdentity As ClaimsIdentity = New ClaimsIdentity({New Claim(ClaimTypes.Name, username)})

            Const sec As String = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1"
            Dim now = DateTime.UtcNow
            Dim securityKey = New Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.[Default].GetBytes(sec))
            Dim signingCredentials = New Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature)
            Dim token = CType(tokenHandler.CreateJwtSecurityToken(issuer:="http://localhost:64403", audience:="http://localhost:64403", subject:=claimsIdentity, notBefore:=issuedAt, expires:=expires, signingCredentials:=signingCredentials), JwtSecurityToken)
            Dim tokenString = tokenHandler.WriteToken(token)
            Return tokenString
        Catch ex As Exception
            Log4NetHelper.LogError(Nothing, ex.ToString())
        End Try
    End Function

End Class
