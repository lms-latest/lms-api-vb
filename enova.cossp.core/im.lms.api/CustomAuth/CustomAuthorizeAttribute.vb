﻿Imports Microsoft.IdentityModel.Tokens
Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IdentityModel.Tokens.Jwt
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Text
Imports System.Web
Imports System.Web.Http
Imports System.Web.Http.Controllers
Imports enova.cossp.api.APIControllers
Imports enova.cossp.entities

Public Class CustomAuthorizeAttribute
    Inherits AuthorizeAttribute

    Public Overrides ReadOnly Property AllowMultiple As Boolean
        Get
            Return False
        End Get
    End Property

    Public Overrides Sub OnAuthorization(ByVal actionContext As HttpActionContext)
        If actionContext Is Nothing Then
        End If
        Try
            Dim ControllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName
            Dim UserName = HttpContext.Current.User.Identity.Name
            Dim AllowedControllers As New List(Of AllowedControllers)
            Dim screenController = New ScreenController()
            AllowedControllers = screenController.GetListOfAllowedControllers(UserName, ControllerName)
            'Dim x As List(Of AllowedControllers) = AllowedControllers.Where(Function(n) n.ControllerName.Equals(ControllerName))
            If AllowedControllers.Count > 0 Then

            Else
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized)
            End If


        Catch ex As Exception
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized)
        End Try
    End Sub

    Protected Overrides Function IsAuthorized(ByVal context As HttpActionContext) As Boolean
        Return True
    End Function
End Class
