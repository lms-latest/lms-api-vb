﻿Imports Microsoft.IdentityModel.Tokens

Public Class Token
    Public access_token As String
    Public expires_in As String
    Public fullname As String
    Public username As String
    Public RoleName As String
    Public UserId As Integer
    Public ID As Integer
    Public TeamMemberID As Integer?
End Class
Public Class TokenProviderOptions
    Public Property Path As String
    Public Property Issuer As String
    Public Property Audience As String
    Public Property Expiration As TimeSpan
    Public Property SigningCredentials As SigningCredentials
End Class

