﻿Imports System.Net.Http

Public Class Models

End Class
Public Class LoginRequest

    Private usernameValue As String
    Public Property Username() As String
        Get
            Return usernameValue
        End Get
        Set(ByVal value As String)
            usernameValue = value
        End Set
    End Property

    Private passwordValue As String
    Public Property Password() As String
        Get
            Return passwordValue
        End Get
        Set(ByVal value As String)
            passwordValue = value
        End Set
    End Property
    Private rememberMeValue As Boolean
    Public Property RememberMe() As Boolean
        Get
            Return rememberMeValue
        End Get
        Set(ByVal value As Boolean)
            rememberMeValue = value
        End Set
    End Property
End Class

Public Class LoginResponse

    Public Sub New()
        MyBase.New
        Me.tokenValue = ""
        Me.responseMsg = New HttpResponseMessage
    End Sub

    Private tokenValue As String
    Public Property Token() As String
        Get
            Return tokenValue
        End Get
        Set(ByVal value As String)
            tokenValue = value
        End Set
    End Property

    Private responseMsgValue As HttpResponseMessage
    Public Property responseMsg() As HttpResponseMessage
        Get
            Return responseMsgValue
        End Get
        Set(ByVal value As HttpResponseMessage)
            responseMsgValue = value
        End Set
    End Property

    Private userNameValue As String
    Public Property userName() As String
        Get
            Return userNameValue
        End Get
        Set(ByVal value As String)
            userNameValue = value
        End Set
    End Property

    Private contractCountValue As Integer
    Public Property contractCount() As Integer
        Get
            Return contractCountValue
        End Get
        Set(ByVal value As Integer)
            contractCountValue = value
        End Set
    End Property

    Private buildingCountValue As Integer
    Public Property buildingCount() As Integer
        Get
            Return buildingCountValue
        End Get
        Set(ByVal value As Integer)
            buildingCountValue = value
        End Set
    End Property

    Private jobTitleValue As String
    Public Property jobTitle() As String
        Get
            Return jobTitleValue
        End Get
        Set(ByVal value As String)
            jobTitleValue = value
        End Set
    End Property

    Private profilePictureValue As String
    Public Property profilePicture() As String
        Get
            Return profilePictureValue
        End Get
        Set(ByVal value As String)
            profilePictureValue = value
        End Set
    End Property

    Private isCSFScreenAccessValue As Boolean
    Public Property isCSFScreenAccess() As Boolean
        Get
            Return isCSFScreenAccessValue
        End Get
        Set(ByVal value As Boolean)
            isCSFScreenAccessValue = value
        End Set
    End Property

    Private allowedScreenUrlValue As List(Of String)
    Public Property allowedScreenUrl() As List(Of String)
        Get
            Return allowedScreenUrlValue
        End Get
        Set(ByVal value As List(Of String))
            allowedScreenUrlValue = value
        End Set
    End Property
End Class

