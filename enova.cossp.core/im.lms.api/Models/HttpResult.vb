﻿Public Class HttpResult

    Private messageValue As String
    Public Property Message() As String
        Get
            Return messageValue
        End Get
        Set(ByVal value As String)
            messageValue = value
        End Set
    End Property

    Private successValue As Boolean

    Public Property Success() As Boolean
        Get
            Return successValue
        End Get
        Set(ByVal value As Boolean
            )
            successValue = value
        End Set
    End Property

    Private dataValue As Object
    Public Property Data() As Object
        Get
            Return dataValue
        End Get
        Set(ByVal value As Object)
            dataValue = value
        End Set
    End Property
    Private tokenValue As String
    Public Property Token() As String
        Get
            Return tokenValue
        End Get
        Set(ByVal value As String)
            tokenValue = value
        End Set
    End Property



End Class
