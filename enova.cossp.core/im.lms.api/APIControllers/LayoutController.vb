﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class LayoutController
        Inherits ApiController
        <HttpGet>
        Public Function GetNotificationsByUserID() As List(Of Notification)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                Dim notification As List(Of Notification) = New List(Of Notification)()
                Dim notificationManager As NotificationManager = New NotificationManager()
                Return notificationManager.GetNotificationsByUserID(user.ID)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function


        <HttpGet>
        Public Function GetUserSidebarScreenList() As List(Of NavigationScreen)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim screenList As List(Of NavigationScreen) = New List(Of NavigationScreen)()
                Dim userManager As UserManager = New UserManager()
                Return userManager.GetUserSidebarScreenList(userName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpPut>
        Public Sub UpdateNotificationIsRead(ByVal id As Integer)
            Try
                Dim notificationManager As NotificationManager = New NotificationManager()
                notificationManager.SetNotificationIsRead(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try


        End Sub



    End Class
End Namespace