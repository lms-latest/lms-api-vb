﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ProfileModerationController
        Inherits ApiController

        <HttpGet>
        Public Function GetOldUserProfileData(ByVal id As Integer) As ProfileModeration
            Try
                Dim userName As String = HttpContext.Current.User.Identity.Name
                'Dim profileOldData As ProfileModeration = New ProfileModeration()
                'profileOldData.ID = GetUserId()
                Dim profileModerationManager As ProfileModerationManager = New ProfileModerationManager()
                Return profileModerationManager.GetOldUserProfileData(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function GetNewUserProfileData(ByVal id As Integer) As ProfileModeration
            Try
                Dim userName As String = HttpContext.Current.User.Identity.Name
                Dim profileNewData As ProfileModeration = New ProfileModeration()
                profileNewData.ID = GetUserId()
                Dim profileModerationManager As ProfileModerationManager = New ProfileModerationManager()
                Return profileModerationManager.GetNewUserProfileData(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        Private Shared Function GetUserId() As Integer
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim userDetails = userManager.GetUserByUserName(userName)
                Return userDetails.ID
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        '<HttpGet>
        'Public Function GetListOfJobTitle() As List(Of JobTitle)
        '    Try
        '        Dim jobTitle As List(Of JobTitle) = New List(Of JobTitle)()
        '        Dim jobTitleManager As JobTitleManager = New JobTitleManager()
        '        Return jobTitleManager.GetListOfJobTitle()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function

        '<Route(“api/ProfileModeration/GetListOfUserProfileDraftData/{Status}”)>
        <HttpPost>
        Public Function GetListOfUserProfileDraftData(ByVal obj As ProfileModerationPaging) As PaggedResult(Of ProfileModeration)
            Try
                Dim profileModerationManager As ProfileModerationManager = New ProfileModerationManager()
                Dim result As PaggedResult(Of ProfileModeration) = profileModerationManager.GetListOfUserProfileDraftData(obj.Status, obj.Paging)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        '<HttpGet>
        'Public Function GetListOfUserProfileDraftData(ByVal status As String) As List(Of ProfileModeration)
        '    Try
        '        Dim user As List(Of ProfileModeration) = New List(Of ProfileModeration)()
        '        Dim profileModerationManager As ProfileModerationManager = New ProfileModerationManager()
        '        Return profileModerationManager.GetListOfUserProfileDraftData(status)
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function

        <HttpPost>
        Public Function SaveProfileModeration(ByVal profileModerationData As ProfileModeration)
            Dim sessonId As String
            Try
                Dim result As String

                Dim profileModerationManager As ProfileModerationManager = New ProfileModerationManager()
                If (profileModerationData.Status.Equals("Approved")) Then
                    sessonId = Authenticate()
                    Dim user As User = New User()
                    user = profileModerationManager.GetProfileUserByUserName(profileModerationData.UserName)

                    Dim obj As Cafm.ContactService.ContactServiceClient = New Cafm.ContactService.ContactServiceClient()
                    Dim objContact As Cafm.ContactService.ContactDto = New Cafm.ContactService.ContactDto()

                    objContact.ContactId = user.cafmUserID '"24296"
                    objContact.EmailAddress = profileModerationData.Email
                    objContact.MobileTelephoneNumber = profileModerationData.MobileNo
                    objContact.OfficeTelephoneNumber = profileModerationData.PhoneNo
                    objContact.JobTitle = profileModerationData.JobTitle
                    objContact.FirstName = profileModerationData.FirstName
                    objContact.Surname = profileModerationData.LastName
                    objContact.KP_COMPANY = profileModerationData.Company
                    'objContact.PictureUrl = user.profilePicture
                    'objContact.JobTitle = userManager.GetJobTitleByUser(user.ID).Name
                    'objContact.ContactId = user.mobileNo
                    'Dim userName As String = HttpContext.Current.User.Identity.Name
                    result = obj.UpdateContact(sessonId, Nothing, objContact)
                    Logout(sessonId)

                End If


                If (result IsNot Nothing Or profileModerationData.Status.Equals("Rejected")) Then
                    profileModerationData.CreatedBy = GetUserId()
                    profileModerationManager.SaveProfileModeration(profileModerationData)
                    Return "updated successfully"
                End If


            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Logout(sessonId)
                ' Return ex.Message
            End Try

        End Function
        <HttpGet>
        Public Function GetUserPhotoUrl() As String
            Try
                Dim request As HttpRequest = HttpContext.Current.Request
                Dim fileName As String = "avatar.png"
                Dim filePath As String = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/" & fileName)

                If File.Exists(filePath) Then
                    Dim appUrl As String = String.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Host, If(request.Url.Port = 80, String.Empty, String.Format(":{0}", request.Url.Port)))
                    Dim downloadFilePath As String = appUrl & "/Uploads/ProfilePic/" & fileName
                    Return downloadFilePath
                Else

                End If
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpPost>
        Public Function Authenticate()
            Try
                Dim obj As Cafm.SecurityService.SecurityServiceClient = New Cafm.SecurityService.SecurityServiceClient()
                Dim loginName = ConfigManager.GetCafmSeviceLoginName()
                Dim password = ConfigManager.GetCafmSevicePassword()
                Dim result = obj.Authenticate(loginName, password)
                Return result.SessionId
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function
        <HttpPost>
        Public Function Logout(ByVal sessionId As String)
            Try
                Dim obj As Cafm.SecurityService.SecurityServiceClient = New Cafm.SecurityService.SecurityServiceClient()
                obj.LogOut(sessionId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

    End Class
End Namespace