﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class CSFController
        Inherits ApiController

        <HttpPost>
        Public Function SaveCSF(ByVal csf As CSF)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim response = cafmManager.SaveCSF(csf)
                Return response
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function


        <HttpGet>
        <Route(“api/CSF/GetCSFTaskDetails/{taskSeq}”)>
        Public Function GetCSFTaskDetails(taskSeq As Integer)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim taskDetails = cafmManager.GetTaskDetail(taskSeq)
                Return taskDetails
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetCSFTasks()
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim notificationManager As NotificationManager = New NotificationManager()

                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userDetails = UserManager.GetUserDetails()
                Dim phoneNumber = userDetails.phoneNo
                Dim email = userDetails.email
                Dim userID = userDetails.ID
                Dim message = ConfigManager.MessageForCSFTypeNotification()

                Dim taskDetails As List(Of CafmTask) = cafmManager.GetListIOfCSFTasks(phoneNumber, email)
                Dim csfNotificationType = ConfigManager.NotificationCSFType()

                If taskDetails IsNot Nothing Then

                    For Each task In taskDetails
                        Dim taskNotification As Notification = New Notification()
                        taskNotification.userID = userID
                        taskNotification.type = csfNotificationType
                        taskNotification.referenceID = task.TaskSeq
                        taskNotification.message = message + task.TaskID.ToString()
                        taskNotification.CreatedBy = userID
                        taskNotification.ModifiedBy = userID
                        notificationManager.SaveNotification(taskNotification)
                    Next

                End If

                Return True

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpPost>
        Public Function CheckCSFDone(ByVal csf As CSF)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim response = cafmManager.CheckCSFDone(csf.TaskId)
                Return response
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

    End Class
End Namespace