﻿Imports System.IdentityModel.Tokens.Jwt
Imports System.IO
Imports System.Net
Imports System.Net.Http
Imports System.Security.Claims
Imports System.Web.Http
Imports Microsoft.IdentityModel.Tokens
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib
Imports System.Security.Cryptography


Namespace APIControllers
    Public Class AuthController
        Inherits ApiController

        <HttpPost>
        Public Function Authenticate(ByVal login As LoginRequest) As IHttpActionResult
            Try
                Dim cryptoPrivateKey As String = ConfigManager.cryptoPrivateKey()
                Dim ByteConverter As UnicodeEncoding = New UnicodeEncoding()
                Dim RSA As RSACryptoServiceProvider = New RSACryptoServiceProvider()
                Dim decryptedPassword As String
                Dim decryptedtex As Byte() = Decryption(Convert.FromBase64String(login.Password), False)
                decryptedPassword = Encoding.ASCII.GetString(decryptedtex)
                'Dim httpresult As HttpResult = New HttpResult
                Dim hashed As String = ""
                Dim loginResponse As LoginResponse = New LoginResponse
                Dim loginrequest As LoginRequest = New LoginRequest
                loginrequest.Username = login.Username
                '  Dim password As String = Decryption(login.Password)
                loginrequest.Password = decryptedPassword
                loginrequest.RememberMe = login.RememberMe
                Dim response As IHttpActionResult
                Dim responseMsg As HttpResponseMessage = New HttpResponseMessage()
                Dim isUsernamePasswordValid As Boolean = False
                Dim serviceResponse As Boolean = False
                Dim validPassword = GetUserByUserName(loginrequest.Username)
                If (validPassword.IsAldapUser.Equals(True)) Then

                    Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
                    Dim url = cafmApiUrl + CafmAPIEndPoints.LDAPLoginURL
                    Dim cafmManager As CafmManager = New CafmManager()
                    serviceResponse = cafmManager.GetLDAPLogin(login.Username, loginrequest.Password)
                    If (serviceResponse.Equals(False)) Then
                        Dim eventLog As EventLog = New EventLog
                        eventLog.Comments = "Login Failed"
                        eventLog.ID = validPassword.ID
                        eventLog.EventType = "Login Failed"
                        eventLog.LoggedBy = validPassword.ID

                        Dim EventLogManager As EventLogManager = New EventLogManager()
                        EventLogManager.SaveEventLog(eventLog)
                        loginResponse.responseMsg.StatusCode = HttpStatusCode.Forbidden
                        response = ResponseMessage(loginResponse.responseMsg)
                        Return response
                    Else
                        isUsernamePasswordValid = True
                    End If

                End If
                If (validPassword.IsAldapUser.Equals(False)) Then
                    hashed = HashPassword(loginrequest.Password)
                    If login IsNot Nothing Then isUsernamePasswordValid = If(hashed = validPassword.password, True, False)
                End If


                ' Dim hashed = HashPassword(loginrequest.Password)

                'If login IsNot Nothing Then isUsernamePasswordValid = If(EncodePasswordToBase64(loginrequest.Password) = validPassword.password, True, False)
                'If login IsNot Nothing Then isUsernamePasswordValid = If(loginrequest.Password = validPassword.password, True, False)

                If isUsernamePasswordValid Then
                    Dim userManager As UserManager = New UserManager()
                    Dim userDetails = userManager.GetUserByUserName(login.Username)
                    Dim eventLog As EventLog = New EventLog
                    eventLog.Comments = "Login Successful"
                    eventLog.ID = userDetails.ID
                    eventLog.EventType = "Login Successful"
                    'eventLog.LoggedByName = userDetails.userName
                    eventLog.LoggedBy = userDetails.ID
                    'eventLog.LastLoggedOn = System.DateTime.Now
                    Dim EventLogManager As EventLogManager = New EventLogManager()
                    EventLogManager.SaveEventLog(eventLog)
                    Dim token As String = createToken(loginrequest.Username, loginrequest.RememberMe)
                    loginResponse.userName = validPassword.Name
                    loginResponse.contractCount = userManager.GetListOfContractByUser(validPassword.ID).ToList().Count()
                    loginResponse.buildingCount = userManager.GetListOfBuildingByUser(validPassword.ID, validPassword.IsAllBuilding).ToList().Count()
                    loginResponse.Token = token
                    loginResponse.jobTitle = validPassword.jobTitle
                    loginResponse.profilePicture = CheckIfFileExists(validPassword.profilePicture)

                    Dim list = New List(Of String)
                    Dim screenList As List(Of NavigationScreen) = userManager.GetUserSidebarScreenList(userDetails.userName)

                    Dim screen As NavigationScreen
                    For Each screen In screenList
                        list.Add(screen.URL)
                    Next

                    loginResponse.allowedScreenUrl = list

                    Dim screenManager = New ScreenManager()
                    Dim csfScreenId = ConfigManager.GetCSFScreenId()
                    Dim accessCount = screenManager.ValidateScreenAccessForUser(validPassword.ID, csfScreenId)

                    If (accessCount > 0) Then
                        loginResponse.isCSFScreenAccess = True
                    Else
                        loginResponse.isCSFScreenAccess = False
                    End If


                    Return Ok(Of LoginResponse)(loginResponse)
                Else
                    Dim userManager As UserManager = New UserManager()
                    Dim userDetails = userManager.GetUserByUserName(login.Username)
                    Dim eventLog As EventLog = New EventLog
                    eventLog.Comments = "Login Failed"
                    eventLog.ID = userDetails.ID
                    eventLog.EventType = "Login Failed"
                    eventLog.LoggedBy = userDetails.ID

                    Dim EventLogManager As EventLogManager = New EventLogManager()
                    EventLogManager.SaveEventLog(eventLog)
                    loginResponse.responseMsg.StatusCode = HttpStatusCode.Forbidden

                    response = ResponseMessage(loginResponse.responseMsg)
                    Return response
                End If

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        'Public Function GetUserByUserName(ByVal userName As UserName) As Users
        '    Dim user As Users = New Users()
        '    Dim userManager As UserManager = New UserManager()
        '    Return userManager.GetUserByUserName(userName)
        'End Function

        Public Function CheckIfFileExists(ByVal fileName As String) As String
            Try
                Dim defaultUserProfileImage = ConfigManager.DefaultUserProfileImage()
                Dim folderPath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/")
                Dim filePath = folderPath + fileName
                If (File.Exists(filePath)) Then
                    Return fileName
                End If

                Return defaultUserProfileImage
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        Public Function GetUserByUserName(ByVal userName As String) As User
            Try
                Dim user As User = New User()
                Dim userManager As UserManager = New UserManager()
                Return userManager.GetUserByUserName(userName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        Private Function createToken(ByVal username As String, ByVal rememberMe As Boolean) As String
            Try
                Dim tokenExpiryTime = ConfigManager.GetTokenExpiryTime()
                Dim rememberMeTime = ConfigManager.GetRememberMeTime()
                'Dim issuer = ConfigManager.GetIssuer()
                'Dim audience = ConfigManager.GetAudience()
                Dim issuedAt As DateTime = DateTime.UtcNow
                Dim expires As DateTime = DateTime.UtcNow.AddMinutes(tokenExpiryTime)
                If (rememberMe.Equals(True)) Then
                    expires = DateTime.UtcNow.AddDays(rememberMeTime)
                    'expires = DateTime.UtcNow.AddMinutes(2)
                End If
                Dim tokenHandler = New JwtSecurityTokenHandler()
                Dim claimsIdentity As ClaimsIdentity = New ClaimsIdentity({New Claim(ClaimTypes.Name, username)})

                Const sec As String = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1"
                Dim now = DateTime.UtcNow
                Dim securityKey = New Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.[Default].GetBytes(sec))
                Dim signingCredentials = New Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature)
                Dim token = CType(tokenHandler.CreateJwtSecurityToken(issuer:="http://localhost:64403", audience:="http://localhost:64403", subject:=claimsIdentity, notBefore:=issuedAt, expires:=expires, signingCredentials:=signingCredentials), JwtSecurityToken)
                Dim tokenString = tokenHandler.WriteToken(token)
                Return tokenString
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        Public Shared Function ValidateToken(ByVal token As String) As String
            Try
                Dim username As String = Nothing
                Dim principal As ClaimsPrincipal = GetPrincipal(token)
                If principal Is Nothing Then Return Nothing
                Dim identity As ClaimsIdentity = Nothing

                Try
                    identity = CType(principal.Identity, ClaimsIdentity)
                Catch __unusedNullReferenceException1__ As NullReferenceException
                    Return Nothing
                End Try

                Dim usernameClaim As Claim = identity.FindFirst(ClaimTypes.Name)
                username = usernameClaim.Value
                Return username
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        Public Shared Function GetPrincipal(ByVal token As String) As ClaimsPrincipal
            Try
                Dim Secret As String = "ERMN05OPLoDvbTTa/QkqLNMI7cPLguaRyHzyg7n5qNBVjQmtBhz4SzYh4NBVCXi3KJHlSXKP+oi2+bXr6CUYTR=="
                Dim tokenHandler As JwtSecurityTokenHandler = New JwtSecurityTokenHandler()
                Dim jwtToken As JwtSecurityToken = CType(tokenHandler.ReadToken(token), JwtSecurityToken)
                If jwtToken Is Nothing Then Return Nothing
                Dim key As Byte() = Convert.FromBase64String(Secret)
                Dim parameters As TokenValidationParameters = New TokenValidationParameters() With {
                    .RequireExpirationTime = True,
                    .ValidateIssuer = False,
                    .ValidateAudience = False,
                    .IssuerSigningKey = New Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(key)
                }
                Dim securityToken As Microsoft.IdentityModel.Tokens.SecurityToken
                Dim principal As ClaimsPrincipal = tokenHandler.ValidateToken(token, parameters, securityToken)
                Return principal
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        Public Shared Function Decryption(ByVal Data As Byte(), ByVal DoOAEPPadding As Boolean) As Byte()
            Try
                Dim cryptoPrivateKey As String = ConfigManager.cryptoPrivateKey()
                Dim decryptedData As Byte()
                Dim RSAKey As RSAParameters

                Dim RSANew As RSACryptoServiceProvider = New RSACryptoServiceProvider()
                ' RSANew.FromXmlString("<RSAKeyValue><Modulus>sisuTlXP9T7TKdOKDh281SvMHJ7LP16cUFRHvMlZ7Q9R70MfwHSvsw3GgLYOL11LfVt1GkyWz5XFoOZzc/lYvfv/DK/vfCErailqUBzPsbcHG7kvVQsjg9MIGWzFgE/TCDlGRDAWhYsgrAoxY8YxA+cspX4CaM1D2TH78kPE3Fk=</Modulus><Exponent>AQAB</Exponent><P>6zfkuuZCLqb/bMUUsm+mdG7xiwLM98cdh/qqF57yzCcQq/BAVr4+XADjQBNSVmjj71GlREB/xCmQMMkXPrp+Rw==</P><Q>wej0BUz9y1hi3dYnVgaqeJcgcuvSFg9mGGxEdnXpCiOMZU+7hTOHasfQk5XRjDF8TBQkVZAmnSNFGSWSlvAAXw==</Q><DP>tU2Q1Wr0Kmd4TAugx2T95ZM6RQ70lCv7HDve7XQL68ZUuGLoBLSA9oOMpm/+MDKLNoU1IOWiVb4/sQrfCu/Osw==</DP><DQ>Hf2BgDBgMEsDP3wXqV5ujygQhLWkUHUhPTXBgPDLkh3dYO3r+rX1g7ZTs/+/4QbmSn3zAKjC0BrcXP9KpL2J0Q==</DQ><InverseQ>lFnmwOXxnUf4g9v8fnmi4ZQ780UdQSgjfe1wc7RYiYMkhpJCgpzLAUofMNpowix+tZAADEfrrBjiVEowXatlHw==</InverseQ><D>N9e66siQpqBrVPe9lJETh4jzr6DBuXnw6miQF4bQjbG4j5JtWEf6bdeKePsW1Rebuo241WZd+nBKAhVY6GelvVfG42ZnjD8G2Oyn4PZD0UdfLeqn7KKYyNAVnbcwjEyfSl7AIVlmL0jd/akx6TxvEZucd309b1+hi3Lm/uWrk+U=</D></RSAKeyValue>")
                RSANew.FromXmlString(cryptoPrivateKey)
                decryptedData = RSANew.Decrypt(Data, DoOAEPPadding)

                Return decryptedData
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
    End Class
End Namespace