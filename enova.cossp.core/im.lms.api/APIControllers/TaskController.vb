﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib
Imports Newtonsoft.Json

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class TaskController
        Inherits ApiController
        '<HttpGet>
        'Public Function GetListOfTask() As List(Of Task)
        '    Try
        '        Dim task As List(Of Task) = New List(Of Task)()
        '        Dim taskManager As TaskManager = New TaskManager()
        '        Return taskManager.GetTask()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function

        '<HttpPost>
        'Public Function SaveTask(ByVal task As Task)
        '    Try
        '        Dim taskManager As TaskManager = New TaskManager()
        '        taskManager.SaveTask(task)
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function

        <HttpGet>
        Public Function GetListOfContract() As List(Of Contract)
            Try
                'Dim contract As List(Of Contract) = New List(Of Contract)()
                'Dim contractManager As ContractManger = New ContractManger()
                'Return contractManager.GetListOfContract()
                Dim userManager As UserManager = New UserManager()
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userId = userManager.GetUserByUserName(userName).ID
                Dim result = userManager.GetListOfContractByUser(userId)
                'Dim item = New Contract()
                'item.Name = "All"
                'result.Insert(0, item)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfTaskType() As List(Of TaskType)
            Try
                Dim taskManager As TaskManager = New TaskManager()
                Dim result = taskManager.GetListOfTaskType()
                Dim item = New TaskType()
                item.Value = ""
                item.Name = "All"
                result.Insert(0, item)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfTaskStatus() As List(Of TaskStatus)
            Try
                Dim taskManager As TaskManager = New TaskManager()
                Dim result = taskManager.GetListOfTaskStatus()
                Dim item = New TaskStatus()
                item.Value = ""
                item.Name = "All"
                result.Insert(0, item)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        '<HttpGet>
        'Public Function GetListOfBuilding() As List(Of Building)
        '    Dim building As List(Of Building) = New List(Of Building)()
        '    Dim buildingManager As BuildingManager = New BuildingManager()
        '    Return buildingManager.GetListOfBuilding()
        'End Function

        <HttpPost>
        Public Function GetListOfBuildingByUser(ByVal contracts As List(Of Contract)) As List(Of Building)
            Try

                Dim builder As New System.Text.StringBuilder
                
                Dim contractID As String
                For Each contract In contracts
                    contractID = contract.ID
                    contractID = builder.Append("," + contractID).ToString()
                Next

                
                Dim user As User = New User()
                Dim userManager As UserManager = New UserManager()
                Dim userName = HttpContext.Current.User.Identity.Name
                user = userManager.GetUserByUserName(userName)
                'user.building = userManager.GetListOfBuildingByUser(user.ID, user.IsAllBuilding)
                'Dim buildingManager As BuildingManager = New BuildingManager()
                Dim taskManager As TaskManager = New TaskManager()
                Dim result = taskManager.GetListOfBuildingByUser(user.ID, user.IsAllBuilding, contractID)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Function GetListOfBuildingByContract(ByVal contract As List(Of Contract)) As List(Of Building)
            Try
                Dim user As User = New User()
                Dim userManager As UserManager = New UserManager()
                Dim userName = HttpContext.Current.User.Identity.Name
                user = userManager.GetUserByUserName(userName)
                'user.building = userManager.GetListOfBuildingByUser(user.ID, user.IsAllBuilding)
                Dim buildingManager As BuildingManager = New BuildingManager()
                Dim result = buildingManager.GetListOfBuildingByContract(contract)
                'Dim item = New Building()
                'item.Name = "All"
                'result.Insert(0, item)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        '<HttpGet>
        'Public Function GetListOfLocationByBuilding(ByVal buildingId As String) As List(Of Location)
        '    Try
        '        Dim location As List(Of Location) = New List(Of Location)()
        '        Dim locationManager As LocationManager = New LocationManager()
        '        Return locationManager.GetListOfLocationByBuilding(buildingId)
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '        Return Nothing
        '    End Try
        'End Function

        <HttpGet>
        Public Function GetListOfProblem() As List(Of Problem)
            Try
                Dim problem As List(Of Problem) = New List(Of Problem)()
                Dim problemManager As ProblemManager = New ProblemManager()
                Return problemManager.GetListOfProblem()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <Route(“api/Task/GetListOfProblemByContractId/{contractId}”)>
        <HttpGet>
        Public Function GetListOfProblemByContractId(ByVal contractId As String) As List(Of Problem)
            Try
                Dim problemManager As ProblemManager = New ProblemManager()
                Return problemManager.GetProblemByContract(contractId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <Route(“api/Task/GetLocationByContractId/{contractId}”)>
        <HttpGet>
        Public Function GetLocationByContractId(ByVal contractId As Integer) As List(Of Location)
            Try
                Dim locationManager As LocationManager = New LocationManager()
                Return locationManager.GetLocationByContract(contractId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function
        <Route(“api/Task/GetLocationByBuildingId/{buildingId}”)>
        <HttpGet>
        Public Function GetLocationByBuildingId(ByVal buildingId As Integer) As List(Of Location)
            Try
                Dim locationManager As LocationManager = New LocationManager()
                Return locationManager.GetLocationByBuilding(buildingId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function


#Region "Task Cafm Services"

        <HttpPost>
        Public Function Authenticate()
            Try
                Dim obj As Cafm.SecurityService.SecurityServiceClient = New Cafm.SecurityService.SecurityServiceClient()
                Dim loginName = ConfigManager.GetCafmSeviceLoginName()
                Dim password = ConfigManager.GetCafmSevicePassword()
                Dim result = obj.Authenticate(loginName, password)
                Return result.SessionId
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        'Public Function SaveTask(ByVal task As Task)
        <HttpPost>
        Public Function SaveTask(ByVal task As Task)
            Dim sessonId As String
            Try
                Dim accountId = ConfigManager.GetAccountId()
                Dim bdetCallerSourceId = ConfigManager.GetBDETCallerSourceId()
                Dim instructionId = ConfigManager.GetInstructionId()
                Dim timeZoneId = ConfigManager.GetTimeZoneId()

                sessonId = Authenticate()
                Dim taskServiceV3Client As Cafm.TaskServiceV3.TaskServiceV3Client = New Cafm.TaskServiceV3.TaskServiceV3Client()
                Dim breakdownTaskDtoV3 As Cafm.TaskServiceV3.BreakdownTaskDtoV3 = New Cafm.TaskServiceV3.BreakdownTaskDtoV3()
                breakdownTaskDtoV3.AccountId = accountId
                breakdownTaskDtoV3.AssetId = task.AssetId
                breakdownTaskDtoV3.BuildingId = task.BuildingId
                breakdownTaskDtoV3.Category = task.Category
                breakdownTaskDtoV3.ContractId = task.ContractId
                breakdownTaskDtoV3.CreatedDateUtc = DateTime.UtcNow
                breakdownTaskDtoV3.Discipline = task.Discipline
                breakdownTaskDtoV3.InstructionId = instructionId 'task.InstructionId
                breakdownTaskDtoV3.LocationId = task.LocationId
                breakdownTaskDtoV3.LongDescription = task.LongDescription
                breakdownTaskDtoV3.Priority = task.Priority
                breakdownTaskDtoV3.RaisedDateUtc = DateTime.UtcNow
                breakdownTaskDtoV3.ShortDescription = task.ShortDescription
                breakdownTaskDtoV3.BDET_CALLER_SOURCE_ID = bdetCallerSourceId 'task.CallerSource
              
                Dim userDetail = UserManager.GetUserDetails()
                breakdownTaskDtoV3.Phone = userDetail.phoneNo
                breakdownTaskDtoV3.ReporterName = userDetail.FirstName + " " + userDetail.LastName
                'breakdownTaskDtoV3.ReporterId = "4152"
                breakdownTaskDtoV3.TimeZoneId = task.TimeZoneId
                Dim result = taskServiceV3Client.CreateBreakdownTask(sessonId, Nothing, breakdownTaskDtoV3)
                If (result IsNot Nothing) Then

                    Dim userDetails As User = UserManager.GetUserDetails()
                    Dim cafmManager As CafmManager = New CafmManager()
                    cafmManager.UpdateEmailForTask(result.TaskId, userDetails.email)
                    '  cafmManager.SaveEmailForTask(result.TaskId, userDetails.email)

                    If Not (String.IsNullOrEmpty(task.TaskImageBase64)) Then
                        cafmManager.SaveTaskPicture(result.TaskId, task.TaskImageBase64, task.FileName, task.FileType)
                    End If

                    Dim eventLog As EventLog = New EventLog
                    eventLog.Comments = "Task created"
                    eventLog.ID = userDetail.ID
                    eventLog.EventType = "Task created"
                    eventLog.LoggedBy = userDetail.ID
                    Dim EventLogManager As EventLogManager = New EventLogManager()
                    EventLogManager.SaveEventLog(eventLog)
                End If
                Logout(sessonId)
                Return result

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Logout(sessonId)
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Function GetListOfTask(ByVal params As TaskListCafmParameters)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetListOfTask(params)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <Route(“api/Task/GetTaskPictures/{taskSeq}”)>
        <HttpGet>
        Public Function GetTaskPictures(ByVal taskSeq As String)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetTaskPictures(taskSeq)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        '<HttpGet>
        'Public Function GetListOfContract()
        '    Try
        '        Dim cafmManager As CafmManager = New CafmManager()
        '        Return cafmManager.GetContractList()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '        Return Nothing
        '    End Try
        'End Function

        <Route(“api/Task/GetTaskDetail/{taskId}”)>
        <HttpGet>
        Public Function GetTaskDetail(ByVal taskId As String)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetTaskDetail(taskId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <Route(“api/Task/GetListOfBuildingByContractId/{contractId}”)>
        <HttpGet>
        Public Function GetListOfBuildingByContractId(ByVal contractId As String)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetBuildingListByContractId(contractId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <Route(“api/Task/GetListOfLocationByBuildingId/{buildingId}”)>
        <HttpGet>
        Public Function GetListOfLocationByBuildingId(ByVal buildingId As String)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetListOfLocationByBuildingId(buildingId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        '<HttpGet>
        'Public Function GetListOfProblem()
        '    Try
        '        Dim cafmManager As CafmManager = New CafmManager()
        '        Return cafmManager.GetListOfProblem()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '        Return Nothing
        '    End Try
        'End Function

        <HttpPost>
        Public Function GetListOfEvents(ByVal paging As PagingModelCafm)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetListOfEvents(paging)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Function GetListOfStocks(ByVal paging As PagingModelCafm)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Return cafmManager.GetListOfStock(paging)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function
        <HttpPost>
        Public Function Logout(ByVal sessionId As String)
            Try
                Dim obj As Cafm.SecurityService.SecurityServiceClient = New Cafm.SecurityService.SecurityServiceClient()
                obj.LogOut(sessionId)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        '<Route(“api/Task/GetListOfProblemByContractId/{contractId}”)>
        '<HttpGet>
        'Public Function GetListOfProblemByContractId(ByVal contractId As String)
        '    Try
        '        Dim cafmManager As CafmManager = New CafmManager()
        '        Return cafmManager.GetProblemByContractId(contractId)
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '        Return Nothing
        '    End Try
        'End Function
#End Region

    End Class
End Namespace