﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class UserProfileController
        Inherits ApiController

        <HttpGet>
        Public Function GetUserProfileDetails() As UserInfo
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userprofileManager As UserProfileManager = New UserProfileManager()
                Return userprofileManager.GetUserProfileDetails(userName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        '<HttpGet>
        'Public Function GetListOfJobTitle() As List(Of JobTitle)
        '    Try
        '        Dim jobTitle As List(Of JobTitle) = New List(Of JobTitle)()
        '        Dim jobTitleManager As JobTitleManager = New JobTitleManager()
        '        Return jobTitleManager.GetListOfJobTitle()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function
        <HttpPost>
        Public Sub SaveUserProfileData(ByVal userInfo As UserInfo)
            Try
                Dim userName As String = HttpContext.Current.User.Identity.Name
                userInfo.ID = GetUserId()
                Dim userprofileManager As UserProfileManager = New UserProfileManager()
                userprofileManager.SaveUserProfileData(userInfo)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub

        <HttpGet>
        Public Function GetNotificationSetting() As List(Of NotificationSetting)
            Try
                Dim userprofileManager As UserProfileManager = New UserProfileManager()
                Return userprofileManager.GetListOfNotificationSetting()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetNotificationPreferences() As List(Of NotificationPreference)
            Try
                Dim userprofileManager As UserProfileManager = New UserProfileManager()
                Return userprofileManager.GetListOfNotificationPreferences()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpPost>
        Public Sub SaveNotificationData(ByVal notificationSetting As List(Of NotificationSetting))
            Try
                Dim userName As String = HttpContext.Current.User.Identity.Name
                Dim setting As NotificationSetting = New NotificationSetting()
                setting.UserID = GetUserId()
                Dim userprofileManager As UserProfileManager = New UserProfileManager()
                userprofileManager.SaveNotificationData(notificationSetting, setting.UserID)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Sub
        Private Shared Function GetUserId() As Integer
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim userDetails = userManager.GetUserByUserName(userName)
                Return userDetails.ID
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpGet>
        Public Function GetUserProfileNotificationByUserID() As List(Of NotificationSetting)
            Try
                Dim userName As String = HttpContext.Current.User.Identity.Name
                Dim setting As NotificationSetting = New NotificationSetting()
                setting.UserID = GetUserId()
                Dim userprofileManager As UserProfileManager = New UserProfileManager()
                Return userprofileManager.GetUserProfileNotificationByUserID(setting.UserID)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <Route(“api/UserProfile/ValidatedUserEmail/{email}”)>
        <HttpGet>
        Public Function ValidatedUserEmail(ByVal email As String) As Integer
            Try
                Dim userManager As UserManager = New UserManager()
                Return userManager.ValidatedUserEmail(email)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpPost>
        Public Function UploadUserPhoto() As String
            Try
                Dim folderName As String = ConfigManager.UserPhotoFolder
                Dim fileExt As String = String.Empty
                Dim files = HttpContext.Current.Request.Files
                Dim fileName As String = String.Empty
                Dim folderPath As String = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic")
                If HttpContext.Current.Request.Files.AllKeys.Any() Then
                    If (Not System.IO.Directory.Exists(folderPath)) Then
                        System.IO.Directory.CreateDirectory(folderPath)
                    End If
                    Dim httpPostedFile = HttpContext.Current.Request.Files(0)
                    Dim fileExtentsion = Path.GetExtension(httpPostedFile.FileName)
                    fileName = UserManager.GenerateFileName() + fileExtentsion
                    If httpPostedFile IsNot Nothing Then
                        Dim fileSavePath = Path.Combine(folderPath, fileName)
                        httpPostedFile.SaveAs(fileSavePath)
                    End If
                End If

                Return fileName
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <Route(“api/UserProfile/UpdateIsNotificationClosed/{userName}”)>
        <HttpGet>
        Public Sub UpdateIsNotificationClosed(ByVal userName As String)
            Try
                Dim userProfileManager As UserProfileManager = New UserProfileManager()
                userProfileManager.UpdateIsNotificationClosed(userName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub
    End Class
End Namespace