﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory
Imports System.Net.Http
Imports System.Net.Http.Headers

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class UserProfileNotificationController
        Inherits ApiController
        <HttpGet>
        Public Function GetListOfAllUserProfileNotification() As List(Of UserProfileNotification)
            Try
                Dim notificationSetting As List(Of UserProfileNotification) = New List(Of UserProfileNotification)()
                Dim userProfileNotificationManager As UserProfileNotificationManager = New UserProfileNotificationManager()
                notificationSetting = userProfileNotificationManager.GetListOfAllUserProfileNotification()
                Return notificationSetting
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

    End Class
End Namespace