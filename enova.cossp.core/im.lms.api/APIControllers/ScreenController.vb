﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Namespace APIControllers
    Public Class ScreenController
        Inherits ApiController

        Public Function GetListOfAllowedControllers(ByVal userName As String, ByVal ControllerName As String) As List(Of AllowedControllers)
            Try
                Dim allowedControllers As List(Of AllowedControllers) = New List(Of AllowedControllers)()
                Dim screenManager As ScreenManager = New ScreenManager()
                Return screenManager.GetListOfAllowedControllers(userName, ControllerName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

    End Class
End Namespace