﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ContractDocumentController
        Inherits ApiController
        <HttpGet>
        Public Function GetListOfContract() As List(Of Contract)
            Try

                Dim contractManager As ContractManger = New ContractManger()
                Dim contract As List(Of Contract) = New List(Of Contract)()
                Return contractManager.GetListOfContract()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function


    End Class
End Namespace
















