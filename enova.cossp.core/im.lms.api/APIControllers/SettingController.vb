﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class SettingController
        Inherits ApiController

        <HttpGet>
        Public Function GetListOfSetting() As List(Of Setting)
            Try
                Dim setting As List(Of Setting) = New List(Of Setting)()
                Dim settingManager As SettingManager = New SettingManager()
                Return settingManager.GetListOfSetting()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpPut>
        Public Sub UpdateSetting(ByVal userResponse As Setting)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                userResponse.ModifiedBy = user.ID
                Dim settingManager As SettingManager = New SettingManager()
                settingManager.UpdateSetting(userResponse)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Sub
        <HttpGet>
        Public Function GetSettingByID(ByVal id As Integer) As Setting
            Try

                Dim settingManager As SettingManager = New SettingManager()
                Return settingManager.GetSettingByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function


    End Class
End Namespace

