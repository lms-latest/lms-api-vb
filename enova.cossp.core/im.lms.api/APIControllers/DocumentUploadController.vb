﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory
Imports System.Net.Http
Imports System.Net.Http.Headers

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class DocumentUploadController
        Inherits ApiController
        <HttpPost>
        Public Sub PostInvoice(invoice As Invoice)
            Try
                Dim documentManager As DocumentManager = New DocumentManager()
                documentManager.PostInvoice(invoice)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Sub
    End Class
End Namespace