﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory
Imports System.Net.Http
Imports System.Net.Http.Headers

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class SendDocumentController
        Inherits ApiController

        <HttpGet>
        Public Function GetListOfContractByUser() As List(Of Contract)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                Dim contract As List(Of Contract) = New List(Of Contract)()
                Return userManager.GetListOfContractByUser(user.ID)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        '<Route(“api/SendDocument/DownloadDocument/{id}/{Name}”)>
        <HttpPost>
        Public Function DownloadDocument(ByVal docDetails As DocumentInfo) As HttpResponseMessage
            Try
                Dim documentManager As DocumentManager = New DocumentManager()
                Dim check As Integer = New Integer()
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                check = documentManager.CheckUserAccessSentDocument(user.ID, docDetails.ID, docDetails.name, 0)

                If (check = 1) Then
                    Dim path = ConfigManager.DocumentFolderPath() + docDetails.ID.ToString + "-" + docDetails.name

                    Dim result As New HttpResponseMessage(HttpStatusCode.OK)

                    Dim stream = New FileStream(path, FileMode.Open)


                    result.Content = New StreamContent(stream)
                    result.Content.Headers.ContentType = New MediaTypeHeaderValue("application/octet-stream")

                    Return result
                Else
                    Dim result = New HttpResponseMessage("UnAuthorised")
                    Return result
                End If




            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpPost>
        Public Function UploadDocument() As String
            Try
                'Dim folderName As String = ConfigManager.UserPhotoFolder
                Dim fileExt As String = String.Empty
                Dim files = HttpContext.Current.Request.Files
                Dim fileName As String = String.Empty
                If HttpContext.Current.Request.Files.AllKeys.Any() Then
                    Dim httpPostedFile = HttpContext.Current.Request.Files(0)
                    fileName = UserManager.GenerateFileName()
                    If httpPostedFile IsNot Nothing Then
                        Dim fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/Documents"), fileName)
                        httpPostedFile.SaveAs(fileSavePath)
                    End If
                End If

                Return fileName
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfDocumentRecipients() As List(Of DocumentRecipient)
            Try
                Dim documentManager As DocumentManager = New DocumentManager()
                Dim documentRecipients As List(Of DocumentRecipient) = New List(Of DocumentRecipient)()
                Return documentManager.GetListOfDocumentRecipients()

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpPost>
        Public Sub SaveDocument(ByVal document As Document)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                document.sentBy = user.ID

                Dim documentManager As DocumentManager = New DocumentManager()
                documentManager.SaveDocument(document)


            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try


        End Sub

        <HttpPost>
        Public Function GetUserSentDocuments(ByVal filters As ContractFilter) As PaggedResult(Of DocumentInfo)
            Try

                Dim documentManager As DocumentManager = New DocumentManager()
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                Return documentManager.GetUserSentDocuments(user.ID, filters, 0)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

    End Class
End Namespace
