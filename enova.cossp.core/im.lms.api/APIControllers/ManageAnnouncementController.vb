﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ManageAnnouncementController
        Inherits ApiController
        <HttpPost>
        Public Sub SaveAnnouncement(ByVal announcement As Announcement)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                announcement.CreatedBy = user.ID
                Dim announcementManager As AnnouncementManager = New AnnouncementManager()
                announcementManager.SaveAnnouncement(announcement)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try


        End Sub
        <HttpGet>
        Public Function GetListOfRole() As List(Of Role)
            Try
                Dim role As List(Of Role) = New List(Of Role)()
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.GetListOfRole()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpPost>
        Public Function GetListOfAnnouncement(ByVal filters As Pagging) As PaggedResult(Of Announcement)
            Try
                Dim announcement As List(Of Announcement) = New List(Of Announcement)()
                Dim announcementManager As AnnouncementManager = New AnnouncementManager()
                Return announcementManager.GetListOfAnnouncement(filters)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpGet>
        Public Function GetAnnouncementByID(ByVal id As Integer) As Announcement
            Try
                Dim announcement As List(Of Announcement) = New List(Of Announcement)()
                Dim announcementManager As AnnouncementManager = New AnnouncementManager()
                Return announcementManager.GetAnnouncementByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpDelete>
        Public Function DeleteAnnouncementByID(ByVal id As Integer) As Announcement
            Try
                Dim announcement As List(Of Announcement) = New List(Of Announcement)()
                Dim announcementManager As AnnouncementManager = New AnnouncementManager()
                Return announcementManager.DeleteAnnouncementByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        '<HttpGet>
        'Public Function GetAnnouncementResultReportByAnnouncementID(ByVal id As Integer) As List(Of PollResultReport)
        '    Try
        '        Dim pollReport As List(Of PollResultReport) = New List(Of PollResultReport)()
        '        Dim pollManager As PollManager = New PollManager()
        '        Return pollManager.GetPollResultReportByPollID(id)
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function
        '<HttpGet>
        'Public Function CheckAnyResponseOnAnnouncement(ByVal id As Integer) As Integer
        '    Try
        '        Dim pollReport As List(Of PollResultReport) = New List(Of PollResultReport)()
        '        Dim pollManager As PollManager = New PollManager()

        '        Return pollManager.CheckUserResponseOnPoll(Nothing, id)
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try

        'End Function
        <HttpPost>
        Public Function UploadAnnoucementPic() As String
            Try
                ' Dim folderName As String = ConfigManager.UserPhotoFolder
                Dim fileExt As String = String.Empty
                Dim files = HttpContext.Current.Request.Files
                Dim fileName As String = String.Empty
                ' Dim folderPath As String = HttpContext.Current.Server.MapPath("~/Uploads/AnnoucementPic")
                If HttpContext.Current.Request.Files.AllKeys.Any() Then
                    'If (Not System.IO.Directory.Exists(folderPath)) Then
                    '    System.IO.Directory.CreateDirectory(folderPath)
                    'End If
                    Dim httpPostedFile = HttpContext.Current.Request.Files(0)
                    Dim fileExtentsion = Path.GetExtension(httpPostedFile.FileName)
                    fileName = UserManager.GenerateFileName() + fileExtentsion
                    'Dim fileExtentsion = Path.GetExtension(httpPostedFile.FileName)
                    'fileName = UserManager.GenerateFileName() + fileExtentsion
                    If httpPostedFile IsNot Nothing Then
                        Dim fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/AnnoucementPic"), fileName)
                        httpPostedFile.SaveAs(fileSavePath)
                    End If
                End If

                Return fileName
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function
    End Class
End Namespace