﻿Imports System.IO
Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class MessageController
        Inherits ApiController

        <HttpPost>
        Public Sub SaveMessage(ByVal message As Message)
            Try
                Dim messageManager As MessageManager = New MessageManager()
                messageManager.SaveMessage(message)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub
        <HttpGet>
        Public Function GetUserByUserName() As MessageUserInfo
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim messageManager As MessageManager = New MessageManager()
                Return messageManager.GetUserByUserName(userName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfMessagesByID(ByVal id As Integer) As List(Of Message)
            Try
                Dim messages As List(Of Message) = New List(Of Message)()
                Dim messageManager As MessageManager = New MessageManager()
                Return messageManager.GetListOfMessagesByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        '<HttpGet>
        'Public Function GetUserPhotoUrl() As String
        '    Try
        '        Dim request As HttpRequest = HttpContext.Current.Request
        '        Dim fileName As String = "avatar.png"
        '        Dim filePath As String = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic/" & fileName)

        '        If File.Exists(filePath) Then
        '            Dim appUrl As String = String.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Host, If(request.Url.Port = 80, String.Empty, String.Format(":{0}", request.Url.Port)))
        '            Dim downloadFilePath As String = appUrl & "/Uploads/ProfilePic/" & fileName
        '            Return downloadFilePath
        '        Else

        '        End If
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function

    End Class
End Namespace