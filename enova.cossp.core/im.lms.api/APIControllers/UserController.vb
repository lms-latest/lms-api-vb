﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class UserController
        Inherits ApiController
        <HttpPost>
        Public Sub SaveUser(ByVal user As User)
            Try
                Dim userManager As UserManager = New UserManager()
                'user.password = EncodePasswordToBase64(user.password)
                user.password = HashPassword(user.password)
                userManager.SaveUser(user)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Sub

        '<HttpGet>
        'Public Function GetListOfUser() As List(Of User)
        '    Try
        '        Dim user As List(Of User) = New List(Of User)()
        '        Dim userManager As UserManager = New UserManager()
        '        Return userManager.GetListOfUser()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function

        <HttpPost>
        Public Function GetListOfUser(ByVal pagging As Pagging) As PaggedResult(Of User)
            Try
                'Dim user As User = New User()
                Dim userManager As UserManager = New UserManager()
                Dim result As PaggedResult(Of User) = userManager.GetListOfUser(pagging)
                Return result
                'Dim user As List(Of User) = New List(Of User)()
                'Dim userManager As UserManager = New UserManager()
                'Return userManager.GetListOfUser()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GeneratePassword() As String
            Try
                Dim userManager As UserManager = New UserManager()
                Return UserManager.GeneratePassword(10, 1)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        'Public Function GetUserByUserName(ByVal userName As UserName) As Users
        '    Dim user As Users = New Users()
        '    Dim userManager As UserManager = New UserManager()
        '    Return userManager.GetUserByUserName(userName)
        'End Function

        <HttpGet>
        Public Function GetUserByUserName() As User
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Return userManager.GetUserByUserName(userName)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfContract() As List(Of Contract)
            Try
                Dim contract As List(Of Contract) = New List(Of Contract)()
                Dim contractManager As ContractManger = New ContractManger()
                Return contractManager.GetListOfContract()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function GetListOfBuilding() As List(Of Building)
            Try
                Dim building As List(Of Building) = New List(Of Building)()
                Dim buildingManager As BuildingManager = New BuildingManager()
                Return buildingManager.GetListOfBuilding()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function GetListOfCategory() As List(Of Category)
            Dim category As List(Of Category) = New List(Of Category)()
            Dim categoryManager As CategoryManager = New CategoryManager()
            Return categoryManager.GetListOfCategory()
        End Function
        <HttpPost>
        Public Function GetListOfBuildingByContract(ByVal contract As List(Of Contract)) As List(Of Building)
            Try
                Dim building As List(Of Building) = New List(Of Building)()
                Dim buildingManager As BuildingManager = New BuildingManager()
                Return buildingManager.GetListOfBuildingByContract(contract)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        '<HttpGet>
        'Public Function GetListOfJobTitle() As List(Of JobTitle)
        '    Try
        '        Dim jobTitle As List(Of JobTitle) = New List(Of JobTitle)()
        '        Dim jobTitleManager As JobTitleManager = New JobTitleManager()
        '        Return jobTitleManager.GetListOfJobTitle()
        '    Catch ex As Exception
        '        Log4NetHelper.LogError(Nothing, ex.ToString())
        '    End Try
        'End Function
        <HttpGet>
        Public Function GetListOfRole() As List(Of Role)
            Try
                Dim role As List(Of Role) = New List(Of Role)()
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.GetListOfRole()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetUserByID(ByVal id As Integer) As User
            Try
                Dim user As List(Of User) = New List(Of User)()
                Dim userManager As UserManager = New UserManager()
                Return userManager.GetUserByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpDelete>
        Public Function DeleteUserByID(ByVal id As Integer) As User
            Try
                Dim user As List(Of User) = New List(Of User)()
                Dim userManager As UserManager = New UserManager()
                Return userManager.DeleteUserByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpPost>
        Public Sub EditUser(ByVal user As User)
            Try
                Dim userManager As UserManager = New UserManager()
                userManager.SaveUser(user)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub

        <HttpPost>
        Public Function UploadUserPhoto() As String
            Try
                Dim folderName As String = ConfigManager.UserPhotoFolder
                Dim fileExt As String = String.Empty
                Dim files = HttpContext.Current.Request.Files
                Dim fileName As String = String.Empty
                Dim folderPath As String = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePic")
                If HttpContext.Current.Request.Files.AllKeys.Any() Then
                    If (Not System.IO.Directory.Exists(folderPath)) Then
                        System.IO.Directory.CreateDirectory(folderPath)
                    End If
                    Dim httpPostedFile = HttpContext.Current.Request.Files(0)
                    Dim fileExtentsion = Path.GetExtension(httpPostedFile.FileName)
                    fileName = UserManager.GenerateFileName() + fileExtentsion
                    If httpPostedFile IsNot Nothing Then
                        Dim fileSavePath = Path.Combine(folderPath, fileName)
                        httpPostedFile.SaveAs(fileSavePath)
                    End If
                End If
                'Dim files = If(HttpContext.Current.Request.Files.Count > 0, HttpContext.Current.Request.Files, Nothing)
                'Dim tempDirectory As String = HttpContext.Current.Server.MapPath("~/" & folderName)
                'Try
                '    If files IsNot Nothing AndAlso files.Count > 0 Then
                '        Dim file As HttpPostedFile = HttpContext.Current.Request.Files(0)
                '        fileExt = Path.GetExtension(file.FileName)
                '        If fileExt.IsValidImageType() Then
                '            Dim path As String = path.Combine(tempDirectory, userid & ".png")
                '            If Not IO.Directory.Exists(tempDirectory) Then
                '                IO.Directory.CreateDirectory(tempDirectory)
                '            End If
                '            file.SaveAs(path)
                '            Return "OK"
                '        Else
                '            Return "File Type Not Supported"
                '        End If
                '    Else
                '        Return "File Not Found"
                '    End If
                'Catch ex As Exception
                '    'ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
                '    Return ex.Message
                'End Try
                Return fileName
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Function SendEmailForUserCreation(ByVal user As User) As User
            Try
                Dim details As DetailsForPasswordUpdationEmail = New DetailsForPasswordUpdationEmail()
                Dim userManager As UserManager = New UserManager()
                details.Email = user.email
                details.Name = user.FirstName + " " + user.LastName
                details.UserName = user.userName
                If (user.IsAldapUser) Then
                    details.Password = ""
                    userManager.SendEmailForLDAPUserCreation(details)
                Else
                    details.Password = user.password
                    userManager.SendEmailForUserCreation(details)
                End If

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <Route(“api/User/ValidatedUserEmail/{email}”)>
        <HttpGet>
        Public Function ValidatedUserEmail(ByVal email As String) As Integer
            Try
                Dim userManager As UserManager = New UserManager()
                Return userManager.ValidatedUserEmail(email)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <Route(“api/User/ValidatedUserName/{name}”)>
        <HttpGet>
        Public Function ValidatedUserName(ByVal name As String) As Integer
            Try
                Dim userManager As UserManager = New UserManager()
                Return userManager.ValidatedUserName(name)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

    End Class
End Namespace