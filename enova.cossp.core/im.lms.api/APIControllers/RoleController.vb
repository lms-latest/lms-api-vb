﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class RoleController
        Inherits ApiController
        <HttpGet>
        Public Function GetListOfRole() As List(Of Role)
            Try
                Dim roles As List(Of Role) = New List(Of Role)()
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.GetListOfRole()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpPost>
        Public Sub SaveRole(ByVal role As Role)
            Try
                Dim roleManager As RoleManager = New RoleManager()
                roleManager.SaveRole(role)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub

        <HttpGet>
        Public Function GetListOfScreen() As List(Of Screen)
            Try
                Dim screen As List(Of Screen) = New List(Of Screen)()
                Dim screenManager As ScreenManager = New ScreenManager()
                Return screenManager.GetListOfScreen()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfReport() As List(Of Report)
            Try
                Dim reports As List(Of Report) = New List(Of Report)()
                Dim reportManager As ReportManager = New ReportManager()
                reports = reportManager.GetListOfReport()
                Return reports
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetRoleByID(ByVal id As Integer) As Role
            Try
                Dim role As List(Of Role) = New List(Of Role)()
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.GetRoleByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpDelete>
        Public Function DeleteRoleByID(ByVal id As Integer) As Integer
            Try
                Dim role As List(Of Role) = New List(Of Role)()
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.DeleteRoleByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <Route(“api/Role/ValidatedRoleName/{name}”)>
        <HttpGet>
        Public Function ValidatedRoleName(ByVal name As String) As Integer
            Try
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.ValidatedRoleName(name)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

    End Class
End Namespace