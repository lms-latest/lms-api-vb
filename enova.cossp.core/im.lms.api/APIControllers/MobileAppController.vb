﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class MobileAppController
        Inherits ApiController

        <HttpPost>
        Public Sub SaveMobileApp(ByVal mobileApp As MobileApp)
            Try
                Dim mobileAppManager As MobileAppManager = New MobileAppManager()
                mobileApp.Active = True
                mobileApp.CreatedBy = UserManager.GetUserId()
                mobileApp.ModifiedBy = UserManager.GetUserId()
                mobileAppManager.SaveMobileApp(mobileApp)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub

        <HttpGet>
        Public Function GetMobileApp() As MobileApp
            Try
                Dim mobileAppManager As MobileAppManager = New MobileAppManager()
                Dim mobileApp As MobileApp = New MobileApp()
                mobileApp = mobileAppManager.GetMobileApp()
                mobileApp.MobileAppDocuments = mobileAppManager.GetMobileAppDocument()
                Return mobileApp
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

    End Class
End Namespace