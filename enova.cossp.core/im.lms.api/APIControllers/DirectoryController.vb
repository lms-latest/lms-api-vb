﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class DirectoryController
        Inherits ApiController
        <HttpPost>
        Public Function GetListOfDirectory(ByVal pagging As Pagging) As PaggedResult(Of Directory)
            Try
                Dim directoryManager As DirectoryManger = New DirectoryManger()
                Dim result As PaggedResult(Of Directory) = directoryManager.GetDirectory(pagging)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
    End Class
End Namespace