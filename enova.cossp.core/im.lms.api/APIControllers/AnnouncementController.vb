﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class AnnouncementController
        Inherits ApiController
        <HttpGet>
        Public Function GetAnnouncementByID(ByVal id As Integer) As Announcement
            Try
                Dim announcement As List(Of Announcement) = New List(Of Announcement)()
                Dim announcementManager As AnnouncementManager = New AnnouncementManager()
                Return announcementManager.GetAnnouncementByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
    End Class
End Namespace