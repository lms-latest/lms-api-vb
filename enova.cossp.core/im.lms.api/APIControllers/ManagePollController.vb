﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ManagePollController
        Inherits ApiController
        <HttpPost>
        Public Sub SavePoll(ByVal poll As Poll)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                poll.CreatedBy = user.ID
                Dim pollManager As PollManager = New PollManager()
                pollManager.SavePoll(poll)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try


        End Sub
        <HttpGet>
        Public Function GetListOfRole() As List(Of Role)
            Try
                Dim role As List(Of Role) = New List(Of Role)()
                Dim roleManager As RoleManager = New RoleManager()
                Return roleManager.GetListOfRole()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        '<Route(“api/ManagePoll/GetListOfPoll/{first}/{size}/{sort}/{sortDirection}”)>

        'Public Function GetListOfPoll(ByVal filters As Pagging) As List(Of Poll)
        <HttpPost>
        Public Function GetListOfPoll(ByVal filters As Pagging) As PaggedResult(Of Poll)
            Try
                Dim poll As List(Of Poll) = New List(Of Poll)()
                Dim pollManager As PollManager = New PollManager()
                Return pollManager.GetListOfPoll(filters)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpGet>
        Public Function GetPollByID(ByVal id As Integer) As Poll
            Try
                Dim poll As List(Of Poll) = New List(Of Poll)()
                Dim pollManager As PollManager = New PollManager()
                Return pollManager.GetPollByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpDelete>
        Public Function DeletePollByID(ByVal id As Integer) As Poll
            Try
                Dim poll As List(Of Poll) = New List(Of Poll)()
                Dim pollManager As PollManager = New PollManager()
                Return pollManager.DeletePollByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpGet>
        Public Function GetPollResultReportByPollID(ByVal id As Integer) As List(Of PollResultReport)
            Try
                Dim pollReport As List(Of PollResultReport) = New List(Of PollResultReport)()
                Dim pollManager As PollManager = New PollManager()
                Return pollManager.GetPollResultReportByPollID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function CheckAnyResponseOnPoll(ByVal id As Integer) As Integer
            Try
                Dim pollReport As List(Of PollResultReport) = New List(Of PollResultReport)()
                Dim pollManager As PollManager = New PollManager()

                Return pollManager.CheckUserResponseOnPoll(Nothing, id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function


    End Class
End Namespace