﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ManageReportController
        Inherits ApiController
        <HttpPost>
        Public Function SaveReport(ByVal report As Report) As Boolean
            Try
                If (report.ReportID IsNot Nothing) Then
                    Dim embedUrl = PowerBIHelper.GetPowerBIEmbeddedUrl(report.ReportID).Result

                    If (embedUrl Is Nothing) Then
                        Return False
                    Else
                        report.EmbeddedUrl = embedUrl
                    End If
                End If

                Dim reportManager As ReportManager = New ReportManager()
                reportManager.SaveReport(report)
                Return True
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetListOfReport() As List(Of Report)
            Try
                Dim reports As List(Of Report) = New List(Of Report)()
                Dim reportManager As ReportManager = New ReportManager()
                Return reportManager.GetListOfReport()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function GetTypeOfReport() As List(Of ReportType)
            Try
                Dim reports As List(Of ReportType) = New List(Of ReportType)()
                Dim reportManager As ReportManager = New ReportManager()
                Return reportManager.GetTypeOfReport()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function GetSizeOfWidget() As List(Of WidgetSize)
            Try
                Dim reports As List(Of WidgetSize) = New List(Of WidgetSize)()
                Dim reportManager As ReportManager = New ReportManager()
                Return reportManager.GetSizeOfWidget()
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpGet>
        Public Function GetReportByID(ByVal id As Integer) As Report
            Try
                Dim reportManager As ReportManager = New ReportManager()
                Return reportManager.GetReportByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
        <HttpDelete>
        Public Function DeleteReportByID(ByVal id As Integer) As Report
            Try
                Dim reportManager As ReportManager = New ReportManager()
                Return reportManager.DeleteReportByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        <Route(“api/ManageReport/ValidateReportName/{name}”)>
        <HttpGet>
        Public Function ValidateReportName(ByVal name As String) As Integer
            Try
                Dim reportManager As ReportManager = New ReportManager()
                Return reportManager.ValidateReportName(name)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

    End Class
End Namespace