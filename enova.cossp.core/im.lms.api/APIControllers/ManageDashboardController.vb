﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.PowerBI.Api.V2
Imports Microsoft.PowerBI.Api.V2.Models
Imports Microsoft.Rest

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ManageDashboardController
        Inherits ApiController

        Public manageDashboardManager As ManageDashboardManager

        Public Sub New()
            manageDashboardManager = New ManageDashboardManager()
        End Sub

        <HttpGet>
        Public Function GetManageDashboardWidgets() As List(Of WidgetInfo)

            Try
                Dim widgetList = New List(Of WidgetInfo)
                Dim allWidgetsSizeID = ConfigManager.AllWidgetsSizeID()
                widgetList = manageDashboardManager.GetManageDashboardWidget(allWidgetsSizeID)
                'Dim widgetInfo As WidgetInfo = New WidgetInfo
                'For Each widgetInfo In widgetList
                '    If (widgetInfo.ReportID IsNot Nothing And (widgetInfo.TypeID = 3 Or widgetInfo.TypeID = 2)) Then
                '        Dim embedUrl = PowerBIHelper.GetPowerBIEmbeddedUrl(widgetInfo.ReportID).Result
                '        widgetInfo.EmbeddedUrl = embedUrl

                '    End If

                'Next
                Return widgetList
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetAccessToken() As String

            Try
                Dim token As String = PowerBIHelper.GetPowerBIToken().Result
                Return token
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <Route(“api/ManageDashboard/GetScreenSection/{screenID}”)>
        <HttpGet>
        Public Function GetScreenSection(screenID As String) As List(Of ScreenSection)

            Try
                Dim reportList = manageDashboardManager.GetScreenSection(screenID)
                Return reportList
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpPost>
        Public Function SaveDashboardSections(savedSectionDetails As List(Of ScreenSection)) As Boolean

            Try
                Dim userId = UserManager.GetUserId()
                Dim section As ScreenSection
                For Each section In savedSectionDetails
                    manageDashboardManager.SaveDasboardSection(section, userId)
                Next

                Return True
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetUserContracts() As List(Of Contract)
            Try
                Dim userManager = New UserManager()
                Dim userId = GetUserId()
                Dim contractList = userManager.GetListOfContractByUser(userId)
                Return contractList

            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function

        Private Shared Function GetUserId() As Integer
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim userDetails = userManager.GetUserByUserName(userName)
                Return userDetails.ID
            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function


    End Class
End Namespace


