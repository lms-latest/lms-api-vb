﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class DashboardController
        Inherits ApiController

        Public dashboardManager As DashboardManager

        Public Sub New()
            dashboardManager = New DashboardManager()
        End Sub

        <HttpGet>
        Public Function GetDashboardWidgets() As List(Of WidgetInfo)
            Try
                Dim widgetList = New List(Of WidgetInfo)
                Dim allWidgetsSizeID = ConfigManager.AllWidgetsSizeID()
                Dim userID = GetUserId()
                widgetList = dashboardManager.GetDashboardWidgets(allWidgetsSizeID, userID)
                'Dim widgetInfo As WidgetInfo = New WidgetInfo
                'For Each widgetInfo In widgetList
                '    If (widgetInfo.ReportID IsNot Nothing And (widgetInfo.TypeID = 3 Or widgetInfo.TypeID = 2)) Then
                '        Dim embedUrl = PowerBIHelper.GetPowerBIEmbeddedUrl(widgetInfo.ReportID).Result
                '        widgetInfo.EmbeddedUrl = embedUrl

                '    End If

                'Next
                Return widgetList
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetAccessToken() As String
            Try
                Dim token As String = PowerBIHelper.GetPowerBIToken().Result
                Return token
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <Route(“api/Dashboard/GetUserScreenSection/{screenID}”)>
        <HttpGet>
        Public Function GetUserScreenSection(screenID As String) As List(Of ScreenSection)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userID = GetUserId()
                Dim reportList = dashboardManager.GetUserScreenSection(screenID, userID)
                Return reportList
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpPost>
        Public Function SaveDashboardSection(savedSectionDetails As ScreenSection) As Boolean
            Try
                Dim userScreenSection As UserScreenSection = New UserScreenSection()
                userScreenSection.UserID = GetUserId()
                userScreenSection.ReportID = savedSectionDetails.DefaultReportID
                userScreenSection.ScreenSectionID = savedSectionDetails.ID

                dashboardManager.SaveUserDasboardSection(userScreenSection)

                Return True
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetListOfReport() As List(Of Report)
            Try
                Dim userId = GetUserId()
                Dim screenManager = New ScreenManager()
                Dim reportScreenId = ConfigManager.GetReportScreenId()
                Dim accessCount = screenManager.ValidateScreenAccessForUser(userId, reportScreenId)

                If (accessCount > 0) Then
                    Dim reportManager As ReportManager = New ReportManager()
                    Dim list = reportManager.GetReportByUserID(userId)
                    
                    Return list
                Else
                    Return Nothing
                End If

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetListOfMessage() As List(Of Message)
            Try
                Dim userId = GetUserId()
                Dim screenManager = New ScreenManager()
                Dim messageScreenId = ConfigManager.GetMessageScreenId()
                Dim accessCount = screenManager.ValidateScreenAccessForUser(userId, messageScreenId)

                If (accessCount > 0) Then
                    Dim messages As List(Of Message) = New List(Of Message)()
                    Dim messageManager As MessageManager = New MessageManager()
                    messages = messageManager.GetListOfMessagesByID(userId)
                    Return messages
                Else
                    Return Nothing
                End If

            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpGet>
        Public Function GetHappinessIndex()
            Try
                Dim userId = GetUserId()
                Dim cafmManager As CafmManager = New CafmManager()
                Dim response = cafmManager.GetHappinessIndex(userId)
                Return response
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        Private Shared Function GetUserId() As Integer

            Dim userName = HttpContext.Current.User.Identity.Name
            Dim userManager As UserManager = New UserManager()
            Dim userDetails = userManager.GetUserByUserName(userName)
            Return userDetails.ID

        End Function

        <HttpGet>
        Public Function GetUserContracts() As List(Of Contract)
            Try
                Dim userManager = New UserManager()
                Dim userId = GetUserId()
                Dim contractList = userManager.GetListOfContractByUser(userId)
                Return contractList

            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function

    End Class
End Namespace