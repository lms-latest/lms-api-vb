﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class NotificationController
        Inherits ApiController
        <HttpGet>
        Public Function GetNotificationsByUserID(ByVal id As Integer) As List(Of Notification)
            Try
                Dim notification As List(Of Notification) = New List(Of Notification)()
                Dim notificationManager As NotificationManager = New NotificationManager()
                Return notificationManager.GetNotificationsByUserID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function


    End Class
End Namespace