﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class ReportController
        Inherits ApiController

        Public reportManager As ReportManager

        Public Sub New()
            reportManager = New ReportManager()
        End Sub

        <HttpGet>
        Public Function GetTableAndPowerBIReports() As List(Of ReportSummary)
            Try
                Dim userId = GetUserId()
                Return reportManager.GetTableAndPowerBIReports(userId)
            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function

        '<HttpPost>
        'Public Function GetTableAndPowerBIReports(ByVal pagging As Pagging) As PaggedResult(Of ReportSummary)
        '    Try
        '        Dim userId = GetUserId()
        '        Dim result As PaggedResult(Of ReportSummary) = reportManager.GetTableAndPowerBIReports(userId, pagging)
        '        Return result
        '    Catch Ex As Exception
        '        Log4NetHelper.LogError(Nothing, Ex.ToString())
        '    End Try
        'End Function

        <HttpPost>
        Public Function GetReportTableData(ByVal details As ReportContractInfo) As Object
            Try
                Dim reportId = details.ReportID
                Dim contractsList = details.ContractList
                Dim paggingDetails = details.PaggingDetails
                Dim reportDetails As Report = reportManager.GetReportByID(reportId)
                Return reportManager.GetReportTableData(reportDetails, contractsList, paggingDetails)
            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function


        <HttpGet>
        Public Function GetPowerBIAccessToken() As String
            Try
                Dim token As String = PowerBIHelper.GetPowerBIToken().Result
                Return token
            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function

        <HttpGet>
        Public Function GetUserContracts() As List(Of Contract)
            Try
                Dim userManager = New UserManager()
                Dim userId = GetUserId()
                Dim contractList = userManager.GetListOfContractByUser(userId)
                Return contractList

            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function


        <HttpGet>
        Public Function GetReportByID(ByVal id As Integer) As Report
            Try
                Dim userId = GetUserId()
                Dim reportCount = reportManager.ValidateReportAccessForUser(userId, id)

                If (reportCount > 0) Then
                    Return reportManager.GetReportByID(id)
                Else
                    Return Nothing
                End If

            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function

        Private Shared Function GetUserId() As Integer
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim userDetails = userManager.GetUserByUserName(userName)
                Return userDetails.ID
            Catch Ex As Exception
                Log4NetHelper.LogError(Nothing, Ex.ToString())
            End Try
        End Function

    End Class
End Namespace