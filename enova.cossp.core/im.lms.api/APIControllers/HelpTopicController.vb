﻿Imports System.IO
Imports System.Net
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.Common.Lib
Imports enova.cossp.entities

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class HelpTopicController
        Inherits ApiController

        <HttpPost>
        Public Sub SaveHelpTopic(ByVal helpTopic As HelpTopic)
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                helpTopic.Active = True
                If (helpTopic.ID > 0) Then
                    helpTopic.ModifiedBy = UserManager.GetUserId()
                Else
                    helpTopic.CreatedBy = UserManager.GetUserId()
                End If
                helpTopicManager.SaveHelpTopic(helpTopic)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Sub

        <HttpGet>
        Public Function GetListOfHelpTopicAll() As List(Of HelpTopic)
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                Dim result As List(Of HelpTopic) = helpTopicManager.GetListOfHelpTopicAll()
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Function GetListOfHelpTopic(ByVal pagging As Pagging) As PaggedResult(Of HelpTopic)
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                Dim result As PaggedResult(Of HelpTopic) = helpTopicManager.GetListOfHelpTopic(pagging)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpDelete>
        Public Function DeleteHelpTopicByID(ByVal id As Integer) As HelpTopic
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                Return helpTopicManager.DeleteHelpTopicByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
            Return Nothing
        End Function

        <HttpGet>
        Public Function GetHelpTopicByID(ByVal id As Integer) As HelpTopic
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                Return helpTopicManager.GetHelpTopicByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
            Return Nothing
        End Function

        <Route(“api/HelpTopic/ValidatedHelpTopicTitle/{title}/{id}”)>
        <HttpGet>
        Public Function ValidatedHelpTopicTitle(ByVal title As String, ByVal id As Integer) As Integer
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                Return helpTopicManager.ValidatedHelpTopicTitle(title, id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
            Return Nothing
        End Function


        <Route(“api/HelpTopic/GetHelpTopicDocumentsByHelpTopiccID/{id}”)>
        <HttpGet>
        Public Function GetHelpTopicDocumentsByHelpTopiccID(ByVal id As Integer) As List(Of HelpTopicDocument)
            Try
                Dim helpTopicManager As HelpTopicManager = New HelpTopicManager()
                Return helpTopicManager.GetHelpTopicDocumentsByHelpTopiccID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
            Return Nothing
        End Function

        <HttpPost>
        Public Function UploadHelpTopicDocument() As List(Of HelpTopicDocument)
            Try
                Dim lstHelpTopicDocument As List(Of HelpTopicDocument) = New List(Of HelpTopicDocument)()
                Dim fileExt As String = String.Empty
                Dim files = HttpContext.Current.Request.Files
                Dim fileName As String = String.Empty
                Dim folderPath As String = HttpContext.Current.Server.MapPath("~/Uploads/HelpTopic")

                Dim index As Integer = 0
                If HttpContext.Current.Request.Files.AllKeys.Any() Then
                    If (Not System.IO.Directory.Exists(folderPath)) Then
                        System.IO.Directory.CreateDirectory(folderPath)
                    End If

                    For Each file As String In HttpContext.Current.Request.Files
                        Dim helpTopicDocument As HelpTopicDocument = New HelpTopicDocument()
                        Dim httpPostedFile = HttpContext.Current.Request.Files(index)
                        Dim fileExtentsion = Path.GetExtension(httpPostedFile.FileName)
                        fileName = UserManager.GenerateFileName() + fileExtentsion 'httpPostedFile.FileName '
                        If httpPostedFile IsNot Nothing Then
                            Dim fileSavePath = Path.Combine(folderPath, fileName)
                            httpPostedFile.SaveAs(fileSavePath)
                            helpTopicDocument.HelpTopicId = 0
                            helpTopicDocument.DocumentPath = fileName
                            helpTopicDocument.ActualDocumentPath = httpPostedFile.FileName
                            lstHelpTopicDocument.Add(helpTopicDocument)
                        End If
                        index += 1
                    Next
                End If
                Return lstHelpTopicDocument
            Catch ex As Exception
                Return Nothing
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function


        Public Function DownloadDocument(ByVal docDetails As HelpTopicDocument) As HttpResponseMessage
            Try
                Dim fileSavePath = HttpContext.Current.Server.MapPath("~/Uploads/HelpTopic/")
                Dim path = fileSavePath + docDetails.DocumentPath
                Dim result As New HttpResponseMessage(HttpStatusCode.OK)
                Dim stream = New FileStream(path, FileMode.Open)
                result.Content = New StreamContent(stream)
                result.Content.Headers.ContentType = New MediaTypeHeaderValue("application/octet-stream")
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
    End Class
End Namespace