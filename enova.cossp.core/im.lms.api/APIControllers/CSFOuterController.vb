﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib

Namespace APIControllers
    Public Class CSFOuterController
        Inherits ApiController

        <HttpPost>
        Public Function SaveCSFOuter(ByVal csf As CSF)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim response = cafmManager.SaveCSF(csf)
                Return response
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

        '<Route(“api/CSFOuter/GetCSFOuterTaskDetails/{userId}/{taskId}”)>
        <HttpGet>
        <Route(“api/CSFOuter/GetCSFOuterTaskDetails/{taskSeq}”)>
        Public Function GetCSFOuterTaskDetails(taskSeq As Integer)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim taskDetails = cafmManager.GetTaskDetail(taskSeq)
                Return taskDetails
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpPost>
        Public Function CheckCSFDone(ByVal csf As CSF)
            Try
                Dim cafmManager As CafmManager = New CafmManager()
                Dim response = cafmManager.CheckCSFDone(csf.TaskId)
                Return response
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function

    End Class
End Namespace