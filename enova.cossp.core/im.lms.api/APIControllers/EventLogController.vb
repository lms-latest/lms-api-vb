﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib

Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class EventLogController
        Inherits ApiController
        <HttpPost>
        Public Function GetListOfEventLog(ByVal pagging As Pagging) As PaggedResult(Of EventLog)
            Try
                Dim eventLog As List(Of EventLog) = New List(Of EventLog)()
                Dim eventLogManager As EventLogManager = New EventLogManager()
                Dim result As PaggedResult(Of EventLog) = eventLogManager.GetListOfEventLog(pagging)
                Return result
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Function GetEventTypeCount(ByVal pagging As Pagging) As List(Of EventLog)
            Try
                'Dim eventLog As List(Of EventLog) = New List(Of EventLog)()
                Dim eventLogManager As EventLogManager = New EventLogManager()
                Return eventLogManager.GetEventTypeCount(pagging)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Public Async Function SaveEventLog(ByVal eventLog As EventLog) As Threading.Tasks.Task
            Try
                eventLog.ID = UserManager.GetUserId()
                eventLog.LoggedBy = UserManager.GetUserId()
                ' eventLog.LastLoggedOn = System.DateTime.Now
                Dim EventLogManager As EventLogManager = New EventLogManager()
                Await EventLogManager.SaveEventLog(eventLog)
            Catch ex As Exception
                '   Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try
        End Function
    End Class
End Namespace