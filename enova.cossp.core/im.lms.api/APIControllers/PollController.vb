﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports System.IO
Imports enova.cossp.Common.Lib.CryptoHelper
Imports enova.cossp.Common.Lib
Imports System.IO.Directory




Namespace APIControllers
    <CustomAuthorizeAttribute>
    Public Class PollController
        Inherits ApiController

        <HttpGet>
        Public Function GetPollByID(ByVal id As Integer) As Poll
            Try
                Dim poll As List(Of Poll) = New List(Of Poll)()
                Dim pollManager As PollManager = New PollManager()
                Return pollManager.GetPollByID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpPost>
        Public Sub SavePollResult(ByVal userResponse As PollResult)
            Try
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                Dim pollRes As PollResult = New PollResult
                pollRes.userID = user.ID
                pollRes.optionID = userResponse.optionID
                pollRes.pollID = userResponse.pollID
                Dim pollManager As PollManager = New PollManager()
                pollManager.SavePollResult(pollRes)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try


        End Sub
        <HttpGet>
        Public Function GetPollResultReportByPollID(ByVal id As Integer) As List(Of PollResultReport)
            Try
                Dim pollReport As List(Of PollResultReport) = New List(Of PollResultReport)()
                Dim pollManager As PollManager = New PollManager()
                Return pollManager.GetPollResultReportByPollID(id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function
        <HttpGet>
        Public Function CheckUserResponseOnPoll(ByVal id As Integer) As Integer
            Try
                Dim pollReport As List(Of PollResultReport) = New List(Of PollResultReport)()
                Dim pollManager As PollManager = New PollManager()
                Dim userName = HttpContext.Current.User.Identity.Name
                Dim userManager As UserManager = New UserManager()
                Dim user As User = New User()
                user = userManager.GetUserByUserName(userName)
                Return pollManager.CheckUserResponseOnPoll(user.ID, id)
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function


    End Class
End Namespace
