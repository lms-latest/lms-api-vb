﻿Imports System.Net
Imports System.Web.Http
Imports enova.cossp.bll
Imports enova.cossp.entities
Imports enova.cossp.Common.Lib
Imports enova.cossp.Common.Lib.CryptoHelper

Namespace APIControllers

    Public Class ForgotPasswordController
        Inherits ApiController

        Public forgotPasswordManager As ForgotPasswordManager

        Public Sub New()
            forgotPasswordManager = New ForgotPasswordManager()
        End Sub

        <Route(“api/ForgotPassword/SendResetPasswordLink/{email}”)>
        <HttpGet>
        Public Function SendResetPasswordLink(email As String) As Integer
            Try
                Dim userTokenDetails As UserTokenDetails = New UserTokenDetails()
                Dim token = Guid.NewGuid().ToString()
                userTokenDetails.Token = token
                userTokenDetails.Email = email
                userTokenDetails.TokenExpiresOn = DateTime.Today.AddDays(ConfigManager.TokenExpireTimeInDays)

                Dim response As User = forgotPasswordManager.ValidateEmailAndSaveToken(userTokenDetails)
                ' Or email.Contains("@enova-me.com")
                If (response.IsAldapUser) Then
                    Return -1
                End If

                If IsNothing(response) Then
                    Return 0
                Else
                    Dim url = ConfigManager.WebUrlForEnova() + "/reset-password/" + token
                    Dim name = response.Name
                    forgotPasswordManager.SendResetPasswordLink(email, url, name)
                    Return 1
                End If
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <Route(“api/ForgotPassword/ValidateForgotPasswordToken/{token}”)>
        <HttpGet>
        Public Function ValidateForgotPasswordToken(ByVal token As String) As Integer
            Try
                Dim response = forgotPasswordManager.ValidateForgotPasswordToken(token)

                If (response = 1) Then
                    Return 1
                Else
                    Return 0
                End If
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

        <HttpPost>
        Public Function UpdateUserPassword(ByVal newPasswordDetails As NewPasswordDetails) As Integer
            Try
                'newPasswordDetails.NewPassword = EncodePasswordToBase64(newPasswordDetails.NewPassword)
                newPasswordDetails.NewPassword = HashPassword(newPasswordDetails.NewPassword)
                Dim response As User = forgotPasswordManager.ChangeUserPassword(newPasswordDetails)

                If IsNothing(response) Then
                    Return 0
                Else
                    Dim details As DetailsForPasswordUpdationEmail = New DetailsForPasswordUpdationEmail()
                    Dim email = response.email
                    details.Name = response.Name
                    details.UserName = response.userName
                    details.NewPassword = newPasswordDetails.NewPassword
                    forgotPasswordManager.SendEmailForUpdatedPassordConfirmation(email, details)
                    Return 1
                End If
            Catch ex As Exception
                Log4NetHelper.LogError(Nothing, ex.ToString())
            End Try

        End Function

    End Class
End Namespace