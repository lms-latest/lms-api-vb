﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports log4net
Imports log4net.Config

Public Enum LogType
        Debug = 0
        Info = 1
        Warn = 2
        [Error] = 3
        Fatal = 4
    End Enum

    Public Class Log4NetManager
        Private Shared ReadOnly logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
        End Sub

        Public Shared Sub LogEvent(ByVal message As String, ByVal logLevel As LogType)
            log4net.Config.XmlConfigurator.Configure()

            Select Case logLevel
                Case LogType.Debug
                    logger.Debug(message)
                Case LogType.[Error]
                    logger.[Error](message)
                Case LogType.Fatal
                    logger.Fatal(message)
                Case LogType.Info
                    logger.Info(message)
                Case LogType.Warn
                    logger.Warn(message)
            End Select
        End Sub
    End Class

