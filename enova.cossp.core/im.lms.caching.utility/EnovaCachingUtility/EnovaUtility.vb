﻿Option Strict Off
Imports System.ComponentModel
Imports System.IO
Imports System.Net
Imports System.Reflection
Imports System.Text
Imports System.Threading
Imports System.Web.Script.Serialization
Imports enova.cossp.Common.Lib
Imports enova.cossp.dal
Imports enova.cossp.entities
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Namespace EnovaUtility
    Public Class Program

        Public Shared Iterator Function Chunkify(ByVal table As DataTable, ByVal chunkSize As Integer) As IEnumerable(Of DataTable)
            Dim i As Integer = 0

            While i < table.Rows.Count
                Dim Chunk As DataTable = table.Clone()

                For Each Row As DataRow In table.[Select]().Skip(i).Take(chunkSize)
                    Chunk.ImportRow(Row)
                Next

                Yield Chunk
                i += chunkSize
            End While
        End Function
        'Dim token As String = ""
        Public Shared Property token As String
        Public Property HttpContext As Object

        Public Shared Sub Tick(ByVal stateInfo As Object)
            'Console.WriteLine("Tick: {0}", DateTime.Now.ToString("h:mm:ss"))
            'Public Function GetCafmAuthToken()
            Try
                DisplayAndLog("Getting Authorisation token")
                Dim cafmApiUrl As String = ConfigManager.GetCafmApiURL()
                Dim userName As String = ConfigManager.GetCafmApiUserName()
                Dim password As String = ConfigManager.GetCafmApiPassword()
                Dim url = cafmApiUrl & "Auth/createToken"

                Using client As WebClient = New WebClient()
                    Dim reqparm = New System.Collections.Specialized.NameValueCollection()
                    reqparm.Add("username", userName)
                    reqparm.Add("Password", password)
                    Dim responsebytes As Byte() = client.UploadValues(url, "POST", reqparm)
                    Dim responsebody As String = Encoding.UTF8.GetString(responsebytes)
                    Dim response = (New JavaScriptSerializer()).Deserialize(Of Object)(responsebody)
                    token = response

                    'Return response
                End Using

            Catch ex As Exception

                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString(), LogType.[Error])

            End Try
            'End Function
        End Sub



        Public Sub GetEscalationsList(contractID As Integer, EscalationsTable As DataTable)
            Dim request = ConfigManager.GetEscalationsUrl() + "?conseq=," + contractID.ToString() + ","
            Try
                Dim dbHelperObj As DBHelper = New DBHelper()
                Dim escalationUrl As String = ConfigManager.GetEscalationsUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    client.QueryString.Add("conseq", "," + contractID.ToString + ",")
                    Dim responseString As String = client.DownloadString(escalationUrl)

                    Dim response As Object = JsonConvert.DeserializeObject(responseString)
                    Dim items As JArray = CType(response, JArray)
                    Dim length As Integer = items.Count

                    If length > 0 Then
                        Dim escalationTableByContract As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)
                        escalationTableByContract.Columns.Add("ConSeq", GetType(System.Int32))
                        For Each row As DataRow In escalationTableByContract.Rows
                            row("ConSeq") = contractID
                        Next
                        EscalationsTable.Merge(escalationTableByContract)
                        escalationTableByContract.Clear()
                    End If
                End Using

            Catch ex As Exception

                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])

            End Try
        End Sub


        Public Sub GetBuildingList(contractID As Integer, buildingTable As DataTable)
            Dim request = ConfigManager.GetBuildingUrl() + "?conseq=," + contractID.ToString() + ","
            Try
                Dim dbHelperObj As DBHelper = New DBHelper()
                Dim buildingUrl As String = ConfigManager.GetBuildingUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    client.QueryString.Add("conseq", "," + contractID.ToString + ",")
                    Dim responseString As String = client.DownloadString(buildingUrl)

                    Dim response As Object = JsonConvert.DeserializeObject(responseString)
                    Dim items As JArray = CType(response, JArray)
                    Dim length As Integer = items.Count

                    If length > 0 Then
                        Dim buildingTableByContract As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)
                        buildingTableByContract.Columns.Add("ConSeq", GetType(System.Int32))

                        For Each row As DataRow In buildingTableByContract.Rows
                            row("ConSeq") = contractID
                        Next

                        buildingTable.Merge(buildingTableByContract)
                        buildingTableByContract.Clear()
                    End If
                End Using

            Catch ex As Exception

                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])
            End Try
        End Sub


        Public Sub GetLocationsList(buildingID As Integer, locationTable As DataTable)

            Dim request = ConfigManager.GetLocationsUrl() + "?BGSeq=," + buildingID.ToString() + ","

            Try
                Dim dbHelperObj As DBHelper = New DBHelper()
                Dim locationUrl As String = ConfigManager.GetLocationsUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    client.QueryString.Add("BGSeq", "," + buildingID.ToString + ",")

                    Dim responseString As String = client.DownloadString(locationUrl)
                    Dim response As Object = JsonConvert.DeserializeObject(responseString)
                    Dim items As JArray = CType(response, JArray)
                    Dim length As Integer = items.Count

                    If length > 0 Then
                        Dim locationTableByBuilding As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)
                        locationTableByBuilding.Columns.Add("BG_SEQ", GetType(System.Int32))

                        For Each row As DataRow In locationTableByBuilding.Rows
                            row("BG_SEQ") = buildingID
                        Next

                        locationTable.Merge(locationTableByBuilding)
                        locationTableByBuilding.Clear()
                    End If
                End Using

            Catch ex As Exception


                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])

            End Try
        End Sub

        Public Sub UpdateContractList()
            DisplayAndLog("Start UpdateContractList Execution started at:- " + DateTime.Now)
            Dim request = ConfigManager.GetContractUrl()
            Try
                Dim dbHelperObj As DBHelper = New DBHelper()
                Dim contractUrl As String = ConfigManager.GetContractUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    Dim responseString As String = client.DownloadString(contractUrl)
                    Dim response As Object = JsonConvert.DeserializeObject(responseString)

                    Dim items As JArray = CType(response, JArray)
                    Dim length As Integer = items.Count
                    If length > 0 Then
                        Dim contractTable As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)

                        Dim counter As Integer = 0
                        For Each Chunk As DataTable In Chunkify(contractTable, 100)
                            Dim affectedRows = GetAffectedRows("MakeTempTableContract", Chunk, "ContractList")
                            Dim field As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                            counter += 1
                            DisplayAndLog("Processed temp ContractTable chunk number " + counter.ToString() + " Inserted " + field.ToString() + "chunk")
                        Next
                        ExecuteMainUpdate("Contract", "contractcosspapiupdate")


                        Dim buildingTable As DataTable = New DataTable()
                        Dim EscalationsTable As DataTable = New DataTable()
                        DisplayAndLog("Fetching BuildingList and EscalationsList....")
                        For i As Integer = 0 To contractTable.Rows.Count - 1
                            Dim conSeq As Integer = contractTable.Rows(i)("ConSeq")
                            GetEscalationsList(conSeq, EscalationsTable)
                            Thread.Sleep(20)
                            GetBuildingList(conSeq, buildingTable)
                            Thread.Sleep(20)

                        Next
                        If buildingTable IsNot Nothing AndAlso buildingTable.Rows.Count > 0 Then
                            Dim view As DataView = New DataView(buildingTable)
                            Dim distinctValuesBuilding As DataTable = view.ToTable(True, "BG_SEQ")

                            DisplayAndLog("Fetching LoactaionList....")
                            Dim LocationTable As DataTable = New DataTable()
                            For i As Integer = 0 To distinctValuesBuilding.Rows.Count - 1
                                Dim bg_seq As Integer = distinctValuesBuilding.Rows(i)("BG_SEQ")
                                GetLocationsList(bg_seq, LocationTable)
                                Thread.Sleep(20)
                            Next

                            counter = 0
                            For Each Chunk As DataTable In Chunkify(buildingTable, 1000)

                                Dim affectedRows = GetAffectedRows("MakeTempTableBuilding", Chunk, "BuildingList")
                                Dim field As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                                counter += 1
                                DisplayAndLog("Processed temp BuildingTable chunk number" + counter.ToString() + " Inserted " + field.ToString() + "chunk"
                                      )

                            Next
                            ExecuteMainUpdate("Building", "buildingcosspapiupdate")


                            If LocationTable IsNot Nothing AndAlso LocationTable.Rows.Count > 0 Then
                                counter = 0
                                For Each Chunk As DataTable In Chunkify(LocationTable, 10000)

                                    Dim affectedRows = GetAffectedRows("MakeTempTableLocation", Chunk, "LocationList")
                                    Dim field As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                                    counter += 1
                                    DisplayAndLog("Processed temp LocationTable chunk number" + counter.ToString() + " Inserted  " + field.ToString() + "chunk"
                                      )

                                Next
                                ExecuteMainUpdate("Location", "locationcosspapiupdate")
                            Else
                                DisplayAndLog("No Data Recieved from Location Api")

                            End If

                        Else
                            DisplayAndLog("No Data recieved from Building Api")

                        End If
                        If EscalationsTable IsNot Nothing AndAlso EscalationsTable.Rows.Count > 0 Then
                            counter = 0
                            For Each Chunk As DataTable In Chunkify(EscalationsTable, 100)

                                Dim affectedRows = GetAffectedRows("MakeTempTableEscalation", Chunk, "EscalationList")
                                Dim field As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                                counter += 1
                                DisplayAndLog("Processed temp EscalationTable chunk number" + counter.ToString() + " Inserted" + field.ToString() + "chunk"
                                      )

                            Next
                            ExecuteMainUpdate("Directory", "escalationscosspapiupdate")
                        Else
                            DisplayAndLog("No Data recieved from Escalation Api")
                        End If
                    Else
                        DisplayAndLog("No data recieved from Contract Api... Exiting")
                        Return
                    End If

                End Using
                DisplayAndLog("End UpdateContractList Execution started at:- " + DateTime.Now)
            Catch ex As Exception
                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])

            End Try
        End Sub


        Public Sub UpdateProblemList()
            Dim request = ConfigManager.GetProblemListUrl()
            Try

                Dim problemUrl As String = ConfigManager.GetProblemListUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    Dim responseString As String = client.DownloadString(problemUrl)
                    Dim response As Object = JsonConvert.DeserializeObject(responseString)
                    Dim items As JArray = CType(response, JArray)


                    Dim length As Integer = items.Count
                    If length > 0 Then
                        Dim ProblemTable As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)

                        Dim counter As Integer = 0
                        For Each Chunk As DataTable In Chunkify(ProblemTable, 10000)

                            Dim affectedRows = GetAffectedRows("MakeTempTableProblem", Chunk, "ProblemList")
                            Dim field As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                            counter += 1
                            DisplayAndLog("Processed temp ProblemTable chunk number" + counter.ToString() + " Inserted " + field.ToString() + "chunk"
                                      )
                        Next
                        ExecuteMainUpdate("Problem", "problemcosspapiupdate")
                    Else
                        DisplayAndLog("No data Recieved from Problem Api.... Exiting")
                        Return
                    End If

                End Using

            Catch ex As Exception
                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])

            End Try
        End Sub
        Public Sub UpdateTaskTypeList()
            Dim request = ConfigManager.GetTaskTypeUrl()
            Try

                Dim tasktypeUrl As String = ConfigManager.GetTaskTypeUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    Dim responseString As String = client.DownloadString(tasktypeUrl)
                    Dim response As Object = JsonConvert.DeserializeObject(responseString)
                    Dim items As JArray = CType(response, JArray)


                    Dim length As Integer = items.Count
                    If length > 0 Then
                        Dim TaskTypeTable As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)




                        Dim affectedRows = GetAffectedRows("TaskTypeCosspApiUpdate", TaskTypeTable, "TaskList")
                        Dim insertCount As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                        Dim deleteCount As Integer = affectedRows.Rows(0).Field(Of Integer)(1)


                        DisplayAndLog("Processed TaskTypeTable" + " Inserted " + insertCount.ToString() + " Deleted " + deleteCount.ToString())


                    Else
                        DisplayAndLog("No data Recieved from TaskType Api.... Exiting")
                        Return
                    End If

                End Using

            Catch ex As Exception
                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])

            End Try
        End Sub
        Public Sub UpdateTaskStatusList()
            Dim request = ConfigManager.GetTaskStatusUrl()
            Try

                Dim taskstatusUrl As String = ConfigManager.GetTaskStatusUrl()

                Using client As WebClient = New WebClient()
                    client.Encoding = Encoding.UTF8
                    client.Encoding = UTF8Encoding.UTF8

                    client.Headers.Add("Authorization", "Bearer " & token)
                    Dim responseString As String = client.DownloadString(taskstatusUrl)
                    Dim response As Object = JsonConvert.DeserializeObject(responseString)
                    Dim items As JArray = CType(response, JArray)


                    Dim length As Integer = items.Count
                    If length > 0 Then
                        Dim TaskStatusTable As DataTable = CType(JsonConvert.DeserializeObject(responseString, (GetType(DataTable))), DataTable)




                        Dim affectedRows = GetAffectedRows("TaskStatusCosspApiUpdate", TaskStatusTable, "TaskStatusList")
                        Dim insertCount As Integer = affectedRows.Rows(0).Field(Of Integer)(0)
                        Dim deleteCount As Integer = affectedRows.Rows(0).Field(Of Integer)(1)


                        DisplayAndLog("Processed TaskStatusTable" + " Inserted " + insertCount.ToString() + " Deleted " + deleteCount.ToString())


                    Else
                        DisplayAndLog("No data Recieved from TaskStatus Api.... Exiting")
                        Return
                    End If

                End Using

            Catch ex As Exception
                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString() + "Failed At Request" + request, LogType.[Error])

            End Try
        End Sub
        Public Shared Function GetAffectedRows(ByVal procedureName As String, ByVal Optional Chunk As DataTable = Nothing, ByVal Optional paramName As String = Nothing) As DataTable


            Try
                Dim dbHelperObj As DBHelper = New DBHelper()
                Dim commandChunk As IDbCommand = dbHelperObj.GetCommandObject()

                If paramName IsNot Nothing Then
                    Dim dataTable As IDbDataParameter = commandChunk.CreateParameter()
                    dataTable.ParameterName = paramName
                    dataTable.Value = Chunk
                    commandChunk.Parameters.Add(dataTable)
                End If

                commandChunk.CommandType = CommandType.StoredProcedure
                commandChunk.CommandText = procedureName
                commandChunk.CommandTimeout = 6000


                Dim result As DataTable = New DataTable
                result = dbHelperObj.GetDataTable(commandChunk)


                Return result

            Catch ex As Exception
                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString(), LogType.[Error])
            End Try

        End Function



        Public Function ExecuteMainUpdate(ByVal table As String, ByVal sp As String
                                       )
            Try
                Dim affectedRowsMainTable = GetAffectedRows(sp)
                Dim insertCount As Integer = affectedRowsMainTable.Rows(0).Field(Of Integer)(0)
                Dim deleteCount As Integer = affectedRowsMainTable.Rows(0).Field(Of Integer)(1)
                Dim updateCount As Integer = affectedRowsMainTable.Rows(0).Field(Of Integer)(2)
                DisplayAndLog("Processed Table " + table + "Insert Count " + insertCount.ToString() + " Delete Count " + deleteCount.ToString() + "Update Count " + updateCount.ToString())
            Catch ex As Exception
                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString(), LogType.[Error])
            End Try

        End Function

        Public Shared Sub DisplayAndLog(ByVal log As String)
            Console.WriteLine(log)
            Log4NetManager.LogEvent(log, LogType.Info)
        End Sub



        Public Shared Sub Main(ByVal args As String())
            Try
                DisplayAndLog("Utility Started.")
                Tick(Nothing)
                Dim callback As TimerCallback = New TimerCallback(AddressOf Tick)

                Dim stateTimer As Timer = New Timer(callback, Nothing, 0, 280000)
                Dim p As Program = New Program()


                'p.UpdateContractList()
                'p.UpdateProblemList()
                'p.UpdateTaskTypeList()
                p.UpdateTaskStatusList()

            Catch ex As Exception

                Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString(), LogType.[Error])
            End Try


            DisplayAndLog("Utility process finished.")

        End Sub

    End Class
End Namespace



'Public Function ExecuteQueries(ByVal sp As String
'                               )
'    Try
'        Dim dbHelperObj As DBHelper = New DBHelper()
'        Dim command As IDbCommand = dbHelperObj.GetCommandObject()
'        command.CommandType = CommandType.StoredProcedure
'        command.CommandText = sp
'        dbHelperObj.ExecuteQuery(command)
'    Catch ex As Exception
'        Log4NetManager.LogEvent("An exception occured while executing function " & System.Reflection.MethodInfo.GetCurrentMethod().ReflectedType.ToString() & "." & System.Reflection.MethodInfo.GetCurrentMethod().Name & ". The exception is: " & ex.ToString(), LogType.[Error])
'    End Try

'End Function

'Dim mylist As List(Of String) = New List(Of String)(New String() {"contractcosspapiupdate", "escalationscosspapiupdate", "buildingcosspapiupdate", "locationcosspapiupdate", "problemcosspapiupdate"})

'For Each item In mylist
'    p.ExecuteQueries(item)
'Next

'Public Function RemoveDuplicatesRecords(ByVal dt As DataTable) As DataTable
'    Dim UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.[Default])
'    Dim dt2 As DataTable = UniqueRows.CopyToDataTable()
'    Return dt2
'End Function

'Public Shared Function ConvertToDatatable(Of T)(ByVal data As List(Of T)) As DataTable

'    Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(data.First())
'    Dim table As DataTable = New DataTable()

'    For i As Integer = 0 To props.Count - 1
'        Dim prop As PropertyDescriptor = props(i)

'        If prop.PropertyType.IsGenericType AndAlso prop.PropertyType.GetGenericTypeDefinition() = GetType(Nullable(Of)) Then
'            table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()(0))
'        Else
'            table.Columns.Add(prop.Name, prop.PropertyType)

'        End If
'    Next

'    Dim values As Object() = New Object(props.Count - 1) {}

'    For Each item As T In data

'        For i As Integer = 0 To values.Length - 1
'            values(i) = props(i).GetValue(item)
'        Next

'        table.Rows.Add(values)
'    Next
'    table.AcceptChanges()
'    Return table
'End Function

'Public Shared Sub WriteDataToFile(ByVal submittedDataTable As DataTable, ByVal submittedFilePath As String)
'    Dim i As Integer = 0
'    Dim sw As StreamWriter = Nothing
'    sw = New StreamWriter(submittedFilePath, False)

'    For i = 0 To submittedDataTable.Columns.Count - 1 - 1
'        sw.Write(submittedDataTable.Columns(i).ColumnName & ";")
'    Next

'    sw.Write(submittedDataTable.Columns(i).ColumnName)
'    sw.WriteLine()

'    For Each row As DataRow In submittedDataTable.Rows
'        Dim array As Object() = row.ItemArray

'        For i = 0 To array.Length - 1 - 1
'            sw.Write(array(i).ToString() & ";")
'        Next

'        sw.Write(array(i).ToString())
'        sw.WriteLine()
'    Next

'    sw.Close()
'End Sub